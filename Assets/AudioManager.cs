﻿using UnityEngine.Audio;
using System.Collections;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public GameObject spatialPrefab;
    public Sound[] sounds;

    public static AudioManager instance;

    void Awake() {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;

            s.source.loop = s.loop;           

        }
	}
    
    public void Play(string name)
    {
       Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound; " + name + "not found!");
            return;
        }
        s.source.PlayOneShot(s.clip); 
    }

    public void PlayRandomPitch(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }
        float randomPitch = UnityEngine.Random.Range(s.maxPitch, s.minpitch);
        s.source.pitch = randomPitch;

        s.source.PlayOneShot(s.clip);

        
    }

    public void PlayRandomPitch(string name, Vector3 pos)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }
        float randomPitch = UnityEngine.Random.Range(s.maxPitch, s.minpitch);

        if(spatialPrefab != null)
        {
            GameObject go = Instantiate(spatialPrefab, pos, Quaternion.identity);
            AudioSource spatial = go.GetComponent<AudioSource>();

            spatial.clip = s.clip;
            spatial.volume = s.volume;
            spatial.pitch = randomPitch;
            spatial.PlayOneShot(spatial.clip);
        }


    }
}
