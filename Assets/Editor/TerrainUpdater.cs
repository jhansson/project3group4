﻿using UnityEngine;
using UnityEditor;

public class TerrainUpdater : EditorWindow
{

    public bool replaceSplat = true;
    public bool addScript = true;

    [MenuItem("Window/Update Terrain")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<TerrainUpdater>("Update Terrain");
    }

    private void OnGUI()
    {

        GUI.skin.label.wordWrap = true;

        string instructions = "Make sure you have the Material and Tile checked out on Perforce.\n\n" +
            "Then select the Terrain mesh in hierarchy, and hit update. Depending on option this will " +
            "update the splatmap for the correct material, and add the script useded to orient the textures.\n\n" +
            "After update, you will have to manually replace the material for the terrain however. Either by drag " +
            "dropping it or changing it in the Mesh Renderer.";

        GUILayout.Label("Instructions", EditorStyles.boldLabel);
        GUILayout.Label(instructions);

        replaceSplat = GUILayout.Toggle(replaceSplat, "Replace Splat Map");
        addScript = GUILayout.Toggle(addScript, "Add Terrain Script");

        if (GUILayout.Button("Update")) {
            Object terrain = Selection.activeObject;

            if (terrain != null)
            {
                if (replaceSplat)
                    ReplaceSplatMap((GameObject)terrain);
                if (addScript)
                    AddTerrainOrient((GameObject)terrain);
            }                
        }
    }

    private void AddTerrainOrient(GameObject terrain)
    {
        MonoBehaviour script = terrain.AddComponent<TerrainOrientation>();
    }

    private void ReplaceSplatMap(GameObject terrain)
    {
        Material material = null;

        // The Material we're looking for has the same name as the tile which ought to be parent of the selected terrain
        string SearchFor = terrain.transform.parent.name;

        // Hardcoded folder path, where we have all our premade materials for each tile using the correct shader.
        string[] folders = {"Assets/Environments/Materials/Terrain"};
        string[] guids = AssetDatabase.FindAssets(SearchFor, folders);

        // We should only be finding one material matching the tile prefab name,
        if (guids.Length > 0)
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[0]);
            material = AssetDatabase.LoadAssetAtPath<Material>(path);

            if (material != null)
            {
                material.SetTexture("_SplatMap", GetSplatMap(terrain));
            }            
        }
    }

    private Texture GetSplatMap(GameObject terrain)
    {
        // Assuming terrain now has the material given from Terrain To Mesh, we should now get the vertex paint map.
        Renderer renderer = terrain.GetComponent<Renderer>();
        return renderer.sharedMaterial.GetTexture("_V_T2M_Control");
    }
}