﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(PlayerInput))]
public class AnimaticPlayer : MonoBehaviour {

    private VideoPlayer vp;
    public bool isLastLevel;
    private PlayerInput pInput;
    public GameObject background;

    private void Awake()
    {
        vp = GetComponent<VideoPlayer>();

        vp.loopPointReached += MovieFinished;
        pInput = GetComponent<PlayerInput>();
        vp.Play();
    }

    private void Start()
    {
        //background.SetActive(false);


    }

    private void Update()
    {
        if(pInput.JumpButton || Input.GetButtonDown("MenuButton"))
        {
            LoadNextLevel();
        }

        //vp.SetDirectAudioVolume(vp.audi)
    }

    private void MovieFinished(VideoPlayer vPlayer)
    {
        LoadNextLevel();
    }

    private void LoadNextLevel()
    {
        if (!isLastLevel)
            LevelManager.levelManager.LoadNextLevel();
        else
            LevelManager.levelManager.LoadLevel(0);
    }
}
