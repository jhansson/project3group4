﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HadesWall : MonoBehaviour
{

    [SerializeField] private GameObject[] portals;
    [SerializeField] private GameObject[] walls;
    [SerializeField] private GameObject[] enemySpawnPoints;
    [SerializeField] private GameObject[] enemies;
    [SerializeField] private GameObject spawnVFX;
    [HideInInspector] public bool alreadySpawned;
    private bool enemiesActive;
    private List<GameObject> currentEnemies;
    private NavMeshSurface navMesh;
    private Animator anim;
    private AudioSource source;
    [SerializeField] private AudioClip spawnEnemiesLaughter;
    [SerializeField] private AudioClip[] attackClip;
    private int sfxIndex;
    private int previousIndex;

    private void Awake()
    {
        currentEnemies = new List<GameObject>();
        SetWallState(false);
        navMesh = GetComponent<NavMeshSurface>();
        if (navMesh != null)
            navMesh.BuildNavMesh();
        anim = GetComponent<Animator>();
        SetPortalActiveState(false);
        source = GetComponent<AudioSource>();

    }

    private void Update()
    {
        if (enemiesActive)
        {
            for (int i = currentEnemies.Count - 1; i >= 0; i--)
            {
                if (currentEnemies[i] == null)
                    currentEnemies.RemoveAt(i);
            }

            if (currentEnemies.Count <= 0)
            {
                SetWallState(false);
                anim.SetBool("WallUp", false);
                enemiesActive = false;
                SetPortalActiveState(false);
            }

        }

    }

    public void SetPortalActiveState(bool activeState)
    {
        if (portals.Length > 0)
        {
            for (int i = 0; i < portals.Length; i++)
            {
                portals[i].SetActive(activeState);
            }
            if (activeState == true)
                SpawnEnemies();
        }
    }

    public void SpawnEnemies()
    {
        for (int i = 0; i < enemySpawnPoints.Length; i++)
        {
            int rand = Random.Range(0, enemies.Length);
            GameObject go = Instantiate(enemies[rand], enemySpawnPoints[i].transform.position, Quaternion.identity);
            currentEnemies.Add(go);
            if (spawnVFX != null)
                Instantiate(spawnVFX, enemySpawnPoints[i].transform.position, Quaternion.identity);
        }
        source.clip = spawnEnemiesLaughter;
        source.Play();
        anim.SetBool("WallUp", true);
        alreadySpawned = true;
        enemiesActive = true;
        SetWallState(true);
    }

    public void SetWallState(bool activeState)
    {
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i].SetActive(activeState);
        }
    }

    public void PlayAttackSFX()
    {
        if (attackClip.Length == 0)
            return;

        if (attackClip.Length == 1)
        {
            sfxIndex = 0;
        }
        else
        {
            sfxIndex = Random.Range(0, attackClip.Length);
            if (sfxIndex == previousIndex)
            {
                PlayAttackSFX();
                return;
            }
        }
        previousIndex = sfxIndex;

        source.clip = attackClip[sfxIndex];
        source.Play();
    }

    /// <summary>
    /// Called from hades death animation
    /// </summary>
    public void Died()
    {
        LevelManager.levelManager.LoadNextLevel();
    }
}
