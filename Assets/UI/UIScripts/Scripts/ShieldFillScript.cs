﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldFillScript : MonoBehaviour {

    [SerializeField]
    private Image fillImage;

    //[SerializeField]
    private Shield shield;

    private float imageFull = 1;
    private float imageEmpty = 0;

	// Use this for initialization
	void Start () {
        shield = FindObjectOfType<Shield>();
        fillImage.fillAmount = imageEmpty;
	}

    // Update is called once per frame
    void Update() {
        if (shield != null)
        {
            if (!shield.shieldEnabled)
            {
                if (shield.transform.parent.gameObject.activeInHierarchy && Time.time <= shield.timer + shield.rechargeDuration)
                {
                    fillImage.fillAmount = Mathf.Clamp((Time.time - shield.timer) / shield.rechargeDuration, imageEmpty, imageFull);
                }
            }
            else
            {
                fillImage.fillAmount = imageFull;
            }
        } else
        {
            shield = FindObjectOfType<Shield>();
        }
        

        /*
        ResetShield();

        if (shield.shieldEnabled == false && shield.shieldHealth <= shield.timesHit)
        {
            fillImage.fillAmount = imageEmpty;
        }
        else if (shield.shieldEnabled == true)
        {
            //fillImage.fillAmount = imageFull;
        }
        */
	}

    void ResetShield()
    {
        if (shield.timesHit >= shield.shieldHealth)
        {
            StartCoroutine("Refill");
        }
    }

    private IEnumerator Refill()
    {
        while(fillImage.fillAmount < 1)
        {
            fillImage.fillAmount += Time.deltaTime;
            fillImage.fillAmount = Mathf.Clamp01(fillImage.fillAmount);
            yield return new WaitForSeconds(.1f);
        }

    }
}
