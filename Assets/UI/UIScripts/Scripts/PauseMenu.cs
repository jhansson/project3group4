﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour {

    public EventSystem eventSystem;
    private bool buttonSelected;
    public GameObject selectedButton;

    private void Update()
    {
        if ((Input.GetAxisRaw("MoveHorizontal") != 0 || Input.GetAxisRaw("MoveVertical") != 0) && !buttonSelected)
        {
            eventSystem.SetSelectedGameObject(selectedButton);
            buttonSelected = true;
        }

    }

    private void OnDisable()
    {
        buttonSelected = false;
    }

    private void OnEnable()
    {
        selectedButton.GetComponent<Button>().Select();
        selectedButton.GetComponent<Button>().OnSelect(null);
    }
}
