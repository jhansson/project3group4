﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    
    private GameObject loader;

    public void PlayGame()
    {
       
        StartCoroutine(PlayCameraAnimation());

    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit!!!");
    }

    IEnumerator PlayCameraAnimation ()
    {
        FindObjectOfType<Camera>().GetComponent<PlayableDirector>().Play();

        yield return new WaitForSeconds(5);

        if (loader != null)
        {
            loader.GetComponent<LO_SelectStyle>().SetStyle("TW3_Style");
            loader.GetComponent<LO_LoadScene>().ChangeToScene("Tutorial");
        }
        else
        {
            LevelManager.levelManager.LoadNextLevel();
        }

        
    }

    public void ReturnToMainMenu()
    {
        LevelManager.levelManager.LoadLevel(0); 
    }

}
