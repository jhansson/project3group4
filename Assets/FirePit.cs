﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePit : MonoBehaviour {

    private Collider col;
    private float timer;
    private float fireStartTime;
    private bool playerIsInside;
    private bool fireStarted;
    [SerializeField] private GameObject firePrefab;
    [SerializeField] private float timeToBurn;
    [SerializeField] private float fireLifetime;
    [SerializeField] private float dealDamageCooldown;

    Vector3 fireSize = new Vector3(0.1f, 0.1f, 0.1f);
    [SerializeField] private float fireBuildUpMutliplier = 5;
    private ParticleSystem.MainModule ps;

    private void Awake()
    {
        col = GetComponent<BoxCollider>();
        ps = firePrefab.GetComponent<ParticleSystem>().main;
        firePrefab.transform.localScale = fireSize;
    }

    private void Update()
    {
        Debug.Log("Hey");
        //check if fire has already started
        if(!fireStarted)
        {
            
            if (playerIsInside) //if the player is inside bounds, then add to timer
                timer += Time.deltaTime;
            else
                timer -= Time.deltaTime * .5f; //else decrease the timer

            timer = Mathf.Clamp(timer, 0, timeToBurn);
            if (timer == timeToBurn)
            {
                fireStarted = true;
                fireStartTime = Time.time;
                StartCoroutine("StartFire");
            }

        }
        firePrefab.transform.localScale = fireSize * timer * fireBuildUpMutliplier;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
            playerIsInside = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
            playerIsInside = false;
    }

    IEnumerator StartFire()
    {
        while (Time.time <= fireStartTime + fireLifetime)
        {
            Debug.Log("still dealing damage");
            Collider[] overlap = Physics.OverlapBox(transform.position, col.bounds.extents);
            if (overlap.Length > 0)
            {
                for (int i = 0; i < overlap.Length; i++)
                {
                    overlap[i].gameObject.SendMessage("TakeDamage", 1, SendMessageOptions.DontRequireReceiver);
                }
                Debug.Log("Damage dealt, now waiting for next");
                yield return new WaitForSeconds(dealDamageCooldown);
            }
        }
        Debug.Log("out of loop");
        StartCoroutine("ScaleDownFire");
        //should slowly scale down the fire
    }

    IEnumerator ScaleDownFire()
    {
        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        fireStarted = false;
        timer = 0;
    }
}
