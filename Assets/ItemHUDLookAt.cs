﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHUDLookAt : MonoBehaviour {

    private Transform lookAtTarget;

    // Use this for initialization
    void Start () {
        lookAtTarget = FindObjectOfType<Camera>().transform;
        transform.LookAt(lookAtTarget);
        transform.localScale = new Vector3(-1, 1, 1);
    }
	
	// Update is called once per frame
	void Update () {
        //transform.parent.LookAt(lookAtTarget);
    }
}
