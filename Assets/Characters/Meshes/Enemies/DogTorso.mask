%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: DogTorso
  m_Mask: 01000000010000000100000000000000000000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Dog_People1
    m_Weight: 1
  - m_Path: Dog_People1/dog_people_eyes
    m_Weight: 1
  - m_Path: Dog_People_Pants2
    m_Weight: 1
  - m_Path: dog_people_shirt
    m_Weight: 1
  - m_Path: root
    m_Weight: 1
  - m_Path: root/pelvis
    m_Weight: 1
  - m_Path: root/pelvis/spine_01
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/neck_01
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/neck_01/head
    m_Weight: 1
  - m_Path: root/pelvis/Tale_01
    m_Weight: 1
  - m_Path: root/pelvis/Tale_01/Tale_02
    m_Weight: 1
  - m_Path: root/pelvis/Tale_01/Tale_02/Tale_03
    m_Weight: 1
  - m_Path: root/pelvis/Tale_01/Tale_02/Tale_03/Tale_04
    m_Weight: 1
  - m_Path: root/pelvis/Tale_01/Tale_02/Tale_03/Tale_04/Tale_05
    m_Weight: 1
  - m_Path: root/pelvis/Tale_01/Tale_02/Tale_03/Tale_04/Tale_05/Tale_06
    m_Weight: 1
  - m_Path: root/pelvis/Tale_01/Tale_02/Tale_03/Tale_04/Tale_05/Tale_06/Tale_07
    m_Weight: 1
  - m_Path: root/pelvis/thigh_l
    m_Weight: 1
  - m_Path: root/pelvis/thigh_l/calf_l
    m_Weight: 1
  - m_Path: root/pelvis/thigh_l/calf_l/foot_l
    m_Weight: 1
  - m_Path: root/pelvis/thigh_l/calf_l/foot_l/ball_l
    m_Weight: 1
  - m_Path: root/pelvis/thigh_r
    m_Weight: 1
  - m_Path: root/pelvis/thigh_r/calf_r
    m_Weight: 1
  - m_Path: root/pelvis/thigh_r/calf_r/foot_r
    m_Weight: 1
  - m_Path: root/pelvis/thigh_r/calf_r/foot_r/ball_r
    m_Weight: 1
