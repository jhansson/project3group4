﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupidsUnderwear : MonoBehaviour {


    [SerializeField] private int amountOfHealth = 1;
    [SerializeField] private float rechargeDuration = 10;
    private float timer;

    private void OnEnable()
    {
        timer = Time.time;
    }

    // Update is called once per frame
    void Update () {

        if(Time.time > timer + rechargeDuration)
        {
            GetComponentInParent<IHealth>().AddHealth(amountOfHealth);
            timer = Time.time;
        }
    }
}
