﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    public Vector3 rotateAxis = new Vector3(0, 0, 2);

    private void FixedUpdate()
    {
        transform.Rotate(rotateAxis * Time.fixedDeltaTime);
    }
}
