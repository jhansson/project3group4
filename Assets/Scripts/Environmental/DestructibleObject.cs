﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour {

    [SerializeField] protected GameObject[] loot;
    [SerializeField] protected float dropRate;
    [SerializeField, Tooltip("How many hits it can withstand before getting destroyed"), Range(1, 10)]
    protected int health = 1;
    protected int hitCounter;
    [SerializeField] protected GameObject breakEffect;
    [SerializeField] protected GameObject breakMesh;
    public enum ObjectSound {barrel, watermelon, vase};
    public ObjectSound currentObject;

    public void Awake()
    {
    }

    public virtual void TakeDamage(int damage)
    {
        //hitCounter++;
        //if(hitCounter >= health)
        //{


        //}

        health -= damage;
        if (health <= 0)
        {
            OnDie();
            Destroy(gameObject);
        }

    }


    public virtual void OnDie()
    {
        switch (currentObject)
        {
            case ObjectSound.barrel:
                FindObjectOfType<AudioManager>().PlayRandomPitch("BarrelBreak", transform.position);
                
                break;
            case ObjectSound.watermelon:
                FindObjectOfType<AudioManager>().PlayRandomPitch("WatermelonSplat", transform.position);
                
                break;
            case ObjectSound.vase:
                FindObjectOfType<AudioManager>().PlayRandomPitch("VaseBreak", transform.position);
                
                break;
        }
        DropItem();

        if(breakEffect != null)
            Instantiate(breakEffect, transform.position, Quaternion.identity);

        if (breakMesh != null)
        {
            RaycastHit hit;
            Ray ray = new Ray(transform.position, Vector3.down);
            if(Physics.Raycast(ray, out hit))
            {
                Instantiate(breakMesh, hit.point, Quaternion.identity);
            }
            else
                Instantiate(breakMesh, transform.position, Quaternion.identity);
        }

       
    }

    public virtual void DropItem()
    {
        if (loot.Length == 0)
            return;
        for (int i = 0; i < loot.Length; i++)
        {
            float rand = Random.Range(0f, 1f);
            if (rand <= dropRate)
            {
                int randItem = loot.Length;
                randItem = Random.Range(0, randItem);
                if (loot[randItem] != null)
                    Instantiate(loot[randItem], transform.position, Quaternion.identity);
                break;
            }
        }
    }
}
