﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    [SerializeField] protected GameObject hudDisplay;

    public virtual void Awake()
    {
        if (hudDisplay != null)
            hudDisplay.SetActive(false);
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        //TODO should display a box with item name and wait for button presses
        if (other.tag == "Player")
        {
            if(hudDisplay != null)
                hudDisplay.SetActive(true);
        }
    }

    public virtual void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (hudDisplay != null)
                hudDisplay.SetActive(false);
        }
    }

    public virtual void Interact(GameObject go)
    {

    }
}
