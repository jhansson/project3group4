﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractablePortal : Interactable {

    public string sceneName;

    private void Start()
    {
    }

    public override void Interact(GameObject go)
    {

        base.Interact(go);
        FindObjectOfType<AudioManager>().Play("PortalUse");
        LevelManager.levelManager.LoadNextLevel();
        //LevelManager.levelManager.LoadLevel(sceneName);
        //SceneManager.LoadScene(sceneName);
    }
}
