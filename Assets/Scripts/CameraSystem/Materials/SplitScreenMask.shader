﻿Shader "Mask/SplitScreen" {
	//Simple depthmask shader 
	SubShader {
		Tags{ "Queue" = "Geometry-10" } //Tags {Queue = Background} // "Queue" = "Geometry+502"
	    Pass {
			Tags{ "LightMode" = "Deferred" } // otherwise does not work in deferred path
			ZWrite On
			ColorMask 0
		}
	}
}