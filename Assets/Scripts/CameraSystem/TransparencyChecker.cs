﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparencyChecker : MonoBehaviour
{
    Camera cam;
    [SerializeField] private Material material;
    public Shader PostOutline;
    private Renderer rend;
    Material newMaterial;
    public LayerMask layerMask;
    [SerializeField] private Vector3 offset;
    public bool isStaticMesh;

    void Start()
    {
        cam = Camera.main;
        rend = GetComponent<Renderer>();
        newMaterial = new Material(PostOutline);
    }

    void FixedUpdate()
    { 
        RaycastHit hit;

        Ray ray = new Ray(transform.position + offset, (cam.transform.position - transform.position) * 100);
        Debug.DrawRay(ray.origin, ray.direction * 100, Color.red);

        if (Physics.Raycast(ray, out hit, layerMask))
        {
            //gameObject.layer = LayerMask.NameToLayer("Outline");
            SetRenderlayerForChildren(LayerMask.NameToLayer("Outline"));
        }
        else
        {
            //gameObject.layer = LayerMask.NameToLayer("Default");
            SetRenderlayerForChildren(LayerMask.NameToLayer("Default"));
        }
               
    }

    private void SetRenderlayerForChildren(int layermask)
    {
        // Get all active game objects, with skinned mesh renderer components
        if(!isStaticMesh)
        {
            SkinnedMeshRenderer[] children = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(false);

            foreach (SkinnedMeshRenderer child in children)
            {
                child.gameObject.layer = layermask;
            }
        }
        else
        {
            Renderer[] children = gameObject.GetComponentsInChildren<Renderer>(false);

            foreach (Renderer child in children)
            {
                child.gameObject.layer = layermask;
            }
        }

    }
}

