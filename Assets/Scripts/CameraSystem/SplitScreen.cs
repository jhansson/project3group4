﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitScreen : MonoBehaviour
{
  
    public Vector3 battleCam;

    public Vector3 exploreCam;

    public Vector3 targetCam;

    [SerializeField, Tooltip("Reference to the Player 1 gameobject in the scene.")]
    private Transform player1;

    [SerializeField, Tooltip("Reference to the Player 2 gameobject in the scene.")]
    private Transform player2;

    [SerializeField, Tooltip("The distance in meters between the players when the screen should split.")]
    private float splitDistance = 5;

    [SerializeField, Tooltip("The color of the splitter which splits the two screens up.")]
    private Color splitterColor;

    [SerializeField, Tooltip("The width of the splitter which splits the two screens up.")]
    private float splitterWidth;

    [SerializeField, Tooltip("The camera offset when only one camera is active.")]
    public Vector3 cameraOffsetSameCamera = new Vector3(-7, 15, -7);

    [SerializeField, Tooltip("The camera offset when the split camera is active.")]
    private Vector3 cameraOffsetSplitCamera = new Vector3(-10, 20, -10);

    [SerializeField, Tooltip("Distance in meters that each camera will move to keep players in split screen view.")]
    private float offsetToKeepPlayersInSplitViews = 7f;

    [SerializeField, Tooltip("The camera rotation")]
    private Vector3 cameraRotation = new Vector3(0, 45f, 0);

    [SerializeField, Tooltip("How quickly the camera keeps up with the target.")]
    private float cameraSpeed;

    [SerializeField, Tooltip("The split screen mask material.")]
    private Material splitScreenMaterial;

    [SerializeField, Tooltip("Material for the splitter")]
    private Material splitterMaterial;

    //The two cameras, both of which are initalized/referenced in the start function.
    private GameObject camera1;
    private GameObject camera2;

    //The two quads used to draw the second screen, both of which are initalized in the start function.
    private GameObject split;
    private GameObject splitter;

    private void Awake()
    {
        
    }

    void Start()
    {
        targetCam = cameraOffsetSameCamera;
        exploreCam = targetCam;

        //Referencing camera1 and initalizing camera2.
        camera1 = Camera.main.gameObject;
        camera1.transform.rotation = Quaternion.Euler(cameraRotation);
        camera2 = new GameObject();
        camera2.transform.rotation = Quaternion.Euler(cameraRotation);
        camera2.transform.position = camera1.transform.position;
        camera2.AddComponent<Camera>();
        camera2.AddComponent<Assets.MultiAudioListener.VirtualMultiAudioListener>();
        camera2.GetComponent<Camera>().depth = Camera.main.depth - 1;
        camera2.GetComponent<Camera>().fieldOfView = 50f;

        //Setting up the culling mask of camera2 to ignore the layer "TransparentFX" as to avoid rendering the split and splitter on both cameras.
        camera2.GetComponent<Camera>().cullingMask = ~(1 << LayerMask.NameToLayer("TransparentFX"));

        //Setting up the splitter and initalizing the gameobject.
        splitter = GameObject.CreatePrimitive(PrimitiveType.Quad);
        splitter.transform.parent = gameObject.transform;
        splitter.transform.localPosition = Vector3.forward;
        splitter.transform.localScale = new Vector3(10, 0.1f / 10, 1);
        splitter.transform.localEulerAngles = Vector3.zero;
        splitter.SetActive(false);

        //Setting up the split and initalizing the gameobject.
        split = GameObject.CreatePrimitive(PrimitiveType.Quad);
        split.transform.parent = splitter.transform;
        split.transform.localPosition = new Vector3(0, -(1 / (0.1f / 9.5f)), -0.05f);
        split.transform.localScale = new Vector3(1, 2 / (0.1f / 9.5f), 1);
        split.transform.localEulerAngles = Vector3.zero;

        //Creates both temporary materials required to create the splitscreen.
        Material tempMat = splitterMaterial;
        tempMat.color = splitterColor;
        splitter.GetComponent<Renderer>().material = tempMat;
        splitter.GetComponent<Renderer>().sortingOrder = 2;
        splitter.layer = LayerMask.NameToLayer("TransparentFX");
        Material tempMat2 = splitScreenMaterial;
        split.GetComponent<Renderer>().material = tempMat2;
        split.layer = LayerMask.NameToLayer("TransparentFX");
    }

    void LateUpdate()
    {
        CamChange();

        if (player2 != null && player1.gameObject.activeInHierarchy && player2.gameObject.activeInHierarchy)
        {
            // Direction to eachother and camera offset to keep in the middle
            Vector3 directionToPlayerOne = (player1.transform.position - player2.transform.position) / (player1.transform.position - player2.transform.position).magnitude;
            Vector3 directionToPlayerTwo = (player2.transform.position - player1.transform.position) / (player2.transform.position - player1.transform.position).magnitude;

            // The offset for the cameras to keep the player centered in their respective views.
            Vector3 camSplitOffset = (directionToPlayerOne * offsetToKeepPlayersInSplitViews) + (player2.transform.forward * 3f);
            Vector3 camSplitOffsetTwo = directionToPlayerTwo * offsetToKeepPlayersInSplitViews + (player1.transform.forward * 3f);

            //Gets the z axis distance between the two players and just the standard distance.
            float zDistance = player1.position.z - player2.transform.position.z;
            float distance = Vector3.Distance(player1.position, player2.transform.position);

            //Sets the angle of the player up, depending on who's leading on the x axis.
            float angle;
            if (player1.transform.position.x <= player2.transform.position.x)
            {
                angle = Mathf.Rad2Deg * Mathf.Acos(zDistance / distance);
            }
            else
            {
                angle = Mathf.Rad2Deg * Mathf.Asin(zDistance / distance) - 90;
            }

            //Rotates the splitter according to the new angle.
            //splitter.transform.localEulerAngles = new Vector3(0, 0, angle + cameraRotation.y);

            //Gets the exact midpoint between the two players.
            Vector3 midPoint = new Vector3((player1.position.x + player2.position.x) / 2, (player1.position.y + player2.position.y) / 2, (player1.position.z + player2.position.z) / 2);
            //midPoint -= new Vector3(cameraOffsetSameCamera.x, 0, cameraOffsetSameCamera.z);
            //Waits for the two cameras to split and then calcuates a midpoint relevant to the difference in position between the two cameras.
            if (distance > splitDistance)
            {
                Vector3 offset = midPoint - player1.position;
                offset.x = Mathf.Clamp(offset.x, -offsetToKeepPlayersInSplitViews / 2, offsetToKeepPlayersInSplitViews / 2);
                offset.y = Mathf.Clamp(offset.y, -offsetToKeepPlayersInSplitViews / 2, offsetToKeepPlayersInSplitViews / 2);
                offset.z = Mathf.Clamp(offset.z, -offsetToKeepPlayersInSplitViews / 2, offsetToKeepPlayersInSplitViews / 2);
                midPoint = player1.position + offset;

                Vector3 offset2 = midPoint - player2.position;
                offset2.x = Mathf.Clamp(offset.x, -offsetToKeepPlayersInSplitViews / 2, offsetToKeepPlayersInSplitViews / 2);
                offset2.y = Mathf.Clamp(offset.y, -offsetToKeepPlayersInSplitViews / 2, offsetToKeepPlayersInSplitViews / 2);
                offset2.z = Mathf.Clamp(offset.z, -offsetToKeepPlayersInSplitViews / 2, offsetToKeepPlayersInSplitViews / 2);
                Vector3 midPoint2 = player2.position - offset;

                //Sets the splitter and camera to active and sets the second camera position as to avoid lerping continuity errors.
                if (splitter.activeSelf == false)
                {
                    splitter.SetActive(true);
                    camera2.SetActive(true);

                    camera2.transform.position = camera1.transform.position; // player2.transform.position + cameraOffsetSplitCamera + camSplitOffset;
                    //camera2.transform.rotation = camera1.transform.rotation;
                    camera2.transform.rotation = Quaternion.Euler(cameraRotation);
                }
                else
                {
                    // Set camera for player 1
                    //camera1.transform.position = Vector3.Lerp(camera1.transform.position, player1.transform.position + cameraOffsetSplitCamera + camSplitOffsetTwo, Time.deltaTime * cameraSpeed);
                    camera1.transform.position = Vector3.Lerp(camera1.transform.position, midPoint + cameraOffsetSplitCamera + camSplitOffsetTwo, Time.deltaTime * cameraSpeed);
                    Quaternion newRot = Quaternion.LookRotation(midPoint - camera1.transform.position);
                    //camera1.transform.rotation = Quaternion.Lerp(camera1.transform.rotation, newRot, Time.deltaTime * 5);
                    camera1.transform.rotation = Quaternion.Euler(cameraRotation);

                    // Set camera for player 2
                    //camera2.transform.position = Vector3.Lerp(camera2.transform.position, player2.transform.position + cameraOffsetSplitCamera + camSplitOffset, Time.deltaTime * cameraSpeed);
                    camera2.transform.position = Vector3.Lerp(camera2.transform.position, midPoint2 + cameraOffsetSplitCamera + camSplitOffset, Time.deltaTime * cameraSpeed);
                    Quaternion newRot2 = Quaternion.LookRotation(midPoint2 - camera2.transform.position);
                    //camera2.transform.rotation = Quaternion.Lerp(camera2.transform.rotation, newRot2, Time.deltaTime * cameraSpeed);
                    camera2.transform.rotation = Quaternion.Euler(cameraRotation);
                }
            }
            else
            {
                //Deactivates the splitter and camera once the distance is less than the splitting distance (assuming it was at one point).
                if (splitter.activeSelf)
                    splitter.SetActive(false);

                camera2.SetActive(false);
                //midPoint += (directionToPlayerTwo * -distance);
                midPoint.y += (distance / 2f);
                
                camera1.transform.position = Vector3.Lerp(camera1.transform.position, midPoint + cameraOffsetSameCamera, Time.deltaTime * cameraSpeed);
                Quaternion newRot = Quaternion.LookRotation(midPoint - camera1.transform.position);
                //camera1.transform.rotation = Quaternion.Lerp(camera1.transform.rotation, newRot, Time.deltaTime * 5);
                camera1.transform.rotation = Quaternion.Euler(cameraRotation);
            }
        }
        else
        {
            Transform activePlayer = (player1.gameObject.activeInHierarchy) ? player1 : player2;
            splitter.SetActive(false);
            camera2.SetActive(false);
            if (activePlayer != null)
            {
                camera1.transform.position = Vector3.Lerp(camera1.transform.position, activePlayer.transform.position + cameraOffsetSameCamera, Time.deltaTime * cameraSpeed);
                Quaternion newRot = Quaternion.LookRotation(activePlayer.transform.position - camera1.transform.position);
                //camera1.transform.rotation = Quaternion.Lerp(camera1.transform.rotation, newRot, Time.deltaTime * cameraSpeed);
                camera1.transform.rotation = Quaternion.Euler(cameraRotation);
            }
            //
        }
    }

    public void CamChange()
    {
        cameraOffsetSameCamera = Vector3.Lerp(cameraOffsetSameCamera, targetCam, 1.5f);
    }
}


