﻿using System.Collections;
using UnityEngine;

public class OutlinePostEffect : MonoBehaviour {

    Camera AttachedCamera;
    public Shader PostOutline;
    public Shader DrawSimple;
    Camera TempCam;
    public Material PostMaterial;
    // public RenderTexture TempRT;

	void Start () {
        AttachedCamera = GetComponent<Camera>();
        TempCam = new GameObject().AddComponent<Camera>();
        TempCam.enabled = false;
        // PostMaterial = new Material(PostOutline);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        //set up a temporary camera
        TempCam.CopyFrom(AttachedCamera);
        TempCam.clearFlags = CameraClearFlags.Color;
        TempCam.backgroundColor = Color.black;

        //cull any layer that isn't the outline
        TempCam.cullingMask = 1 << LayerMask.NameToLayer("Outline");

        //make the temporary rendertexture
        RenderTexture TempRT = new RenderTexture(source.width, source.height, 0, RenderTextureFormat.R8);

        //put it to video memory
        TempRT.Create();

        //set the camera's target texture when rendering
        TempCam.targetTexture = TempRT;        

        //render all objects this camera can render, but with our custom shader.
        TempCam.RenderWithShader(DrawSimple, "");

        // Get the rendered scene as a texture
        PostMaterial.SetTexture("_SceneTex", source);

        // copy the temporary RT to the final image
        // Graphics.Blit(source, destination);  // The normally rendered image.
        Graphics.Blit(TempRT, destination, PostMaterial);   // The outline 

        // release the temporary Render Texture
        TempRT.Release();
    }
}
