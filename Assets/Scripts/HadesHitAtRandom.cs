﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HadesHitAtRandom : MonoBehaviour {

    Animator animator;

    // Use this for initialization
    void Start () {
        animator = gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        animator.SetBool("Hit", false);
        int hit = Random.Range(0, 10);

        if (hit <= 8)
            animator.SetBool("Hit", true);
	}
}
