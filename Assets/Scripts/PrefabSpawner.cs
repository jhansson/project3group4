﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabSpawner : MonoBehaviour {

    public GameObject prefab;

    private void Start()
    {
        if(prefab != null)
            Instantiate(prefab, transform);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(transform.position, new Vector3(1.5f, 0.5f, 1.5f));



    }

}
