﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiercingProjectile : Projectile {



    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void PreventDeepCollision()
    {
        base.PreventDeepCollision();
    }

    private void OnTriggerEnter(Collider other)
    {
        Shield opponentShield = other.gameObject.GetComponentInChildren<Shield>();
        if (opponentShield != null && opponentShield.shield.activeInHierarchy)
        {
            opponentShield.DamageShield(1f);
        }
        else
        {
            if (other.gameObject.GetComponent<PlayerController>() == null)
                other.gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
        }
        if(projectileHitEffect != null)
            Instantiate(projectileHitEffect, transform.position, Quaternion.identity);
    }

}
