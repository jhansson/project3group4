﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveProjectile : Projectile {


    [SerializeField] private float explosiveRadius = 2;

    #region Baseline Projectile Functions

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void PreventDeepCollision()
    {
        base.PreventDeepCollision();
    }
    #endregion

    public override void OnCollisionEnter(Collision other)
    {
        //Sphere overlap, deal damage to each
        Collider[] col;
        col = Physics.OverlapSphere(transform.position, explosiveRadius);
        if(col.Length != 0)
        {
            for (int i = 0; i < col.Length; i++)
            {
                Shield opponentShield = col[i].gameObject.GetComponentInChildren<Shield>();
                if (opponentShield != null && opponentShield.shield.activeInHierarchy)
                {
                    //opponentShield.DamageShield(1f);
                }
                else
                {
                    if(col[i].gameObject.GetComponent<PlayerController>() == null)
                        col[i].gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
                }

            }
        }
        Instantiate(projectileHitEffect, transform.position, Quaternion.LookRotation(other.contacts[0].normal));
        FindObjectOfType<AudioManager>().PlayRandomPitch("Explosion", transform.position);
        Destroy(gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(transform.position, explosiveRadius);
    }
}
