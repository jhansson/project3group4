﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component alongside 'isTrigger' collision boxes to manage the OnTriggerEnter events
/// </summary>
public class MeleeHitbox : MonoBehaviour {

    [HideInInspector] public MeleeHitboxManager myManager;
    [SerializeField] private GameObject hitEffect;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Deal damage to: "+ other.gameObject.name);
        Shield opponentShield = other.gameObject.GetComponentInChildren<Shield>();
        if (opponentShield != null && opponentShield.shield.activeInHierarchy)
        {
            opponentShield.DamageShield(1f);
        }
        else
        {
            other.gameObject.SendMessage("TakeDamage", myManager.damage, SendMessageOptions.DontRequireReceiver);
            if (hitEffect != null)
                Instantiate(hitEffect, other.gameObject.transform.position, Quaternion.identity);
        }
        

    }

}
