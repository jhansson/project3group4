﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Activates hitboxes which methods are called from animation events
/// </summary>
public class MeleeHitboxManager : MonoBehaviour {

    [SerializeField, Tooltip("The hitbox we want to turn on and off, usually placed on the limbs")]
    private MeleeHitbox hitBox;

    /// <summary>
    /// How much damage we should deal when we collide with enemy entity
    /// </summary>
    [Tooltip("How much damage we should deal when we collide with enemy entity")]
    public float damage;

    public enum MeleeAttackSound { Swoosh, None };
    [SerializeField] private MeleeAttackSound meleeSound;



    private void Awake()
    {
        hitBox.myManager = this;
        hitBox.gameObject.SetActive(false);
    }

    //Add music clips here
    public void PlaySFX()
    {
        switch (meleeSound)
        {
            case MeleeAttackSound.Swoosh:
                FindObjectOfType<AudioManager>().PlayRandomPitch("MeleeAttack");
                break;
            case MeleeAttackSound.None:
                FindObjectOfType<AudioManager>().Play("None");
                break;
        }

    }

    public void OpenHitbox()
    {
        hitBox.gameObject.SetActive(true);
    }

    public void CloseHitbox()
    {
        hitBox.gameObject.SetActive(false);
    }
}
