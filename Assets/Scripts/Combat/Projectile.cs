﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(SphereCollider))]
public class Projectile : MonoBehaviour
{
    [SerializeField]
    protected float damage = 10;
  
    [SerializeField]
    protected float initialForce;

    [SerializeField]
    protected float spreadFactor;

    [SerializeField]
    protected float lifeTime = 5f;

    public float fireRate = 0.25f;

    [SerializeField, Tooltip("Particle system to spawn when projectile hit something/destroys")]
    protected GameObject projectileHitEffect;

    protected Rigidbody rb;
    protected float radius;
    protected SphereCollider sphereCollider;
    protected Vector3 direction;
    protected float timer;

    public enum ObjectSound { Apollo, Explosive, Zeus, None };
    public ObjectSound projectileSound;


    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        sphereCollider = GetComponent<SphereCollider>();
        radius = 0.4f; // transform.localScale.x * sphereCollider.radius;

        timer = Time.time;
        direction = transform.forward;
        direction.x += Random.Range(-spreadFactor, spreadFactor);
        rb.AddForce(direction * initialForce, ForceMode.Impulse);
        switch (projectileSound)
        {
            case ObjectSound.Apollo:
                FindObjectOfType<AudioManager>().PlayRandomPitch("ApolloArrow", transform.position);
                
                break;
            case ObjectSound.Explosive:
                FindObjectOfType<AudioManager>().PlayRandomPitch("ExplosiveArrow", transform.position);
                
                break;
            case ObjectSound.Zeus:
                FindObjectOfType<AudioManager>().Play("ThunderArrow");
                
                break;
            case ObjectSound.None:
                FindObjectOfType<AudioManager>().Play("None");

                break;
        }
    }

    public virtual void Update()
    {
        if (Time.time > timer + lifeTime)
        {
            Destroy(gameObject);
        }
    }

    public virtual void FixedUpdate()
    {
        PreventDeepCollision();
    }

    public virtual void OnCollisionEnter(Collision other)
    {
        Shield opponentShield = other.gameObject.GetComponentInParent<Shield>();
        if (opponentShield != null && opponentShield.shield.activeInHierarchy)
        {
            opponentShield.DamageShield(1f);
        }
        else
        {
            other.gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
        }

        Instantiate(projectileHitEffect, transform.position, Quaternion.LookRotation(other.contacts[0].normal));       
        Destroy(gameObject);
    }

    /// <summary>
    /// Prevent the projectile to go through objects
    /// </summary>
    public virtual void PreventDeepCollision()
    {
        float distanceThisFrame = rb.velocity.magnitude * Time.deltaTime;

        // If the distance is less than the diameter, just let the physics engine handle collision detection 
        if (distanceThisFrame > radius * 2)
        {
            RaycastHit hit;
            Vector3 direction = rb.velocity.normalized;
            // If we would hit something this frame, move to that position to ensure it,
            // but backed off a bit so we don't go through the collider
            if (Physics.SphereCast(transform.position, radius * 2, direction, out hit, distanceThisFrame))
            {
                rb.transform.position = hit.point;
            }
        }
    }
}
