﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    [SerializeField, Tooltip("Projectile to spawn")]
    public GameObject projectileToShoot;
    public GameObject[] projectiles;

    [SerializeField, Tooltip("Transform in world where the projectile will spawn")]
    private Transform spawnTransform;

    private float timeLastFired;

    EquipmentManager manager;

    private void Awake()
    {
        if (GetComponent<EquipmentManager>() != null)
        {
            manager = GetComponent<EquipmentManager>();
            manager.onEquipmentChanged += OnEquipmentChanged;
        }

    }

    private void OnEquipmentChanged(EQ newItem, EQ oldItem)
    {
        if(newItem != null)
            if(newItem.equipSlot == EquipmentSlot.Weapon)
            {
                projectileToShoot = projectiles[newItem.itemIndex];
            }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Shoot()
    {
        if (projectileToShoot == null)
            return;
        if (Time.time > projectileToShoot.GetComponent<Projectile>().fireRate + timeLastFired)
        {
            timeLastFired = Time.time;
            Instantiate(projectileToShoot, spawnTransform.position, spawnTransform.rotation);
        }

        
    }
}
