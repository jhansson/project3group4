﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class LevelManager : MonoBehaviour {

    public static LevelManager levelManager;
    private Animator anim;
    private string levelToLoad;
    private int sceneIndex;

    public delegate void OnLevelLoaded(int sceneIndex);
    public OnLevelLoaded onLevelLoaded;

    public delegate void OnLoadingLevel(int sceneIndex);
    public OnLoadingLevel onLoadingLevel;

    public delegate void OnRestartingLevel(int sceneIndex);
    public OnRestartingLevel onRestartingLevel;
    private void Awake()
    {
        if (levelManager != null)
            GameObject.Destroy(levelManager);
        else
            levelManager = this;

        //DontDestroyOnLoad(this);
        anim = GetComponent<Animator>();
    }

    public void RestartLevel()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        //Deletes the players equipment
        for (int i = 0; i < 4; i++)
        {
            
            string filename = "equipment" + i + ".data";
            string path = Application.persistentDataPath + "/" + filename;
            File.Delete(path);
            //PlayerPrefs.SetString(savedEquipment + i, str);
            //print("saving: " + str);
            //if (currentEquipment[i] == null)
            //    return;
            //PlayerPrefs.SetInt("ItemSlot" + i, (int)currentEquipment[i].equipSlot);
            //PlayerPrefs.SetInt("ItemIndex" + i, currentEquipment[i].itemIndex);
            //PlayerPrefs.SetInt("SpeedModifier" + i, currentEquipment[i].speedModifier);

        }
        if (onRestartingLevel != null)
            onRestartingLevel.Invoke(scene);
        LoadLevel(scene);
    }

    public void LoadNextLevel()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        scene++;
        LoadLevel(scene);
    }

    public void LoadLevel(string sceneName)
    {
        //Play fade animation
        print("got the message");
        anim.SetTrigger("FadeIn");
        levelToLoad = sceneName;
        Time.timeScale = 0.2f; //could be removed, just remember to set the fade animation to 1sec since its also affected by the timescale
    }

    public void LoadLevel(int scene)
    { 
        Time.timeScale = 1;
        sceneIndex = scene;
        anim.SetTrigger("FadeIn");
        if (onLoadingLevel != null)
            onLoadingLevel.Invoke(sceneIndex);
    }

    /// <summary>
    /// Called through an animation event, loads the level given when LoadLevel was called
    /// </summary>
    public void OnFadeComplete()
    {

        SceneManager.LoadScene(sceneIndex);
        Time.timeScale = 1f;

        anim.SetTrigger("FadeOut");
        if (onLevelLoaded != null)
            onLevelLoaded.Invoke(sceneIndex);
    }

    public void MoveObjectToScene()
    {
    }
}
