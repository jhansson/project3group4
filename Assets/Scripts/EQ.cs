﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EQ {

    public EquipmentSlot equipSlot;
    //public SkinnedMeshRenderer mesh;
    /// <summary>
    /// Corresponds to the item index inside the equipment manager, according to its equip slot
    /// </summary>
    public int itemIndex;
    //public GameObject newWeaponPrefab;

    //Add a modifier for each stat in the CharacterStats script
    public int speedModifier;

}

public enum EquipmentSlot { Head, Chest, Weapon, Feet }