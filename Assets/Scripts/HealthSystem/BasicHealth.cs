﻿using System;
using UnityEngine;
using System.Collections;
using XInputDotNetPure;

namespace EZCameraShake
{

    public class BasicHealth : MonoBehaviour, IHealth
    {
        [SerializeField, Tooltip("Prefab of visual representation if needed")]
        private GameObject healthCanvas;

        [SerializeField, Tooltip("Y offset if using health canvas")]
        private float healthCanvasYOffset = 1.5f;

        [SerializeField]
        private float maxHealth;

        [SerializeField, Tooltip("Particle System to spawn on death.")]
        private GameObject deathEffect;

        [SerializeField, Tooltip("Particle System when taking damage")]
        private GameObject yellEffect;
        [SerializeField] private Vector3 yellOffset = new Vector3(0, .5f, 0);
        private float yellTimer;
        [SerializeField, Tooltip("The time between each yell is possible")]
        private float yellCooldown = 1f;

        private Stat health;

        [SerializeField] private bool isHades = false;

        
        public enum ObjectSound { dog, cyclops, player, hades };

        public enum TakeDamageSound { Normal, None };


        [Tooltip("Choose the character death sound here")]
        public ObjectSound deathSound;

        public TakeDamageSound damageSound = TakeDamageSound.Normal;


        private float currentHealth;

        private GameObject spawnedCanvas;

        public float MaxHealth
        {
            get { return maxHealth; }
        }

        public float CurrentHealthPercentage
        {
            get { return currentHealth; }
        }
        private int numChildren;


        public event Action<float> OnHealthChanged = delegate { };
        public event Action OnDied = delegate { };

        private void Awake()
        {
            //Get reference to material for all meshes
            //set the parameter to 1
            // and then back to 0
            numChildren = transform.childCount;
            yellTimer = Time.time;
        }

        // Use this for initialization
        private void Start()
        {
            //currentHealth = health.maxValue;
            currentHealth = maxHealth;

            // Spawn health bar if wanted.
            if (healthCanvas)
            {
                spawnedCanvas = Instantiate(healthCanvas, transform.position, Quaternion.identity);
                spawnedCanvas.transform.parent = transform;
            }
        }

        private void Update()
        {
            // Keep the health bar in place
            if (spawnedCanvas)
            {
                spawnedCanvas.transform.position = transform.position + new Vector3(0, healthCanvasYOffset, 0);
            }

            if(transform.position.y < -30)
            {
                Die();
            }
        }

        private void PlayDamagedSFX()
        {
            switch (damageSound)
            {
                case TakeDamageSound.Normal:
                    FindObjectOfType<AudioManager>().PlayRandomPitch("NormalDamage");
                    break;
                case TakeDamageSound.None:
                    FindObjectOfType<AudioManager>().Play("None");
                    break;
            }
        }


        public void TakeDamage(float amount)
        {
            if (yellEffect != null && Time.time >= yellTimer + yellCooldown || isHades)
            {
                PlayDamagedSFX();
                if(yellEffect != null)
                    Instantiate(yellEffect, transform.position + yellOffset, Quaternion.identity);
                yellTimer = Time.time;
            }
            else
                PlayDamagedSFX();




            StartCoroutine("Flasher");
            // Only run if you are not already dead
            if (currentHealth <= 0)

                return;

            // Throw error if trying to deal zero or negative damage
            if (amount <= 0)
                throw new ArgumentOutOfRangeException("Invalid Damage amount specified: " + amount);


            // Subrtract the damage amount from current health
            currentHealth = (currentHealth - amount < 0f) ? 0 : currentHealth - amount;
            //health.ModifyValue((int)amount);


            // Trigger local event
            OnHealthChanged(CurrentHealthPercentage);

            // If health is 0 or below, die.
            if (CurrentHealthPercentage == 0)
            {
                Die();
                CameraShaker.Instance.Shake(CameraShakePresets.Explosion);
            }
        }

        public void AddHealth(float amount)
        {
            // Only run if you are not already full health
            if (currentHealth == maxHealth)
                return;

            // Throw error if trying to deal zero or negative damage
            if (amount <= 0)
                throw new ArgumentOutOfRangeException("Invalid Damage amount specified: " + amount);

            // Add health amount to current health
            currentHealth = (currentHealth + amount > maxHealth) ? maxHealth : currentHealth + amount;

            // Trigger local event
            OnHealthChanged(CurrentHealthPercentage);
        }

        private IEnumerator Flasher()
        {
            //Set flash to strength 1
            for (int i = 0; i < numChildren; i++)
            {
                Renderer rend = transform.GetChild(i).gameObject.GetComponent<Renderer>();

                if (rend != null)
                {
                    rend.material.SetFloat("_HitStrength", 1);
                    rend.material.SetFloat("_DamageTaken", 1 - GetHealthPercentage());
                }


            }
            yield return new WaitForSeconds(.1f);

            //Set flash to strength 0
            for (int i = 0; i < numChildren; i++)
            {
                Renderer rend = transform.GetChild(i).gameObject.GetComponent<Renderer>();

                if (rend != null)
                    rend.material.SetFloat("_HitStrength", 0);
            }
            yield return null;
        }



        private void Die()
        {
            OnDied();
            switch (deathSound)
            {
                case ObjectSound.player:
                    FindObjectOfType<AudioManager>().PlayRandomPitch("PlayerDeath", transform.position);                 
                    break;
                case ObjectSound.dog:
                    FindObjectOfType<AudioManager>().PlayRandomPitch("PlayerDeath", transform.position);
                    FindObjectOfType<AudioManager>().PlayRandomPitch("DogDeath", transform.position);                 
                    break;
                case ObjectSound.cyclops:
                    FindObjectOfType<AudioManager>().PlayRandomPitch("PlayerDeath", transform.position);
                    FindObjectOfType<AudioManager>().PlayRandomPitch("CyclopsDeath", transform.position);
                    
                    break;
                case ObjectSound.hades:
                    FindObjectOfType<AudioManager>().Play("HadesDeath");

                    break;
            }
                    Instantiate(deathEffect, transform.position, Quaternion.identity);

            // TODO: Move this to player controller and only send a message to this gameobject that you died and up to it to trigger the event.
            if(isHades)
            {
                GetComponent<Animator>().SetTrigger("DeathAnimation");
                return;
            }
            if (GetComponent<PlayerInput>())
            {
                int playerID = (GetComponent<PlayerInput>().gamepadIndex == XInputDotNetPure.PlayerIndex.One) ? 1 : 2;
                EventManager.Instance.TriggerEvent(new GameEvents.PlayerDied(playerID));
                gameObject.SetActive(false);
                LevelManager.levelManager.RestartLevel();
            }
            else
            {
                if (transform.parent != null && transform.parent.gameObject.layer == LayerMask.NameToLayer("EnemyHitbox"))
                {
                    Destroy(transform.parent.gameObject);
                } else
                {
                    Destroy(gameObject);
                }
                ControllerShaker.Instance.Shake(ControllerShakePresets.Explosion);

            }          
        }

        public float GetHealthPercentage()
        {
            return CurrentHealthPercentage / maxHealth;
        }

        public float GetMaxHealth()
        {
            return MaxHealth;
        }

        public float SetMaxHealth(float newMaxHealth)
        {
            return maxHealth = newMaxHealth;
        }

        public float GetMax()
        {
            return maxHealth;
        }


    }
}
