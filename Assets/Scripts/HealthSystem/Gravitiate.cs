﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravitiate : MonoBehaviour {

    Transform target;

	
	// Update is called once per frame
	void Update ()
    {
        if (target != null)
        {
            transform.parent.transform.position = Vector3.MoveTowards(transform.parent.transform.position, target.position, 10 * Time.deltaTime);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("Player"))
            target = other.transform;
    }


}
