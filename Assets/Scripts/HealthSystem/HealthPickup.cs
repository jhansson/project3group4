﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour {

    IHealth health;

    [SerializeField]
    private float amountOfHealth;
    


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            other.GetComponent<IHealth>().AddHealth(amountOfHealth);
            FindObjectOfType<AudioManager>().Play("CollectLife");

            Destroy(gameObject);
        }
  
    }
}
