﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    // Health system to represent
    private IHealth health;

    private Transform lookAtTarget;

    public Image fillBar;

    // The image that is used as a bar
    // TODO: Maybe better to use heart system and not a bar


    // Max width of the health bar
    private float barBaseWidth;

    // Use this for initialization
    void Start () {
        health = GetComponentInParent<IHealth>();
        health.OnHealthChanged += UpdateHealthBar;
        UpdateHealthBar(health.GetHealthPercentage());
        fillBar.fillAmount = 1;
    }

    private void UpdateHealthBar(float newHealth)
    {
        fillBar.fillAmount = health.GetHealthPercentage();
    }
}
