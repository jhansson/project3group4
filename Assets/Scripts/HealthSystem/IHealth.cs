﻿using UnityEngine;

public interface IHealth
{
    event System.Action<float> OnHealthChanged;
    event System.Action OnDied;
    void TakeDamage(float amount);
    void AddHealth(float amount);
    float GetHealthPercentage();
    float SetMaxHealth(float newMaxHealth);
    float GetMax();
}
