﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBar : MonoBehaviour {

    public IHealth health;

    public GameObject[] healthImages;
   
    public GameObject healthContainer;

	
	void Start () {
        health = FindObjectOfType<PlayerController>().GetComponent<IHealth>();
        health.OnHealthChanged += HealthUpdated;       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void HealthUpdated(float currentHealth)
    {
        for (int i = 0; i < healthImages.Length; i++)
        {
            if (i < currentHealth)
            {
                healthImages[i].SetActive(true);              
            }
            else
            {
                healthImages[i].SetActive(false);
                healthImages[i].GetComponent<Animator>().SetTrigger("PlayDecreaseHealth");
            }
        }
    }
}
