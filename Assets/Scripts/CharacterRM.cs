﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRM : MonoBehaviour {

    private Animator anim;
    private Rigidbody rb;
    private float forwardAmount;
    private float turnAmount;
    [SerializeField] private float movingTurnSpeed = 360;
    [SerializeField] private float stationaryTurnSpeed = 180;
    public float moveSpeedMultiplier = 1;
    [SerializeField] private float animSpeedMultiplier = 1;
    private bool isGrounded;
    private Vector3 groundNormal;
    private RaycastHit hitInfo;
    private Vector3 point1, point2;
    private float checkDistance;
    private float checkRadius;
    private LayerMask layerMask;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();

        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
    }

    public void Move(Vector3 move)
    {
        move = transform.InverseTransformDirection(move);
        CheckIsGrounded();
        move = Vector3.ProjectOnPlane(move, groundNormal);
        turnAmount = Mathf.Atan2(move.x, move.z);
        forwardAmount = move.z;
        ApplyExtraRotation();
        UpdateAnimator(move);
    }

    /// <summary>
    /// Helps the character to turn faster (in addition to rootmotion)
    /// </summary>
    private void ApplyExtraRotation()
    {
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
        transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
    }

    private void UpdateAnimator(Vector3 move)
    {
        anim.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        anim.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);

        if(isGrounded && move.magnitude > 0)
        {
            anim.speed = animSpeedMultiplier;
        }
        else
        {
            anim.speed = 1;
        }
    }

    public void OnAnimatorMove()
    {
        if(isGrounded && Time.deltaTime > 0)
        {
            Vector3 v = (anim.deltaPosition * moveSpeedMultiplier) / Time.deltaTime;

            v.y = rb.velocity.y;
            rb.velocity = v;
        }
    }

    private void CheckIsGrounded()
    {
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, checkDistance))
        {
            groundNormal = hitInfo.normal;
            isGrounded = true;
            anim.applyRootMotion = true;
        }
        else
        {
            isGrounded = false;
            anim.applyRootMotion = false;
            groundNormal = Vector3.up;
        }
    }
}
