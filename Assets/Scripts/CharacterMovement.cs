﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {

    #region Dash 

    public float dashSpeed = 0.6f;
    public float dashCoolDown = 2f;
    public float dashDuration = 0.2f;
    public float lerpSpeed = 2.5f;
    public float dashStartTime;
    public bool isDashing = false;
    Vector3 dashDir;
   




    #endregion

    #region rotation
    [Header("Rotation")]
    [SerializeField, Range(0.05f, .2f), Tooltip("The time it takes to turn towards where you are moving/looking")]
    private float turnSmoothTime = .1f;
    private float turnSmoothVelocity;
    private float targetRotation;
    #endregion

    #region movement
    [Header("Movement")]
    [Range(1f, 20f)]
    public float moveSpeed = 10f;
    [SerializeField, Tooltip("Defines acceleration over time")]
    private AnimationCurve accelCurve;
    [SerializeField, Range(.1f, 2f)]
    private float accelDuration = 1;
    [SerializeField, Range(.1f, 2f)]
    private float deaccelDuration = 2;

    /// <summary>
    /// Timer that controls the acceleration/decceleration curve
    /// </summary>
    private float time;
    /// <summary>
    /// Used to deccelerate the character
    /// </summary>
    private Vector3 prevMovement;
    /// <summary>
    /// More like the "downwards" velocity, used to simulate gravity
    /// </summary>
    private Vector3 velocity;

    #endregion

    #region jump & gravity
    [HideInInspector]
    public Vector3 gravity;
    [Header("Jumping")]
    [SerializeField, Tooltip("The units/meters of a regular jump"), Range(0.1f, .5f)]
    private float minJumpHeight = .5f;
    [SerializeField, Tooltip("The units/meters of a long jump"), Range(0.5f, 2f)]
    private float maxJumpHeight = .5f;
    private float maxJumpVelocity;
    private float minJumpVelocity;
    public static float globalGravity = -9.81f;
    private bool hasJumped = false;
    //private GroundChecker groundChecker;
    [SerializeField, Tooltip("Multiplies gravity when falling"), Range(1f, 3f)]
    private float fallMultiplier = 2.5f;
    [HideInInspector, Range(-0.01f, -0.05f)]
    private float velocitySensitivity = -0.05f;
    //[SerializeField, Tooltip("The gravity scale used when 'long jumping'"), Range(.5f, 5f)]
    //private float lowJumGravMultiplier = 3f;
    #endregion

    #region groundchecker
    private bool isGrounded;
    [Header("GroundChecker")]
    [SerializeField, Tooltip("The layer we look for to reset our jump")]
    private LayerMask layerMask;

    [SerializeField, Range(0.4f, 0.6f)]
    private float checkRadius = 0.5f;

    [SerializeField, Range(.55f, 1f), Tooltip("Changes only shows in play mode")]
    private float checkDistance = .5f;

    private Vector3 point1, point2;
    #endregion

    private CharacterController charController;

    private void Awake()
    {
        charController = GetComponent<CharacterController>();
        //groundChecker = GetComponent<GroundChecker>();
        gravity = globalGravity * Vector3.up;

    }

    private void FixedUpdate()
    {
        Dash();
    }

    void Update()
    {
        moveSpeed = GetComponent<PlayerStats>().speed.GetValue();
        CheckIsGrounded();
        SetGravityScale();
        velocity += gravity * Time.deltaTime;
        charController.Move(velocity * Time.deltaTime);

    }

    /// <summary>
    /// Moves the character towards a direction, uses acceleration and decceleration
    /// </summary>
    /// <param name="movement"></param>
    public void Move(Vector3 movement)
    {
        if (movement != Vector3.zero)
        {
            //accelerate movement based on curve
            if (time < accelDuration)
            {
                time += Time.deltaTime;
                movement *= accelCurve.Evaluate(time / accelDuration);
            }
            else
            {
                time = accelDuration;
            }
        }
        else
        {
            //decceleration
            if (time <= 0)
            {
                time = 0;
            }
            else
            {
                time -= Time.deltaTime;
            }
            movement = prevMovement;
            movement *= accelCurve.Evaluate(time / deaccelDuration);
        }
        prevMovement = movement; //used for deacceleration
                                 //velocity += (movement * Time.deltaTime);
        charController.Move((movement * moveSpeed) * Time.deltaTime);
    }

    /// <summary>
    /// Rotates the character toward a direction
    /// </summary>
    /// <param name="direction"></param>
    public void RotateTowards(Vector3 direction)
    {
        targetRotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(
            transform.eulerAngles.y,
            targetRotation,
            ref turnSmoothVelocity,
            turnSmoothTime);
    }
    #region jumping
    public void Jump()
    {
        if (!hasJumped && isGrounded)
        {
            maxJumpVelocity = Mathf.Sqrt(-2 * gravity.y * maxJumpHeight);
            velocity.y = maxJumpVelocity;
            hasJumped = true;
        }
    }

    /// <summary>
    /// Stops the character from ascending and begins falling 
    /// </summary>
    public void JumpReleased()
    {
        if (velocity.y > minJumpVelocity)
        {
            minJumpVelocity = Mathf.Sqrt(-2 * gravity.y) * minJumpHeight;
            velocity.y = minJumpVelocity;
        }
    }
    #endregion

    /// <summary>
    /// Increases gravity after a jump
    /// </summary>
    private void SetGravityScale()
    {
        //not working currently
        if (velocity.y < velocitySensitivity && hasJumped) //means we have jumped, therefore fast fall
        {
            gravity = CharacterMovement.globalGravity * fallMultiplier * Vector3.up;
        }
        else if (isGrounded) //we are touching ground, normal gravity and reset jump
        {
            gravity = CharacterMovement.globalGravity * Vector3.up;
            hasJumped = false;
        }
        else //we are falling like normal
        {
            gravity = CharacterMovement.globalGravity * Vector3.up;
        }
    }

    /// <summary>
    /// Sphere casts downward to see if it hits the ground layer
    /// </summary>
    private void CheckIsGrounded()
    {
        RaycastHit hit;
        point1 = transform.position;
        point2 = point1 + Vector3.down * checkDistance;
        isGrounded = Physics.SphereCast(point1, checkRadius, Vector3.down, out hit, checkDistance, layerMask);
    }

    //void OnDrawGizmos()
    //{
    //    Gizmos.DrawSphere(point1, checkRadius);
    //    Gizmos.DrawSphere(point2, checkRadius);
    //}

    public void AttemptDash()
    {
        if (Time.time > dashStartTime + 1f)
        {
            dashStartTime = Time.time;
            print("dash");
            dashDir += prevMovement.normalized * dashSpeed;
            FindObjectOfType<AudioManager>().Play("Dash1");
        }
    }

    private void Dash()
    {


        if (dashStartTime != 0 && Time.time < dashStartTime + 0.4f)
        {
            isDashing = true;
            print("should sprinmt");
            charController.Move(dashDir);
        }
        dashDir = Vector3.Lerp(dashDir, Vector3.zero, lerpSpeed * Time.deltaTime);


    }
   
}
