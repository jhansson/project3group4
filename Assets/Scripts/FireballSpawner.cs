﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballSpawner : MonoBehaviour
{

    private Vector3 center;
    public GameObject player;
    [SerializeField] public float heighOffset = 25;
    public Vector3 size = new Vector3(15, 15, 15);
    public GameObject fireball;
    private float spawnTimer;
    [SerializeField] private float spawnCooldown = .6f;
    [SerializeField] private AudioClip spawnFireballLaughter;
    private AudioSource source;
    private bool hasLaughed;

    private void Awake()
    {
        spawnTimer = Time.time;
        source = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        if(!hasLaughed)
        {
            hasLaughed = true;
            source.clip = spawnFireballLaughter;
            source.Play();
        }

    }

    private void Update()
    {
        if (player == null)
            return;
        center = player.transform.position;
        center.y += heighOffset;
        if (Time.time >= spawnTimer + spawnCooldown)
            SpawnFireball();
    }

    public void SpawnFireball()
    {
        spawnTimer = Time.time;
        Vector3 pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));
        Instantiate(fireball, pos, Quaternion.identity);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 1, 1, 0.5f);
        Gizmos.DrawCube(center, size);
    }
}