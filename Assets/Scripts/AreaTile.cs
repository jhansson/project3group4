﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public enum AreaType
{
    EnemyArea,
    LootArea,
    BossArea
}

public class AreaTile : MonoBehaviour
{   
    [Header("General")]
    public AreaType AreaType;
    [SerializeField, Tooltip("Points where exit prefabs will be spawned.")]
    private GameObject[] exitBlockers;

    [Header("Enemies")]    
    [Tooltip("Points where enemies can spawn from.")]
    public Transform[] EnemySpawnPoints;
    [SerializeField, Tooltip("A reference to the statue on the tile if one exists.")]
    private GameObject turretStatueReference;
    [SerializeField, Range(0, 100), Tooltip("What is the chance in percentage of the statue becoming an enemy.")]
    private int chanceOfStatueComingToLife;

    [Header("Chests")]
    [Range(0, 100), Tooltip("Chance of this area spawning a chest. Set in percentage.")]
    public int ChanceOfSpawningChest;
    [Tooltip("Points where items/chests can be placed.")]
    public Transform[] ItemSpawnPoints;

    [HideInInspector]
    public List<GameObject> Enemies;

    // If the room has been entered before
    [HideInInspector]
    public bool PlayerHasBeenHereBefore;

    private bool playerIsInsideTile;

    private BoxCollider areaTrigger;

    [HideInInspector]
    public bool SpawnChest;

    [HideInInspector]
    public List<GameObject> SpawnedChests;

    [HideInInspector]
    public Vector2 PositionInGrid = Vector2.zero;

    [HideInInspector]
    public GameObject AreaMist;

    public AudioMixerSnapshot standardMusicSnap;
    public AudioMixerSnapshot battleMusicSnap;
    

    private Camera cam;
    private Vector3 targetCam;
    private Vector3 battleCam;
    private Vector3 exploreCam;


    private void Awake()
    {
        Enemies = new List<GameObject>();

        // If chest will be spawned or not
        SpawnChest = Random.Range(0, 100) < ChanceOfSpawningChest ? true : false;

        // If statue should become an enemy or not
        if (turretStatueReference != null)
        {
            if (Random.Range(0, 100) < chanceOfStatueComingToLife)
            {
                // Call a function on the statue that it should be dangerous.
                
            }
        }        
    }

    // Use this for initialization
    private void Start ()
    {
        areaTrigger = gameObject.AddComponent<BoxCollider>();
        areaTrigger.size = new Vector3(27f, 4f, 27f);
        areaTrigger.isTrigger = true;

        cam = Camera.main;

        foreach (GameObject exit in exitBlockers)
        {
            if (exit != null)
            {
                exit.SetActive(false);
            }
        }

        foreach (GameObject chest in SpawnedChests)
        {
            if (chest != null)
            {
                chest.SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
    private void Update ()
    {
        if (exitBlockers[0] != null && exitBlockers[0].activeInHierarchy)
        {
            for (int i = Enemies.Count - 1; i >= 0; i--)
            {
                if (Enemies[i] == null)
                    Enemies.RemoveAt(i);
            }
        }

		if (Enemies.Count <= 0 && (exitBlockers[0] != null && exitBlockers[0].activeInHierarchy))
        {
            AllEnemiesDead();
        }
	}

    private void SpawnExitBlockers()
    {
        foreach (GameObject exit in exitBlockers)
        {

            exit.SetActive(true);
        }
        
        battleMusicSnap.TransitionTo(0.01f);
    }

    private void ActivateEnemies()
    {
        if(turretStatueReference != null)
            turretStatueReference.GetComponent<AIController>().turretActive = true;

        foreach (GameObject enemy in Enemies)
        {          
            enemy.SetActive(true);
        }

        // If there are still enemies here
        if (Enemies.Count > 0)
        {
            // Change to battle cam.
            cam.GetComponent<SplitScreen>().targetCam = cam.GetComponent<SplitScreen>().battleCam;

            // Block the exits.
            SpawnExitBlockers();
        } else
        {
            // Activate all the chests if there are no enemies in this area
            foreach (GameObject chest in SpawnedChests)
            {
                chest.SetActive(true);
            }
        }
    }

    private void AllEnemiesDead()
    {
        
        standardMusicSnap.TransitionTo(0.01f);
        // Deactivate all the blockers and initiate OutOfCombat music
        foreach (GameObject exit in exitBlockers)
        {
            //CombatMusicControl.instance.OutOfCombat();
            if (exit != null)
            {
                exit.SetActive(false);  
            }
        }

        // Activate all the chests
        foreach (GameObject chest in SpawnedChests)
        {
            if (chest != null)
            {
                chest.SetActive(true);
            }
        }

        // Set back to explore cam mode
        cam.GetComponent<SplitScreen>().targetCam = cam.GetComponent<SplitScreen>().exploreCam;
    }

    /// <summary>
    /// Runs every time something enters the large trigger in the area.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        // If the player enter the area and haven't been here before
        if (other.CompareTag("Player") && !PlayerHasBeenHereBefore)
        {            
            PlayerHasBeenHereBefore = true;
            ActivateEnemies();

            if (AreaMist != null)
            {
                AreaMist.GetComponent<ParticleSystem>().Stop();
            }
        }

        if (other.CompareTag("Player"))
        {
            EventManager.Instance.TriggerEvent(new GameEvents.PlayerEnteredArea(PositionInGrid));
        }
    }
}
