﻿using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;
using UnityEngine;

public class ControllerManager : MonoBehaviour{

    private static ControllerManager instance = null;
    public GameObject player1, player2;

    private bool[] controllerAssigned = new bool[2];
    private ControllerData[] controllerData = new ControllerData[2];
    private GamePadState[] state = new GamePadState[4];
    private GamePadState[] prevState = new GamePadState[4];
    private PlayerIndex gamepadIndex;

    public static ControllerManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType(typeof(ControllerManager)) as ControllerManager;
            }
            return instance;
        }
    }

    private void Update()
    {
        #region old
        //for (int i = 0; i < state.Length; i++)
        //{
        //    gamepadIndex = (PlayerIndex)i;
        //    prevState[i] = state[i];
        //    state[i] = GamePad.GetState(gamepadIndex);
        //    if(!state[i].IsConnected) { return; } //if controller is not connected then return

        //    //if A button was pressed
        //    if(state[i].Buttons.A == ButtonState.Pressed && prevState[i].Buttons.A == ButtonState.Released)
        //    {
        //        for (int n = 0; n < controllerData.Length; n++)
        //        {
        //            if(!controllerData[n].isAssigned)
        //            {
        //                controllerData[i].gamepadIndex = gamepadIndex;
        //                controllerData[i].isAssigned = true;
        //                EnablePlayer();
        //                return;
        //            }
        //        }
        //    }
        //}
        #endregion

        gamepadIndex = (PlayerIndex)1;
        prevState[1] = state[1];
        state[1] = GamePad.GetState(gamepadIndex);
        if(state[1].Buttons.A == ButtonState.Pressed && prevState[1].Buttons.A == ButtonState.Released)
        {
            if(player2 !=  null)
            {
                if (!player2.activeInHierarchy)
                {
                    player2.SetActive(true);
                }
            }


        }
    }

    private void EnablePlayer()
    {
        if(!player1.activeInHierarchy)
        {
            player1.SetActive(true);
            //EventManager.Instance.TriggerEvent(new GameEvents.PlayerActivated(1));
        }
        else if(!player2.activeInHierarchy)
        {
            player2.SetActive(true);
            //EventManager.Instance.TriggerEvent(new GameEvents.PlayerActivated(2));
        }
    }

    public ControllerData RequestController()
    {
        for (int i = 0; i < controllerData.Length; i++)
        {
            if (controllerData[i].isAssigned)
            {
                return controllerData[i];
            }
        }
        ControllerData emptyData;
        emptyData.isAssigned = false;
        emptyData.gamepadIndex = (PlayerIndex)0;
        return emptyData;
    }
}
