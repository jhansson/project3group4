﻿namespace GameEvents
{
    public class ButtonPressed : GameEvent
    {
        public float TimePressed { get; set; }

        public ButtonPressed(float timePressed)
        {
            TimePressed = timePressed;
        }
    }
}
