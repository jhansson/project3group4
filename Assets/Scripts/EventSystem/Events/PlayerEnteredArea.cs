﻿using UnityEngine;
namespace GameEvents
{
    public class PlayerEnteredArea : GameEvent
    {
        public Vector2 AreaPositionInGrid { get; set; }

        public PlayerEnteredArea(Vector2 areaPositionInGrid)
        {
            AreaPositionInGrid = areaPositionInGrid;
        }
    }
}
