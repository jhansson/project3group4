﻿ namespace GameEvents
    {
        public class PlayerHealthChange : GameEvent
        {
            public PlayerID Player { get; private set; }
            public float NewHealth { get; private set; }

            public PlayerHealthChange(PlayerID player, float newHealth)
            {
                Player = player;
                NewHealth = newHealth;
                

            }
        }
    }

