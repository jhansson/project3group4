﻿namespace GameEvents
{
    /// <summary>
    /// Trigger this event whenever a player dies.
    /// </summary>
    public class PlayerDied : GameEvent
    {
        public int PlayerID { get; set; }

        public PlayerDied(int player)
        {
            PlayerID = player;
        }
    }
}
