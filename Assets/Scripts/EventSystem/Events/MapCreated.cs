﻿namespace GameEvents
{
    public class MapCreated : GameEvent
    {
        public AreaConnectionType[,] MapGrid { get; set; }

        public MapCreated(AreaConnectionType[,] mapGrid)
        {
            MapGrid = mapGrid;
        }
    }
}
