﻿namespace GameEvents
{
    /// <summary>
    /// Trigger this event whenever a player controller is activated.
    /// Ex. player 2 drop in.
    /// </summary>
    public class PlayerActivated : GameEvent
    {
        public int PlayerID { get; set; }

        public PlayerActivated(int player)
        {
            PlayerID = player;
        }
    }
}
