﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIRoam : StateMachineBehaviour {

    private AIController entity;
    private NavMeshAgent agent;
    private Animator anim;
    private Vector3 startPos;
    [HideInInspector]
    public Vector3 targetPos;
    [SerializeField]
    private float roamDistance = 5f;
    private float waitTime = 5;
    private float timer = 0;
    private float currentVertical;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        entity = animator.gameObject.GetComponent<AIController>();
        agent = animator.gameObject.GetComponent<NavMeshAgent>();
        anim = animator;

        startPos = agent.transform.position;
        NewTargetPosition();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float vertical = Vector3.Distance(entity.transform.position, targetPos);

        vertical = Mathf.Clamp(vertical, 0, .25f);
        if(vertical <= 0.5f)
        {
            //vertical = 0;
            vertical = Mathf.SmoothDamp(vertical, 0, ref currentVertical, 0.1f);
        }
        anim.SetFloat("Vertical", vertical);
        if(Vector3.Distance(agent.transform.position, targetPos) <= .5f)
        {
            //TODO fix so that no animation is playing while just standing around(vertical should be 0)
            if(timer == 0)
            {
                timer = Time.time;
            }
            else if(Time.time > timer + waitTime)
            {
                NewTargetPosition();
            }
        }
    }

    /// <summary>
    /// Chooses a new position to walk towards within an area around the AI's current position
    /// </summary>
    private void NewTargetPosition()
    {
        timer = 0;
        targetPos = startPos;
        Vector2 randomPos = Random.insideUnitCircle * roamDistance;
        targetPos.x += randomPos.x;
        targetPos.z += randomPos.y;

        agent.SetDestination(targetPos);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
