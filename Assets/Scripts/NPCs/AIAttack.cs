﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAttack : StateMachineBehaviour {

    private AIController entity;
    private GameObject target;
    private Vector3 targetPos;
    private Vector3 targetDirection;
    [SerializeField]
    private float attackRange = 9f;
    private bool canAttack;
    [SerializeField]
    private float attackCooldown = 2;
    private float timer;
    [SerializeField]
    private float strafeDistance = 2;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        entity = animator.gameObject.GetComponent<AIController>();
        target = entity.target;
        timer = Time.time;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        targetPos = target.transform.position;
        if (!canAttack)
        {
            UpdateCooldown();
        }
        //DetermineTargetPosition();
        CalculateTargetDirection(targetPos);
        HandleRotation();
        MoveTowardsTarget();
    }

    private void MoveTowardsTarget()
    {
        if(Vector3.Distance(targetPos, entity.transform.position) <= attackRange && canAttack)
        {
            Attack();
            FindObjectOfType<AudioManager>().Play("EnemyAttack");
        }
        else if(!canAttack)
        {

        }
        else
        {
            entity.CharMovement.Move(targetDirection);
        }
    }

    private void HandleRotation()
    {
        entity.CharMovement.RotateTowards(targetDirection);
    }

    private void DetermineTargetPosition()
    {
            if (canAttack)
            {
                targetPos = target.transform.position;
            }
            else
            {
                NewTargetPosition();
            }


    }

    private void NewTargetPosition()
    {
        Vector3 startPos = entity.transform.position;
        targetPos = startPos;
        Vector2 randomPos = Random.insideUnitCircle * strafeDistance;
        targetPos.x += randomPos.x;
        targetPos.z += randomPos.y;
        CalculateTargetDirection(targetPos);
    }

    private void CalculateTargetDirection(Vector3 pos)
    {
        targetDirection = pos - entity.transform.position;
        targetDirection = targetDirection.normalized * Mathf.Clamp01(targetDirection.magnitude);
    }

    private void UpdateCooldown()
    {
        if (Time.time > timer + attackCooldown)
        {
            canAttack = true;
        }
    }

    private void Attack()
    {
        entity.ProjectileShooter.Shoot();
        timer = Time.time;
        canAttack = false;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
