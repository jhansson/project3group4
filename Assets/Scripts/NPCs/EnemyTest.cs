﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTest : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        GetComponent<IHealth>().OnHealthChanged += HealthChanged;
        GetComponent<IHealth>().OnDied += OnDeath;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void TakeDamage(float amount)
    {
        //GetComponent<IHealth>().TakeDamage(amount);        
    }

    void HealthChanged(float currentHealth)
    {
        Debug.Log("Health: " + currentHealth);
    }

    void OnDeath()
    {
        Debug.Log("I died");
    }
}
