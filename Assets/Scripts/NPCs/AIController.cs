﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    public CharacterMovement CharMovement { get; private set; }
    public Animator FSM { get; private set; }
    public ProjectileShooter ProjectileShooter { get; private set; }
    public GameObject target;
    public float minSize, maxSize = 1;
    private float size;
    public float chaseSpeed = 2;
    public float attackRange = 1;
    public float attackDelay = 1;
    [SerializeField, Tooltip("If the AI is an turret, check this box")]
    private bool isTurret;
    [SerializeField, Tooltip("Debug, used to activate the turret from area generation")]
    public bool turretActive;

    [SerializeField] private Collider aggroCollider;

    [SerializeField] private GameObject[] loot;
    [SerializeField] private float dropRate;

    private IHealth health;

    private void Awake()
    {
        CharMovement = GetComponent<CharacterMovement>();


        FSM = GetComponent<Animator>();
        ProjectileShooter = GetComponent<ProjectileShooter>();
        health = GetComponentInParent<IHealth>();
        if (health != null)
        {
            health.OnDied += OnDie;
        }        

        if (!isTurret)
        {
            size = Random.Range(minSize, maxSize);
            chaseSpeed /= size;
            gameObject.transform.localScale *= size;
            attackRange *= size;
            float newMaxHealth = health.GetMax();
            newMaxHealth *= size;
            health.SetMaxHealth(newMaxHealth);
        }

        if (isTurret)
            aggroCollider.enabled = turretActive;


    }

    private void OnTriggerEnter(Collider other)
    {
        if (isTurret)
        {
            if (turretActive)
                CheckStartAttacking(other);
        }
        else
            CheckStartAttacking(other);
    }

    private void CheckStartAttacking(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (aggroCollider != null)
                aggroCollider.enabled = false;
            target = other.gameObject;
            //FSM.SetBool("attacking", true);
            FSM.SetBool("Chase", true);
            FSM.SetBool("TurnOn", true);
        }
    }

    private void OnDie()
    {
        for (int i = 0; i < loot.Length; i++)
        {
            float rand = Random.Range(0f, 1f);
            if (rand <= dropRate)
            {
                int randItem = loot.Length;
                randItem = Random.Range(0, randItem);
                Instantiate(loot[randItem], transform.position, Quaternion.identity);
                break;
            }
        }

    }

    private void Update()
    {
        if (!isTurret)
            return;

        aggroCollider.enabled = turretActive;
    }
}
