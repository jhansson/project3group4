﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPatrol : StateMachineBehaviour {

    private AIController entity;
    private Vector3 startPos;
    private Vector3 targetPos;
    private Vector3 targetDirection;
    [SerializeField]
    private float roamDistance = 5;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        entity = animator.gameObject.GetComponent<AIController>();
        if(startPos == Vector3.zero)
        {
            startPos = entity.transform.position;
        }
        NewTargetPosition();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        MoveToTarget();
        HandleRotation();
    }

    private void NewTargetPosition()
    {
        targetPos = startPos;
        Vector2 randomPos = Random.insideUnitCircle * roamDistance;
        targetPos.x += randomPos.x;
        targetPos.z += randomPos.y;
        CalculateTargetDirection(targetPos);
    }

    private void CalculateTargetDirection(Vector3 pos)
    {
        targetDirection = pos - entity.transform.position;
        targetDirection = targetDirection.normalized * Mathf.Clamp01(targetDirection.magnitude);
    }

    private void MoveToTarget()
    {
        if(Vector3.Distance(targetPos, entity.transform.position) <= 1)
        {
            NewTargetPosition();
        }
        else
        {
            entity.CharMovement.Move(targetDirection);
        }
    }

    private void HandleRotation()
    {
        entity.CharMovement.RotateTowards(targetDirection);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
