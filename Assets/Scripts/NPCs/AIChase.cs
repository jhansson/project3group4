﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIChase : StateMachineBehaviour {

    private AIController entity;
    private NavMeshAgent agent;
    private Animator anim;

    [HideInInspector] public Transform target;
    private float giveUpDistance = 20;
    private float distanceToTarget;
    private float currentVertical;
    private float vertical;
    private float timer;
    private bool canAttack;
    private float targetRotation;
    private float turnSmoothVelocity;
    private float turnSmoothTime = .1f;
    private Vector3 previousTargetPos;
    [SerializeField, Tooltip("How far the AI will predict you based on your current velocity")]
    private float pursuitLength = 1.5f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        anim = animator;
        entity = animator.gameObject.GetComponent<AIController>();
        agent = animator.gameObject.GetComponent<NavMeshAgent>();
        agent.enabled = true;
        anim.SetBool("Attack", false);
        timer = Time.time;
        target = entity.target.transform;
        agent.isStopped = false;
        agent.stoppingDistance = entity.attackRange;
        agent.speed = entity.chaseSpeed;
        agent.SetDestination(target.position);

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        distanceToTarget = Vector3.Distance(agent.transform.position, target.position);
        Vector3 targetVel = (target.position - previousTargetPos) / Time.deltaTime;
        targetVel.Normalize();
        Vector3 futurePos = target.position + targetVel * pursuitLength;
        canAttack = (Time.time >= timer + entity.attackDelay);
        RotateTowards((target.position - agent.transform.position).normalized);
        if (distanceToTarget <= entity.attackRange)
        {
            vertical = Mathf.SmoothDamp(vertical, 0, ref currentVertical, 0.1f);
            Debug.Log(entity.gameObject.name + " : I've reached my attack range");
            agent.enabled = false;
            if (canAttack)
            {
                anim.SetBool("Attack", true);
            }

            //Attack towards target positon
            //TODO probably do a attack state, where the agent is standing still, rotating towards the target
        }
        //else if(distanceToTarget >= giveUpDistance)
        //{
        //    Debug.Log(entity.gameObject.name + " : I give up");
        //    anim.SetBool("Chase", false);
        //    //Give up, back to roam
        //}
        else
        {
            vertical = distanceToTarget;
            agent.enabled = true;
            agent.SetDestination(futurePos);
            Debug.DrawLine(target.position, futurePos);
        }

        vertical = Mathf.Clamp(vertical, 0, 1f);

        anim.SetFloat("Vertical", vertical);
        previousTargetPos = target.position;
    }

    public void RotateTowards(Vector3 direction)
    {
        targetRotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        agent.transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(
            agent.transform.eulerAngles.y,
            targetRotation,
            ref turnSmoothVelocity,
            turnSmoothTime);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
