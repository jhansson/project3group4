﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueScript : MonoBehaviour
{

    [SerializeField]
    int numberOfProjectiles;

    [SerializeField]
    GameObject projectile;

    [SerializeField]
    GameObject Spawner;

    Transform spawnPoint;

    float radius, moveSpeed;

    // Use this for initialization
    void Start()
    {
        radius = 5f;
        moveSpeed = 2f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            spawnPoint = Spawner.transform;
            SpawnProjectiles(numberOfProjectiles);
        }
    }

    void SpawnProjectiles(int numberOfProjectiles)
    {
        float angleStep = 360f / numberOfProjectiles;
        float angle = 0f;

        for (int i = 0; i <= numberOfProjectiles - 1; i++)
        {

            float projectileDirXposition = spawnPoint.position.x + Mathf.Sin((angle * Mathf.PI)) * radius;
            float projectileDirZposition = spawnPoint.position.z + Mathf.Cos((angle * Mathf.PI)) * radius;

            Vector3 projectileVector = new Vector3(projectileDirXposition, 0, projectileDirZposition);
            Vector3 projectileMoveDirection = (projectileVector - spawnPoint.position) * moveSpeed;

            var proj = Instantiate(projectile, spawnPoint.position, Quaternion.identity);
            proj.GetComponent<Rigidbody>().velocity = new Vector3(projectileMoveDirection.x, 0 ,projectileMoveDirection.y);

            angle += angleStep;
        }
    }

}
