﻿using UnityEngine;
using XInputDotNetPure;

public class PlayerInput : MonoBehaviour {

    public InputMethod inputMethod = InputMethod.Gamepad;
    /// <summary>
    /// The number of the plugged in controller
    /// </summary>
    [Tooltip("The number of the plugged in controller")]
    public PlayerIndex gamepadIndex;
    private GamePadState state;
    private GamePadState prevState;
    private ControllerData controllerData;
    //[SerializeField, Tooltip("Used for debug: When enabled it bypasses the controller manager and automatically sets its controller to the selected index")]
    //private bool autoAssign;

    #region movement and looking floats

    /// <summary>
    /// Get the movement horizontal axis of assigned gamepad
    /// </summary>
    public float MoveHorizontal { get; private set; }

    /// <summary>
    /// Get the movement vertical axis of assigned gamepad
    /// </summary>
    public float MoveVertical { get; private set; }

    /// <summary>
    /// Get the look horizontal axis of assigned gamepad
    /// </summary>
    public float LookHorizontal { get; private set; }

    /// <summary>
    /// Get the look vertical axis of assigned gamepad
    /// </summary>
    public float LookVertical { get; private set; }

    #endregion

    #region bool button presses

    /// <summary>
    /// Get if Jump has been pressed
    /// </summary>
    public bool JumpButton { get; private set; }

    /// <summary>
    /// Get if Jump has been released
    /// </summary>
    public bool JumpButtonReleased { get; private set; }

    /// <summary>
    /// Get if Jump is held
    /// </summary>
    public bool JumpButtonHold { get; private set; }

    /// <summary>
    /// Get if Cancel has been pressed
    /// </summary>
    public bool CancelButton { get; private set; }

    /// <summary>
    /// Get if Cancel has been released
    /// </summary>
    public bool CancelButtonReleased { get; private set; }

    /// <summary>
    /// Get if Cancel is held
    /// </summary>
    public bool CancelButtonHold { get; private set; }

    /// <summary>
    /// Get if Shoot has been pressed
    /// </summary>
    public bool ShootButton { get; private set; }

    /// <summary>
    /// Get if Shoot has been released
    /// </summary>
    public bool ShootButtonReleased { get; private set; }

    /// <summary>
    /// Get if Shoot is held
    /// </summary>
    public bool ShootButtonHold { get; private set; }

    /// <summary>
    /// Get if Shoot has been pressed
    /// </summary>
    public bool DashButton { get; private set; }

    /// <summary>
    /// Get if Shoot has been released
    /// </summary>
    public bool DashButtonReleased { get; private set; }

    /// <summary>
    /// Get if Shoot is held
    /// </summary>
    public bool DashButtonHold { get; private set; }

    #endregion

    private void Awake()
    {
        
    }

    private void OnEnable()
    {
        EventManager.Instance.TriggerEvent(new GameEvents.PlayerActivated((int)gamepadIndex));
        //if(autoAssign)
        //{
        //    controllerData.isAssigned = true;
        //    return;
        //}
        //if(ControllerManager.Instance != null)
        //{
        //    controllerData = ControllerManager.Instance.RequestController();
        //    gamepadIndex = controllerData.gamepadIndex;
        //}

    }

    private void Start()
    {
        EventManager.Instance.TriggerEvent(new GameEvents.PlayerActivated((int)gamepadIndex));
    }

    private void Update()
    {
        switch (inputMethod)
        {
            case InputMethod.Gamepad:
                UpdateGamepad();
                break;
            case InputMethod.Keyboard:
                UpdateKeyboard();
                break;
        }
    }

    /// <summary>
    /// Updates the gamepad controls
    /// </summary>
    private void UpdateGamepad()
    {
        //if(!controllerData.isAssigned) { return; }

        prevState = state;
        state = GamePad.GetState(gamepadIndex);

        MoveHorizontal = state.ThumbSticks.Left.X;
        MoveVertical = state.ThumbSticks.Left.Y;

        LookHorizontal = state.ThumbSticks.Right.X;
        LookVertical = state.ThumbSticks.Right.Y;

        JumpButton = (state.Buttons.A == ButtonState.Pressed && prevState.Buttons.A == ButtonState.Released);
        JumpButtonReleased = (state.Buttons.A == ButtonState.Released && prevState.Buttons.A == ButtonState.Pressed);
        JumpButtonHold = (state.Buttons.A == ButtonState.Pressed);

        ShootButton = (state.Triggers.Right == 1 && prevState.Triggers.Right != state.Triggers.Right);
        ShootButtonReleased = (state.Triggers.Right == 0 && prevState.Triggers.Right != 0);
        ShootButtonHold = (state.Triggers.Right != 0);

        DashButton = (state.Triggers.Left == 1 && prevState.Triggers.Left != state.Triggers.Left);
        DashButtonReleased = (state.Triggers.Left == 0 && prevState.Triggers.Left != 0);
        DashButtonHold = (state.Triggers.Left != 0);
    }

    /// <summary>
    /// Updates the keyboard controls
    /// </summary>
    private void UpdateKeyboard()
    {
        MoveHorizontal = Input.GetAxisRaw("MoveHorizontal");
        MoveVertical = Input.GetAxisRaw("MoveVertical");

        LookHorizontal = Input.GetAxisRaw("LookHorizontal");
        LookVertical = Input.GetAxisRaw("LookVertical");

        JumpButton = Input.GetButtonDown("Jump");
        JumpButtonReleased = Input.GetButtonUp("Jump");
        JumpButtonHold = Input.GetButton("Jump");

        ShootButton = Input.GetButtonDown("Fire1");
        ShootButtonReleased = Input.GetButtonUp("Fire1");
        ShootButtonHold = Input.GetButton("Fire1");

    }
}

/// <summary>
/// Changes the input to handle either gamepad or keyboard
/// </summary>
public enum InputMethod
{
    Keyboard,
    Gamepad
}
