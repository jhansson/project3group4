﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZCameraShake
{

    public class PlayerMovement : StateMachineBehaviour
    {
        private Camera camera;
        private PlayerController entity;
        private ProjectileShooter projectileShooter;
        [SerializeField] private LayerMask pickupLayerMask;
        [SerializeField] private float pickupReach = .25f;

        private Vector3 moveDir;
        private Vector3 lookDir;
        /// <summary>
        /// The X direction the camera is pointing towards
        /// </summary>
        private Vector3 camForward, camRight;

        private Vector3 moveForward, moveRight;
        private Vector3 lookForward, lookRight;

        // Accessing animator component to set parameters for blendtree
        private Animator anim;

        private float shootWeight = 0;
        [SerializeField] private float weightSpeed = 8;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            camera = Camera.main;
            entity = animator.gameObject.GetComponent<PlayerController>();
            projectileShooter = entity.GetComponent<ProjectileShooter>();

            // Setting the animator component to the one active on player,
            anim = animator;
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {

            CalculateCameraDirection();
            HandleMovement();
            HandleRotation();
            HandleJumping();
            HandleShooting();
            HandleAnimation();
            HandleDash();
            HandleInteract();
        }

        private void HandleMovement()
        {
            if (entity.PlayerInput.MoveVertical >= 0.25f || entity.PlayerInput.MoveVertical <= -.25f)
                moveForward = camForward * entity.PlayerInput.MoveVertical;
            else
                moveForward *= 0;

            if (entity.PlayerInput.MoveHorizontal >= 0.25f || entity.PlayerInput.MoveHorizontal <= -.25f)
                moveRight = camRight * entity.PlayerInput.MoveHorizontal;
            else
                moveRight *= 0;

            moveDir = moveForward + moveRight;

            moveDir = moveDir.normalized * Mathf.Clamp01(moveDir.magnitude);

            entity.CharMovement.Move(moveDir);
        }

        private void HandleRotation()
        {
            lookForward = camForward * entity.PlayerInput.LookVertical;
            lookRight = camRight * entity.PlayerInput.LookHorizontal;

            lookDir = lookForward + lookRight;

            lookDir.Normalize();

            //rotate towards look input
            if (lookDir != Vector3.zero)
            {
                entity.CharMovement.RotateTowards(lookDir);
            }
            else if (moveDir != Vector3.zero)
            {
                entity.CharMovement.RotateTowards(moveDir);
            }
        }

        private void HandleJumping()
        {
            if (entity.PlayerInput.JumpButton)
            {
                entity.CharMovement.Jump();
            }
            if (entity.PlayerInput.JumpButtonReleased)
            {
                entity.CharMovement.JumpReleased();
            }
        }

        private void HandleShooting()
        {
            if (entity.PlayerInput.ShootButtonHold)
            {
                //if(entity.PlayerInput.inputMethod == InputMethod.Gamepad && projectileShooter.projectileToShoot != null)
                //    ControllerShaker.Instance.ShakeOnce((int)entity.PlayerInput.gamepadIndex, 0.1f, 0.1f, 0.01f, 0.2f);
                shootWeight += Time.deltaTime * weightSpeed;
                shootWeight = Mathf.Clamp01(shootWeight);
                anim.SetLayerWeight(1, shootWeight);

                projectileShooter.Shoot();
            }
            else
            {
                shootWeight -= Time.deltaTime * weightSpeed;
                shootWeight = Mathf.Clamp01(shootWeight);
                anim.SetLayerWeight(1, shootWeight);
            }
        }

        private void CalculateCameraDirection()
        {
            camForward = camera.transform.forward;
            camForward.y = 0f;
            camForward.Normalize();

            camRight = camera.transform.right;
            camRight.y = 0f;
            camRight.Normalize();
        }

        private void HandleAnimation()
        {
            // Setting paramters for animator controller blendtree,
            float horizontal = Vector3.Dot(moveDir, entity.transform.forward);
            float vertical = Vector3.Dot(moveDir, entity.transform.right);
            
            anim.SetFloat("Vertical", horizontal);
            anim.SetFloat("Horizontal", vertical);
        }

        private void HandleDash()
        {
            if(entity.PlayerInput.DashButton)
            {
                entity.CharMovement.AttemptDash();
            }
        }

        private void HandleInteract()
        {
            if(entity.PlayerInput.JumpButton)
            {
                Collider[] col;
                col = Physics.OverlapSphere(entity.transform.position, pickupReach, pickupLayerMask);
                if(col.Length != 0)
                {
                    int closestObject = 0;
                    float closestDistance = 100f;
                    for (int i = 0; i < col.Length; i++)
                    {
                        float distance = Vector3.Distance(entity.transform.position, col[i].gameObject.transform.position);
                        if (distance <= closestDistance)
                        {
                            closestDistance = distance;
                            closestObject = i;
                        }
                    }
                    col[closestObject].gameObject.SendMessage("Interact", entity.gameObject, SendMessageOptions.DontRequireReceiver);
                }

            }
        }

        #region standard animator stuff
        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}
        #endregion
    }
}
