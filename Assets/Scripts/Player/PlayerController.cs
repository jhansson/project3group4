﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private CharacterController charController;
    private Animator fsm;
    public PlayerInput PlayerInput { get; private set; }
    public CharacterMovement CharMovement { get; private set; }

    private void Awake()
    {
        charController = GetComponent<CharacterController>();
        CharMovement = GetComponent<CharacterMovement>();
        fsm = GetComponent<Animator>();
        PlayerInput = GetComponent<PlayerInput>();
    }
}
