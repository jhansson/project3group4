﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class StartPortal : MonoBehaviour
{

	private GameObject player;
	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		StartCoroutine(SpawnPortal());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator SpawnPortal()
	{
		yield return new WaitForSeconds(0.1f);
		
		GetComponent<ParticleSystem>().Stop();
		
		//EventManager.Instance.TriggerEvent(new GameEvents.StartPortalOpen());
	}
}
