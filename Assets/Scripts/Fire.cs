﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {

    private Collider col;
    private ParticleSystem ps;
    private float timer;
    private float fireStartTime;
    private float fireLifetime;
    [SerializeField] private float dealDamageCooldown = .5f;
    [SerializeField] private bool shouldDamagePlayer;

    private void Awake()
    {
        ps = GetComponentInChildren<ParticleSystem>();
        col = GetComponent<Collider>();
        timer = Time.time;
        fireLifetime = ps.main.duration;
        StartCoroutine("StartFire");
    }

    private void Update()
    {
        if(Time.time >= timer + ps.main.duration + 2f)
        {
            Destroy(gameObject);
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if(!other.gameObject.tag.Equals("Player"))
    //        other.gameObject.SendMessage("TakeDamage", 1, SendMessageOptions.DontRequireReceiver);
    //}

    IEnumerator StartFire()
    {
        while (Time.time <= timer + fireLifetime)
        {
            Collider[] overlap = Physics.OverlapBox(transform.position, col.bounds.extents);
            if (overlap.Length > 0)
            {
                for (int i = 0; i < overlap.Length; i++)
                {
                    if(shouldDamagePlayer)
                    {
                        overlap[i].gameObject.SendMessage("TakeDamage", 1, SendMessageOptions.DontRequireReceiver);
                    }
                    else
                    {
                        if(!overlap[i].gameObject.tag.Equals("Player"))
                            overlap[i].gameObject.SendMessage("TakeDamage", 1, SendMessageOptions.DontRequireReceiver);
                    }

                }
                yield return new WaitForSeconds(dealDamageCooldown);
            }
        }
        //should slowly scale down the fire
    }
}
