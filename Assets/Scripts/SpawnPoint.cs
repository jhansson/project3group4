﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnPointType
{
    ENEMY,
    CHEST
}

public class SpawnPoint : MonoBehaviour
{
    [SerializeField, Tooltip("What kind of spawnpoint this is.")]
    private SpawnPointType spawnPointType;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        if (spawnPointType == SpawnPointType.ENEMY)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position, 1f);
        }
        else
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(transform.position, new Vector3(1.5f, 0.5f, 1.5f));
        }


        
    }
}
