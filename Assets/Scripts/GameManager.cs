﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool player1active;
    private bool player2active;
    private bool player1alive;
    private bool player2alive;

    private static bool gameIsPaused;

    public delegate void OnGamePaused();
    public OnGamePaused onGamePaused;

    public delegate void OnGameResumed();
    public OnGameResumed onGameResumed;

    [SerializeField]
    private GameObject pauseMenuUI;

    private int currentLevel;

    public static GameManager instance;

    private void Awake()
    {

        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;

        //DontDestroyOnLoad(gameObject);
        if(LevelManager.levelManager != null)
            LevelManager.levelManager.onLevelLoaded += OnLevelLoaded;
    }

    private void OnLevelLoaded(int sceneIndex)
    {
        if(!FindObjectOfType<PlayerController>())
        {
            //Instantiate player prefab
        }
    }


    private void OnEnable()
    {
        EventManager.Instance.AddListener<GameEvents.PlayerActivated>(OnPlayerActivated);
        EventManager.Instance.AddListener<GameEvents.PlayerDied>(OnPlayerDied);
    }

    // Use this for initialization
    void Start () {
        /*
        player1active = true;
        player2active = true;
        player1alive = true;
        player2alive = true;
        */
        gameIsPaused = false;
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("MenuButton"))
        {
            Debug.Log("paused");
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }

        }
    }

    private void OnLevelWasLoaded(int level)
    {
        
    }

    private void OnPlayerActivated(GameEvents.PlayerActivated e)
    {
        Debug.Log("ny gamepad index" + e.PlayerID);
        if (e.PlayerID == 0)
        {
            player1active = true;
            player1alive = true;
        } else
        {
            player2active = true;
            player2alive = true;
        }
    }

    private void OnPlayerDied(GameEvents.PlayerDied e)
    {
        if (e.PlayerID == 1)
        {
            player1alive = false;
        }
        else
        {
            player2alive = false;
        }

        if (!player1alive && !player2alive)
        {
            StartCoroutine(LoadLevel(currentLevel));
            //Pause();
        }
    }

    private IEnumerator LoadLevel(int levelToLoad)
    {
        yield return new WaitForSeconds(2);
        // Just restart currentscene for now
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnLevelCompleted()
    {

    }

    private void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<GameEvents.PlayerActivated>(OnPlayerActivated);
            EventManager.Instance.RemoveListener<GameEvents.PlayerDied>(OnPlayerDied);
        }        
    }

    public void Resume()
    {
        if(onGameResumed != null)
            onGameResumed.Invoke();
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    private void Pause()
    {
        if(onGamePaused != null)
            onGamePaused.Invoke();
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

}
