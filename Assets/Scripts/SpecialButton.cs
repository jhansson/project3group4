﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SpecialButton : MonoBehaviour
{
    Button myButton;

    void Awake()
    {
        myButton = GetComponent<Button>();
        myButton.onClick.AddListener(ButtonWasClicked);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ButtonWasClicked()
    {
        // Trigger the ButtonPressed event
        // All other classes that has added a listener for this event will be notified
        EventManager.Instance.TriggerEvent(new GameEvents.ButtonPressed(Time.time));
    }
}
