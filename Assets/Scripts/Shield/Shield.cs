﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{

    public GameObject shield;
    public bool shieldEnabled;
    public int timesHit = 0;
    public int shieldHealth = 2;
    public float timer;
    public float rechargeDuration = 10;

    private void OnEnable()
    {
        //Resetting everything
        timesHit = 0;
        timer = Time.time;
        shield.SetActive(true);
        shieldEnabled = true;
    }

    public void DamageShield(float val)
    {
        timesHit++;
        if (timesHit >= shieldHealth)
        {
            shield.SetActive(false);
            shieldEnabled = false;
            timesHit = 0;
            timer = Time.time;
        }
    }

    private void Update()
    {
        if (shield.activeInHierarchy)
            return;

        if(Time.time >= timer + rechargeDuration)
        {
            shield.SetActive(true);
        }
    }
}
