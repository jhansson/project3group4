﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : Interactable {

    public Equipment item;

    public override void Interact(GameObject go)
    {
        base.Interact(go);

        PickUp(go);
    }

    private void PickUp(GameObject go)
    {
        item.Use(go);
        FindObjectOfType<AudioManager>().PlayRandomPitch("CollectItem");
        Destroy(gameObject);
    }
}
