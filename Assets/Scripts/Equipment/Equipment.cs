﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment")]
public class Equipment : Item {

    public EQ eq = new EQ();
    [HideInInspector] public EquipmentSlot equipSlot;
    [HideInInspector] public SkinnedMeshRenderer mesh;
    /// <summary>
    /// Corresponds to the item index inside the equipment manager, according to its equip slot
    /// </summary>
    [HideInInspector] public int itemIndex;
    [HideInInspector] public GameObject newWeaponPrefab;

    //Add a modifier for each stat in the CharacterStats script
    [HideInInspector] public int speedModifier;

    public void Start()
    {
        equipSlot = eq.equipSlot;
        //mesh = eq.mesh;
        itemIndex = eq.itemIndex;
        //newWeaponPrefab = eq.newWeaponPrefab;
        speedModifier = eq.speedModifier;
    }
    public override void Use(GameObject go)
    {
        //Call the equipment manager to equip this
        go.GetComponent<EquipmentManager>().Equip(eq);
    }

}

