﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlazingTrail : MonoBehaviour {

    public GameObject firePrefab;
    private Animator anim;
    private float currentFootstepLeft, previousFootstepLeft;
    private float currentFootstepRight, previousFootstepRight;

    private void Awake()
    {
        anim = GetComponentInParent<Animator>();
    }

    private void Update()
    {
        if(anim != null)
        {
            FootStepLeft();
            FootStepRight();
        }
    }

    private void FootStepLeft()
    {
        currentFootstepLeft = anim.GetFloat("FootstepLeft");
        if (currentFootstepLeft > 0 && previousFootstepLeft < 0)
        {
            Instantiate(firePrefab, transform.position, Quaternion.identity);
        }
        previousFootstepLeft = anim.GetFloat("FootstepLeft");
    }

    private void FootStepRight()
    {
        currentFootstepRight = anim.GetFloat("FootstepRight");
        if (currentFootstepRight > 0 && previousFootstepRight < 0)
        {
            Instantiate(firePrefab, transform.position, Quaternion.identity);
        }
        previousFootstepRight = anim.GetFloat("FootstepRight");
    }
}
