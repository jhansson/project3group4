﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class EquipmentManager : MonoBehaviour {

    [Tooltip("Just a debug feature to help visualize what the character is wearing")]
    public EQ[] currentEquipment;

    private SkinnedMeshRenderer[] currentMeshes;
    public SkinnedMeshRenderer[] headMeshes, chestMeshes, weaponMeshes, feetMeshes;

    public delegate void OnEquipmentChanged(EQ newItem, EQ oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    private int slotIndex;
    private int itemTypeIndex;
    private int oldItemTypeIndex;

    private string savedEquipment = "Equipment";
    private string numEquipment;
    public EQ placeHolderEQ;
    private string filename = "equipment.data";
    private string path;
    public bool[] slotEquipped;

    [Header("Pickup References")]

    public GameObject[] headPickups, chestPickups, weaponPickups, feetPickups;

    private bool setActive = false;

    private void Awake()
    {

        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        slotEquipped = new bool[numSlots];
        currentEquipment = new EQ[numSlots];
        currentMeshes = new SkinnedMeshRenderer[numSlots];



        //Load data

        //Martin Psuedo Code
        //currentEquipment.Length; // save as an int in PlayerPrefs
        //for(currentEquipment.Length)
        //{
        //    currentEquipment[i] // convert to JSON
        //    "whatever" + i; // save string to playerprefs
        //}
    }

    private void Start()
    {
        //slotEquipped[2] = true;
        //Equip(placeHolderEQ);

        if (LevelManager.levelManager != null)
        {
            LevelManager.levelManager.onLoadingLevel += SaveEquipment;
            LevelManager.levelManager.onLevelLoaded += LoadSavedEquipment;
        }
        //If we are in the tutorial scene, we shouldn't be load our equipment.
        //TODO if we want player to drop his equipment upon death, some more checks needs to be made to nuke the 
        int scene = SceneManager.GetActiveScene().buildIndex;
        if (scene != 2)
            LoadSavedEquipment(scene);
    }

    private void SaveEquipment(int sceneIndex)
    {
        print("start saving");
        numEquipment = currentEquipment.Length.ToString();
        PlayerPrefs.SetString("NumberOfEquipment", numEquipment);
        for (int i = 0; i < currentEquipment.Length; i++)
        {

            filename = "equipment" + i + ".data";
            path = Application.persistentDataPath + "/" + filename;
            Debug.Log("saving to: " + path);
            string str = JsonUtility.ToJson(currentEquipment[i]);
            Debug.Log("I've just saved: " + str);
            System.IO.File.WriteAllText(path, str);
            //PlayerPrefs.SetString(savedEquipment + i, str);
            //print("saving: " + str);
            //if (currentEquipment[i] == null)
            //    return;
            //PlayerPrefs.SetInt("ItemSlot" + i, (int)currentEquipment[i].equipSlot);
            //PlayerPrefs.SetInt("ItemIndex" + i, currentEquipment[i].itemIndex);
            //PlayerPrefs.SetInt("SpeedModifier" + i, currentEquipment[i].speedModifier);

        }
    }

    private void LoadSavedEquipment(int sceneIndex)
    {
        //numEquipment = PlayerPrefs.GetString("NumberOfEquipment");
        //int nq = int.Parse(numEquipment);
        //print("number of EQ is: " + nq);
        for (int i = 0; i < currentEquipment.Length; i++)
        {
            filename = "equipment" + i + ".data";
            path = Application.persistentDataPath + "/" + filename;
            Debug.Log("loading from : " + path);
            if(File.Exists(path))
            {
                string contents = System.IO.File.ReadAllText(path);
                print(contents);
                EQ tempEQ = JsonUtility.FromJson<EQ>(contents);
                Equip(tempEQ);
            }


            //string str = PlayerPrefs.GetString(savedEquipment + i);
            //print("should load: " + str);

            //EQ tempEQ = JsonUtility.FromJson<EQ>(str);
            //Equip(tempEQ);

            //EQ tempEQ = new EQ();
            //if (PlayerPrefs.HasKey("ItemSlot" + i))
            //    tempEQ.equipSlot = (EquipmentSlot)PlayerPrefs.GetInt("ItemSlot");
            //if (PlayerPrefs.HasKey("ItemIndex" + i))
            //    tempEQ.itemIndex = PlayerPrefs.GetInt("ItemIndex");
            //if (PlayerPrefs.HasKey("SpeedModifier" + i))
            //    tempEQ.speedModifier = PlayerPrefs.GetInt("SpeedModifier");

            //print("When Loaded equipslot: " + tempEQ.equipSlot);
            //print("When Loaded itemindex: " + tempEQ.itemIndex);
            //print("When Loaded speed: " + tempEQ.speedModifier);

        }
    }

    /// <summary>
    /// Equips the given item and unequips any existing item in its slot
    /// </summary>
    /// <param name="newItem"></param>
    public void Equip(EQ newItem)
    {
        if (newItem == null)
            return;
        print("When Equipped equipslot: " + newItem.equipSlot);
        print("When Equipped itemindex: " + newItem.itemIndex);
        print("When Equipped speed: " + newItem.speedModifier);
        slotIndex = (int)newItem.equipSlot;
        itemTypeIndex = newItem.itemIndex;
        EQ oldItem = currentEquipment[slotIndex];
        if(oldItem != null)
        {
            oldItemTypeIndex = currentEquipment[slotIndex].itemIndex;

            UnEquip(slotIndex);
        }


        if(onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newItem, oldItem);
        }

        currentEquipment[slotIndex] = newItem;

        setActive = true;
        HideUnhideMesh(setActive, itemTypeIndex);
        slotEquipped[slotIndex] = true;
        //currentMeshes[slotIndex] = newMesh;
    }

    /// <summary>
    /// Checks to see if we have any equipment in given slot and unequips it
    /// </summary>
    /// <param name="slotIndex"></param>
    public void UnEquip(int slotIndex)
    {
        if (currentEquipment[slotIndex] != null)
        {
            //if(currentMeshes[slotIndex] != null)
            //    Destroy(currentMeshes[slotIndex].gameObject);

            EQ oldItem = currentEquipment[slotIndex];
            if (onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldItem);
            }
            setActive = false;
            HideUnhideMesh(setActive, oldItemTypeIndex);

            if(slotEquipped[slotIndex])
            {
                DropEquipment((int)oldItem.equipSlot, oldItem.itemIndex);
            }

            slotEquipped[slotIndex] = true;
            currentEquipment[slotIndex] = null;
            //TODO Drop previous item onto the floor.
        }
    }

    public void HideUnhideMesh(bool active, int itemIdex)
    {
        switch(currentEquipment[slotIndex].equipSlot)
        {
            case EquipmentSlot.Head:
                if (headMeshes[itemIdex].gameObject != null)
                    headMeshes[itemIdex].gameObject.SetActive(active);
                break;
            case EquipmentSlot.Chest:
                if (chestMeshes[itemIdex].gameObject != null)
                    chestMeshes[itemIdex].gameObject.SetActive(active);
                break;
            //Removed because we don't have any weapon meshes to replace since we are going with the glove idea
            case EquipmentSlot.Weapon:
                if (weaponMeshes[itemIdex].gameObject != null)
                    weaponMeshes[itemIdex].gameObject.SetActive(active);
                    
                    Debug.Log("Item has Weaponslot");
                break;
            case EquipmentSlot.Feet:
                if (feetMeshes[itemIdex].gameObject != null)
                {
                    feetMeshes[itemIdex].gameObject.SetActive(active);
                }
                break;
        }
    }

    private void DropEquipment(int slotIndex, int itemIndex)
    {
        switch (currentEquipment[slotIndex].equipSlot)
        {
            case EquipmentSlot.Head:
                Instantiate(headPickups[itemIndex], transform.position, Quaternion.identity);
                break;
            case EquipmentSlot.Chest:
                Instantiate(chestPickups[itemIndex], transform.position, Quaternion.identity);
                break;
            case EquipmentSlot.Weapon:
                Instantiate(weaponPickups[itemIndex], transform.position, Quaternion.identity);
                break;
            case EquipmentSlot.Feet:
                Instantiate(feetPickups[itemIndex], transform.position, Quaternion.identity);
                break;
        }
    }
}
