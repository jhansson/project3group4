﻿using UnityEngine;

public class BlurUI : MonoBehaviour {

    private Camera MainCamera = null;
    private Camera TempCamera = null;

    // Use the SimpleBlur included with Amplify here.
    public Material UI_Material = null;

	// Use this for initialization
	void Start () {
        MainCamera = GetComponent<Camera>();
        TempCamera = new GameObject().AddComponent<Camera>();
        TempCamera.enabled = false;
    }
	
	// Update is called once per frame
	private void OnRenderImage(RenderTexture source, RenderTexture destination) {

        //set up a temporary camera
        TempCamera.CopyFrom(MainCamera);
        //TempCamera.clearFlags = CameraClearFlags.Color;
        //TempCamera.backgroundColor = Color.black;

        // make the temporary rendertexture, scale to half source
        RenderTexture TempRT = new RenderTexture(source.width, source.height, 0, RenderTextureFormat.R8);

        // put it to video memory
        TempRT.Create();

        // set the camera's target texture when rendering
        TempCamera.targetTexture = TempRT;

        // Render the image.
        TempCamera.Render();

        // Get the rendered scene as a texture
        UI_Material.SetTexture("_MainSample", source);

        // release the temporary Render Texture
        TempRT.Release();
    }
}
