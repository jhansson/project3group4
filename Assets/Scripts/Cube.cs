﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Cube : MonoBehaviour
{
    Rigidbody rb;
    
    // When component is enabled
    void OnEnable()
    {
        // Add listener for the ButtonPressed event
        EventManager.Instance.AddListener<GameEvents.ButtonPressed>(Jump);
    }

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Jump(GameEvents.ButtonPressed e)
    {
        rb.AddForce(new Vector3(Random.Range(-30f, 30f), Random.Range(100f, 500f), 0));
    }

    // When component is disabled
    void OnDisable()
    {
        // Remove listener for the ButtonPressed event
        EventManager.Instance.RemoveListener<GameEvents.ButtonPressed>(Jump);
    }
}
