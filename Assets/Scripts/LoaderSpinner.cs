﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoaderSpinner : MonoBehaviour
{
    [SerializeField, Tooltip("The background that is used as a fade in and out")]
    private Image backgroundReference;

    private GameObject spinner;

    private void Awake()
    {
        spinner = transform.GetChild(0).transform.gameObject;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (backgroundReference.color.a == 1 && !spinner.activeInHierarchy)
        {
            spinner.SetActive(true);
        }
        else if (backgroundReference.color.a != 1 && spinner.activeInHierarchy)
        {
            spinner.SetActive(false);
        }
	}
}
