﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBarrel : DestructibleObject
{
    [SerializeField] private float explosiveRadius = 3;
    [SerializeField] private int damage = 1;
    [SerializeField] private bool damagePlayer;
    private bool isExploding;

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
    }

    public override void OnDie()
    {
        if (isExploding)
            return;
        isExploding = true;
        Explode();
        DropItem();

        FindObjectOfType<AudioManager>().Play("Explosion");

        if (breakEffect != null)
            Instantiate(breakEffect, transform.position, Quaternion.identity);

    }

    /// <summary>
    /// Sphere overlaps and checks for objects to damage
    /// </summary>
    private void Explode()
    {
        //Sphere overlap, deal damage to each
        Collider[] col;
        col = Physics.OverlapSphere(transform.position, explosiveRadius);
        if (col.Length != 0)
        {
            for (int i = 0; i < col.Length; i++)
            {
                if(col[i].gameObject != transform.gameObject) // don't try to damage the gameobject itself
                {
                    if(damagePlayer)
                    {
                        Shield opponentShield = col[i].gameObject.GetComponentInChildren<Shield>();
                        if (opponentShield != null && opponentShield.shield.activeInHierarchy) //if player has shield, damage it instead of HP
                        {
                            opponentShield.DamageShield(1f);
                        }
                        //Damage any nearby destructible objects
                        else if(col[i].gameObject.GetComponent<DestructibleObject>() != null) //Deal more damage to other destructible objects
                            col[i].gameObject.SendMessage("TakeDamage", damage + 1, SendMessageOptions.DontRequireReceiver);
                        //Damage the player
                        else
                            col[i].gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
                    }
                    else if(!damagePlayer && col[i].gameObject.GetComponent<PlayerController>() == null)
                    {
                        //Damage any nearby destructible objects
                        if(col[i].gameObject.GetComponent<DestructibleObject>() != null) //Deal more damage to other destructible objects
                            col[i].gameObject.SendMessage("TakeDamage", damage + 1, SendMessageOptions.DontRequireReceiver);
                        else
                        col[i].gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, explosiveRadius);
    }
}
