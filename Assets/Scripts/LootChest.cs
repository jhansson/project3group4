﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootChest : Interactable {

    public bool isLocked;
    public bool isOpened;
    private Animator anim;
    public GameObject interactVFX;
    public GameObject openingVFX;
    public Vector3 vfxOffset = new Vector3(0, .5f, 0);

    [SerializeField] private ItemPickup[] loot;
    [SerializeField] private Vector3 lootSpawnOffset = new Vector3(0, 2f, 0);
    private Collider col;

    private GameObject player;
    private EquipmentManager playerEquipment;

    public override void Awake()
    {
        base.Awake();
        col = GetComponent<SphereCollider>();
        anim = GetComponent<Animator>();
    }

    public override void Interact(GameObject go)
    {
        base.Interact(go);

        if (isOpened)
            return;

        player = go;
        playerEquipment = player.GetComponent<EquipmentManager>();
        isOpened = true;
        if (interactVFX != null)
            Instantiate(interactVFX, transform.position + vfxOffset, transform.rotation);
        anim.SetTrigger("Open");
        FindObjectOfType<AudioManager>().PlayRandomPitch("OpenLootMagic");
        FindObjectOfType<AudioManager>().PlayRandomPitch("OpenLootChest");
        if (hudDisplay != null)
            hudDisplay.SetActive(false);
        //TODO change into default physics layer to not interfere with player trying to pickup closest interactable

    }

    public void SpawnItem()
    {
        if(openingVFX != null)
            Instantiate(openingVFX, transform.position + vfxOffset, transform.rotation);
        DropLoot();
    }

    private void DropLoot()
    {
        col.enabled = false;
        if (loot.Length == 0)
            return;
        bool shouldSpawn = true;
        int randItem = Random.Range(0, loot.Length);
        for (int i = 0; i < playerEquipment.currentEquipment.Length; i++)
        {
            if (loot[randItem].item.eq == playerEquipment.currentEquipment[i])
            {
                shouldSpawn = false;
                DropLoot();
                return;
            }
        }
        if(shouldSpawn == true)
            Instantiate(loot[randItem], transform.position + lootSpawnOffset, Quaternion.identity);
        
        
        //look through the four equipped bools
        //priorities the ones that aren't filled in
        //for every bool that isn't true
        //try to spawn items from that equipslot

    }

    public override void OnTriggerEnter(Collider other)
    {
        //TODO should display a box with item name and wait for button presses
        if (other.tag == "Player")
        {
            if (isOpened)
                return;

            if (hudDisplay != null)
                hudDisplay.SetActive(true);
        }
    }

    public override void OnTriggerExit(Collider other)
    {
        if (isOpened)
            return;

        if (other.tag == "Player")
        {
            if (hudDisplay != null)
                hudDisplay.SetActive(false);
        }
    }
}
