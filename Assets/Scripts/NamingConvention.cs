﻿using UnityEngine;
public class NamingConvention {

    /// <summary>
    /// private variable with camelCase, visible in editor and has a locked editable range
    /// </summary>
    [SerializeField, Tooltip("Ex: the stun duration"), Range(0f, 2f)]
    private float cooldownDuration;

    /// <summary>
    /// A gettable property, but set privately
    /// </summary>
    public float CooldownDuration { get; private set; }

    #region click + to expand for example of a bunch of components
    //reduces the visual bloat by containing stuff inside a region
    #endregion

    private void Start()
    {
        //don't use var, be specific about the variables you use!
        var value = 0.5f;
    }
}
