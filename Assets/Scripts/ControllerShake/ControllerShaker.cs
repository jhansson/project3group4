﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class ControllerShaker : MonoBehaviour {

    public static ControllerShaker Instance;
    static Dictionary<string, ControllerShaker> instanceList = new Dictionary<string, ControllerShaker>();

    private List<ControllerShake> controllerShakeInstances = new List<ControllerShake>();
    private Vector2 motors;

    private void Awake()
    {
        Instance = this;
        instanceList.Add(gameObject.name, this);
    }

    private void Update()
    {

        for (int i = 0; i < controllerShakeInstances.Count; i++)
        {
            if (i >= controllerShakeInstances.Count)
                break;

            ControllerShake c = controllerShakeInstances[i];

            if (c.CurrentState == ControllerShakeState.Inactive && c.DeleteOnInactive)
            {
                controllerShakeInstances.RemoveAt(i);
                i--;
            }
            else if (c.CurrentState != ControllerShakeState.Inactive)
            {
                motors = c.UpdateShake();
            }

            for (int n = 0; n < c.gamepadIndex.Count; n++)
            {
                PlayerIndex g = c.gamepadIndex[n];

                GamePad.SetVibration(g, motors.x, motors.y);
            }
        }

    }


    /// <summary>
    /// Starts a shake using the given preset
    /// </summary>
    /// <param name="shake"></param>
    /// <returns></returns>
    public ControllerShake Shake(ControllerShake shake)
    {
        controllerShakeInstances.Add(shake);
        return shake;
    }

    /// <summary>
    /// Plays a custom shake
    /// </summary>
    /// <param name="gamepadIndex">The gamepad to vibrate</param>
    /// <param name="leftMotor">Vibration magnitude of the left motor</param>
    /// <param name="rightMotor">Vibration magnitude of the right motor</param>
    /// <param name="fadeInTime">The length of time for the build up before reaching given the magnitude of the motors</param>
    /// <param name="fadeOutTime">The length of time where the vibration magnitude deteriorates to zero</param>
    /// <returns></returns>
    public ControllerShake ShakeOnce(int gamepadIndex, float leftMotor, float rightMotor, float fadeInTime, float fadeOutTime)
    {
        ControllerShake shake = new ControllerShake(gamepadIndex, leftMotor, rightMotor, fadeInTime, fadeOutTime);
        controllerShakeInstances.Add(shake);

        return shake;
    }
}
