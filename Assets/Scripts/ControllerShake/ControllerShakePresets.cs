﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Plays preset controller shakes to both controllers
/// </summary>
public static class ControllerShakePresets  {

    static int[] gamepads = new int[2];

    /// <summary>
    /// Bumps both player controllers
    /// </summary>
    public static ControllerShake Bump
    {
        get
        {
            ControllerShake c = new ControllerShake(gamepads, 0.2f, 0.2f, .2f, 0.2f);
            return c;
        }
    }

    /// <summary>
    /// Sudden and large vibration, best used with screenshake
    /// </summary>
    public static ControllerShake Explosion
    {
        get
        {
            ControllerShake c = new ControllerShake(gamepads, 0.6f, 0.6f, .2f, 1f);
            return c;
        }
    }
}
