﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public enum ControllerShakeState { FadingIn, FadingOut, Sustained, Inactive }

public class ControllerShake {

    public float leftMotor, rightMotor;
    public float fadeInTime, fadeOutTime;
    public List<PlayerIndex> gamepadIndex = new List<PlayerIndex>();
    public bool DeleteOnInactive = true;

    bool sustain;
    float currentFadeTime;
    float tick = 0;
    Vector2 amt;

    /// <summary>
    /// Will create a new instance that will continue to shake specific controller for the given duration
    /// </summary>
    /// <param name="gamepadIndex">The gamepad to vibrate</param>
    /// <param name="leftMotor">Vibration magnitude of the left motor</param>
    /// <param name="rightMotor">Vibration magnitude of the right motor</param>
    /// <param name="fadeInTime">The length of time for the build up before reaching given the magnitude of the motors</param>
    /// <param name="fadeOutTime">The length of time where the vibration magnitude deteriorates to zero</param>
    public ControllerShake(int gamepadIndex, float leftMotor, float rightMotor, float fadeInTime, float fadeOutTime)
    {
        this.leftMotor = leftMotor;
        this.rightMotor = rightMotor;
        this.fadeInTime = fadeInTime;
        this.fadeOutTime = fadeOutTime;
        this.gamepadIndex.Add((PlayerIndex)gamepadIndex);
        amt.x = leftMotor;
        amt.y = rightMotor;

        if(fadeInTime > 0)
        {
            sustain = true;
            currentFadeTime = 0;
        }
        else
        {
            sustain = false;
            currentFadeTime = 1;
        }
    }

    /// <summary>
    /// Will create a new instance that will continue to shake multiple controllers for the given duration
    /// </summary>
    /// <param name="gamepadIndex">The gamepad(s) to vibrate</param>
    /// <param name="leftMotor">Vibration magnitude of the left motor</param>
    /// <param name="rightMotor">Vibration magnitude of the right motor</param>
    /// <param name="fadeInTime">The length of time for the build up before reaching given the magnitude of the motors</param>
    /// <param name="fadeOutTime">The length of time where the vibration magnitude deteriorates to zero</param>
    public ControllerShake(int[] gamepadIndex, float leftMotor, float rightMotor, float fadeInTime, float fadeOutTime)
    {
        this.leftMotor = leftMotor;
        this.rightMotor = rightMotor;
        this.fadeInTime = fadeInTime;
        this.fadeOutTime = fadeOutTime;
        for (int i = 0; i < gamepadIndex.Length; i++)
        {
            this.gamepadIndex.Add((PlayerIndex)gamepadIndex[i]);
        }

        amt.x = leftMotor;
        amt.y = rightMotor;

        if (fadeInTime > 0)
        {
            sustain = true;
            currentFadeTime = 0;
        }
        else
        {
            sustain = false;
            currentFadeTime = 1;
        }
    }

    /// <summary>
    /// Updates the vibration magnitude based on fadein- & fadeout time
    /// </summary>
    /// <returns>The vibration for left and right motors</returns>
    public Vector2 UpdateShake()
    {
        if(fadeInTime > 0 && sustain)
        {
            if (currentFadeTime < 1)
                currentFadeTime += Time.deltaTime / fadeInTime;
            else if (fadeOutTime > 0)
                sustain = false;
        }

        if (!sustain)
            currentFadeTime -= Time.deltaTime / fadeOutTime;

        return amt * currentFadeTime;
    }

    public float NormalizedFadeTime
    { get { return currentFadeTime; } }

    /// <summary>
    /// True if vibration is fading in or out
    /// </summary>
    bool IsShaking
    { get { return currentFadeTime > 0 || sustain; } }

    /// <summary>
    /// True if no longer fading in, thus vibration has begun fading out
    /// </summary>
    bool IsFadingOut
    { get { return !sustain && currentFadeTime > 0; } }

    /// <summary>
    /// True if vibration has started and thus is fading in
    /// </summary>
    bool IsFadingIn
    { get { return currentFadeTime < 1 && sustain && fadeInTime > 0; } }

    public ControllerShakeState CurrentState
    {
        get
        {
            if (IsFadingIn)
                return ControllerShakeState.FadingIn;
            else if (IsFadingOut)
                return ControllerShakeState.FadingOut;
            else if (IsShaking)
                return ControllerShakeState.Sustained;
            else
                return ControllerShakeState.Inactive;
        }
    }
}
