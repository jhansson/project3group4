﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EquipmentUI : MonoBehaviour {

    public Image[] headImages, chestImages, weaponImages, feetImages;
    public GameObject player;

    EquipmentManager eqManager;

    private void Start()
    {
        eqManager = player.GetComponent<EquipmentManager>();
        eqManager.onEquipmentChanged += EquipmentChanged;
    }


    private void EquipmentChanged(EQ newItem, EQ oldItem)
    {
        if(oldItem != null)
            SetActiveImages(false, oldItem);
        if(newItem != null)
            SetActiveImages(true, newItem);


    }

    private void SetActiveImages(bool active, EQ item)
    {
        switch (item.equipSlot)
        {
            case EquipmentSlot.Head:
                if (headImages[item.itemIndex + 1].gameObject != null)
                    headImages[item.itemIndex + 1].gameObject.SetActive(active);
                break;
            case EquipmentSlot.Chest:
                if (chestImages[item.itemIndex + 1].gameObject != null)
                    chestImages[item.itemIndex + 1].gameObject.SetActive(active);
                break;
            //Removed because we don't have any weapon images to replace since we are going with the glove idea
            case EquipmentSlot.Weapon:
                if (weaponImages[item.itemIndex + 1] != null)
                    weaponImages[item.itemIndex + 1].gameObject.SetActive(active);
                    Debug.Log("Item has Weaponslot");
                break;
            case EquipmentSlot.Feet:
                if (feetImages[item.itemIndex + 1].gameObject != null)
                {
                    feetImages[item.itemIndex + 1].gameObject.SetActive(active);
                }
                break;
        }
    }

}
