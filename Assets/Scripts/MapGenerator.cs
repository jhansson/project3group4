﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

// DungeonGenerator is responsible for generating the random dungeon.
// This class is way too complicated and needs to be broken down.

#region AreaConnectionTypes
// Enumerates each area type that we'll use to build the map.
// WNES means there are entrances/exits from/to west, north, east, and south.
// ES means there are entrances/exits from/to east and south. 
public enum AreaConnectionType
{
    None,
    Empty,
    //WNES,
    WE,
    NS,
    WN,
    NE,
    ES,
    WS,
    WNE,
    WES,
    NES,
    WNS,
    Wx,
    Nx,
    Ex,
    Sx
}
#endregion

public class MapGenerator : MonoBehaviour
{
    #region CellLocation class
    private class CellLocation
    {
        public int X { get; private set; }
        public int Z { get; private set; }
        
        public CellLocation(int setX, int setZ)
        {
            X = setX;
            Z = setZ;
        }
        
        public override bool Equals(object obj)
        {
            if (!(obj is CellLocation)) return false;
            
            CellLocation testObj = (CellLocation)obj;
            return testObj.X == X && testObj.Z == Z;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    #endregion

    #region PortalPlacementType
    private enum PortalPlacementType
    {
        Random,
        AtEdges
    }
    #endregion

    #region Variables

    [Header("General Settings")]
    [SerializeField, Tooltip("How many meters wide is a tile. Tiles has to be square.")]
    private float tileWidth;

    [SerializeField, Tooltip("Amount of tiles in width that will be used for the level.")]
    private int levelWidthInTiles;

    [SerializeField, Tooltip("Amount of tiles in height that will be used for the level.")]
    private int levelHeightInTiles;

    [SerializeField, Tooltip("Minimum amount of tiles that has to be spawned in a map.")]
    private int minAllowedTiles;

    [SerializeField, Tooltip("Minimum amount of tiles that has to be spawned in a map.")]
    private int maxAllowedTiles;

    [Header("Prefab references")]
    [SerializeField, Tooltip("Reference to the chest prefab.")]
    private GameObject chestPrefab;

    [SerializeField, Tooltip("Enemy prefabs")]
    private GameObject[] enemyPrefabs;

    [SerializeField, Tooltip("Prefab of starting portal for the level.")]
    private Transform portalEnterPrefab;

    [SerializeField, Tooltip("Prefab of the portal")]
    private Transform portalExitPrefab;

    [SerializeField, Tooltip("Prefab to use as the mist in areas for this level.")]
    private GameObject mistPrefab;

    [SerializeField, Tooltip("Transform in Hierarchy that areas will have as parent.")]
    private Transform areaTileParentInScene;

    [SerializeField, Tooltip("Transform in Hierarchy that other stuff will have as parent.")]
    private Transform otherStuffParentInScene;

    [Header("Area Tile Prefabs")]
    [SerializeField, Tooltip("Areas without connections.")]
    private Transform[] areaEmpty;

    [SerializeField, Tooltip("Areas with 4 connections.")]
    private Transform[] areaCrossroad;

    [SerializeField, Tooltip("Areas with 3 connections. West, East and South.")]
    private Transform[] areaTSection;

    [SerializeField, Tooltip("Areas with 2 connections as a turn. North and East.")]
    private Transform[] areaTurn;

    [SerializeField, Tooltip("Areas with 2 connections at opposite sides. North and South.")]
    private Transform[] areaStraight;

    [SerializeField, Tooltip("Areas with 1 connection to a dead end. Connection at South.")]
    private Transform[] areaDeadEnd;


    private GameObject player1;
    private const PortalPlacementType PortalPlacement = PortalPlacementType.Random;

    // Map grid
    private AreaConnectionType[,] mapGrid;
    private AreaConnectionType[,] borderGrid;
    private int[,] minimapGrid;

    // Stores with and height of area tiles.
    private float prefabAreaWidth;
    private float prefabAreaHeight;

    // Used to keep rotation on next tile to be placed.
    private Quaternion tileRotation;

    // Keep references to locations.
    private CellLocation playerStartLocation;
    private CellLocation portalStartLocation;
    private CellLocation portalEndLocation;
    private Vector2 bossLocation;

    // TODO: Remove this. Only used for testing.
    private Material materialOverridePortalStart;
    private Material materialOverridePortalOut;

    [HideInInspector]
    public int DungeonFloorNumber = 1;
    #endregion

    private NavMeshSurface navMesh;

    private int nonEmptyTileCount;

    private Transform createPortalEnd;

    private void Start()
    {
        player1 = FindObjectOfType<PlayerController>().transform.parent.gameObject;
        mapGrid = new AreaConnectionType[levelWidthInTiles, levelHeightInTiles];
        minimapGrid = new int[levelWidthInTiles, levelHeightInTiles];
        borderGrid = new AreaConnectionType[levelWidthInTiles + 2, levelHeightInTiles + 2];
        CreateDungeonFloor();
        navMesh = GetComponent<NavMeshSurface>();
        navMesh.BuildNavMesh();        
    }

    private int GetNumberOfNonEmptyTiles()
    {
        nonEmptyTileCount = 0;

        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            for (int j = 0; j < levelHeightInTiles; ++j)
            {
                if (mapGrid[i, j] != AreaConnectionType.Empty)
                {
                    nonEmptyTileCount++;
                }
            }
        }

        return nonEmptyTileCount;
    }

    private void CreateDungeonFloor()
    {
        do
        {
            FillMapGrid();
        } while (mapGrid[(int)bossLocation.x, (int)bossLocation.y] == AreaConnectionType.Empty || GetPotentialPortalPositions().Count == 0 || GetNumberOfNonEmptyTiles() < minAllowedTiles || GetNumberOfNonEmptyTiles() > maxAllowedTiles);

        CreatePortalsRandom();
        CreateMiniMapGrid();
        CreateBorderAroundMap();
        InstantiateAreasFromGrid();
    }

    private void FillMapGrid()
    {
        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            for (int j = 0; j < levelHeightInTiles; ++j)
            {
                mapGrid[i, j] = AreaConnectionType.Empty;
            }
        }

        FillOutGrid();
        FixUpIslands();
        CapOffDeadEnds();        
    }

    private void CreateMiniMapGrid()
    {
        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            for (int j = 0; j < levelHeightInTiles; ++j)
            {
                if (mapGrid[i, j] == AreaConnectionType.Empty)
                {
                    minimapGrid[i, j] = 0;
                } else
                {
                    minimapGrid[i, j] = 1;
                }
            }
        }

        EventManager.Instance.TriggerEvent(new GameEvents.MapCreated(mapGrid));
    }

    private void CreateBorderAroundMap()
    {
        for (int i = 0; i < levelWidthInTiles + 2; ++i)
        {
            for (int j = 0; j < levelHeightInTiles + 2; ++j)
            {
                borderGrid[i, j] = AreaConnectionType.Empty;
            }
        }

        for (int i = 0; i < levelWidthInTiles - 0; ++i)
        {
            for (int j = 0; j < levelHeightInTiles - 0; ++j)
            {
                borderGrid[i + 1, j + 1] = mapGrid[i - 0, j - 0];
            }
        }

        mapGrid = borderGrid;
    }

    private void FillOutGrid()
    {
        // Pick a random tile on the map and make it a dead end to be used for the boss room
        bossLocation = new Vector2(Random.Range(0, levelWidthInTiles), Random.Range(0, levelHeightInTiles));

        // All possible tiles
        AreaConnectionType[] deadEndTiles = { AreaConnectionType.Ex, AreaConnectionType.Nx, AreaConnectionType.Sx, AreaConnectionType.Wx };

        // Possible tiles for corners
        AreaConnectionType[] topLeftTiles = { AreaConnectionType.Ex, AreaConnectionType.Nx };
        AreaConnectionType[] topRightTiles = { AreaConnectionType.Wx, AreaConnectionType.Nx };
        AreaConnectionType[] bottomRightTiles = { AreaConnectionType.Wx, AreaConnectionType.Sx };
        AreaConnectionType[] bottomLeftTiles = { AreaConnectionType.Ex, AreaConnectionType.Sx };

        // Possible tiles for edges (excluding corners)
        AreaConnectionType topTile = AreaConnectionType.Nx;
        AreaConnectionType bottomTile = AreaConnectionType.Sx;
        AreaConnectionType leftTile = AreaConnectionType.Wx;
        AreaConnectionType rightTile = AreaConnectionType.Ex;

        // Set boss tile type depending on position on grid
        AreaConnectionType bossTileType = deadEndTiles[Random.Range(0, deadEndTiles.Length)];
        if ((int)bossLocation.x == 0)
            bossTileType = rightTile;
        if ((int)bossLocation.x == levelWidthInTiles - 1)
            bossTileType = leftTile;
        if ((int)bossLocation.y == 0)
            bossTileType = topTile;
        if ((int)bossLocation.y == levelHeightInTiles - 1)
            bossTileType = bottomTile;

        if ((int)bossLocation.x == 0 && (int)bossLocation.y == 0)
            bossTileType = topLeftTiles[Random.Range(0, topLeftTiles.Length)];
        if ((int)bossLocation.x == 0 && (int)bossLocation.y == levelHeightInTiles - 1)
            bossTileType = bottomLeftTiles[Random.Range(0, topRightTiles.Length)];
        if ((int)bossLocation.x == levelWidthInTiles - 1 && (int)bossLocation.y == 0)
            bossTileType = topRightTiles[Random.Range(0, bottomRightTiles.Length)];
        if ((int)bossLocation.x == levelWidthInTiles - 1 && (int)bossLocation.y == levelHeightInTiles - 1)
            bossTileType = bottomRightTiles[Random.Range(0, bottomLeftTiles.Length)];

        mapGrid[(int)bossLocation.x, (int)bossLocation.y] = bossTileType;
        //mapGrid[1, 0] = bottomLeftTiles[Random.Range(0, topRightTiles.Length)];

        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            for (int j = 0; j < levelHeightInTiles; ++j)
            {
                if (mapGrid[i, j] != AreaConnectionType.Empty) continue;

                // Only add a tile 25% of the time to create more interesting maps-
                // It will still add tiles later to fit them together.
                float pickNew = Random.Range(0f, 1f);
                if (pickNew < 0.35f)
                {
                    mapGrid[i, j] = ChooseValidAreaType(new CellLocation(i, j));
                }
            }
        }
    }

    #region GetPrefabFromAreaConnectionType
    /// <summary>
    /// Returns a random Transform/prefab for the area type with fitting rotation.
    /// </summary>
    /// <param name="areaType"></param>
    /// <returns></returns>
    private Transform GetPrefabFromAreaConnectionType(AreaConnectionType areaType)
    {
        Transform areaPrefab;

        switch (areaType)
        {
            case AreaConnectionType.Empty:
                areaPrefab = areaEmpty[Random.Range(0, areaEmpty.Length)];
                tileRotation = Quaternion.Euler(0, Random.Range(0, 3) * 90f, 0);
                return areaPrefab;
                /*
            case AreaConnectionType.WNES:
                areaPrefab = areaCrossroad[Random.Range(0, areaCrossroad.Length)];
                tileRotation = Quaternion.Euler(0, Random.Range(0, 3) * 90f, 0);
                return areaPrefab;*/
            case AreaConnectionType.WE:
                areaPrefab = areaStraight[Random.Range(0, areaStraight.Length)];
                tileRotation = Quaternion.Euler(0, 90f, 0);
                return areaPrefab;
            case AreaConnectionType.NS:
                areaPrefab = areaStraight[Random.Range(0, areaStraight.Length)];
                tileRotation = Quaternion.identity;
                return areaPrefab;
            case AreaConnectionType.WN:
                areaPrefab = areaTurn[Random.Range(0, areaTurn.Length)];
                tileRotation = Quaternion.Euler(0, -90f, 0);
                return areaPrefab;
            case AreaConnectionType.NE:
                areaPrefab = areaTurn[Random.Range(0, areaTurn.Length)];
                tileRotation = Quaternion.identity;
                return areaPrefab;
            case AreaConnectionType.ES:
                areaPrefab = areaTurn[Random.Range(0, areaTurn.Length)];
                tileRotation = Quaternion.Euler(0, 90f, 0);
                return areaPrefab;
            case AreaConnectionType.WS:
                areaPrefab = areaTurn[Random.Range(0, areaTurn.Length)];
                tileRotation = Quaternion.Euler(0, 180f, 0);
                return areaPrefab;
            case AreaConnectionType.WNE:
                areaPrefab = areaTSection[Random.Range(0, areaTSection.Length)];
                tileRotation = Quaternion.Euler(0, 180f, 0);
                return areaPrefab;
            case AreaConnectionType.WES:
                areaPrefab = areaTSection[Random.Range(0, areaTSection.Length)];
                tileRotation = Quaternion.identity;
                return areaPrefab;
            case AreaConnectionType.NES:
                areaPrefab = areaTSection[Random.Range(0, areaTSection.Length)];
                tileRotation = Quaternion.Euler(0, -90f, 0);
                return areaPrefab;
            case AreaConnectionType.WNS:
                areaPrefab = areaTSection[Random.Range(0, areaTSection.Length)];
                tileRotation = Quaternion.Euler(0, 90f, 0);
                return areaPrefab;
            case AreaConnectionType.Wx:
                areaPrefab = areaDeadEnd[Random.Range(0, areaDeadEnd.Length)];
                tileRotation = Quaternion.Euler(0, 90f, 0);
                return areaPrefab;
            case AreaConnectionType.Nx:
                areaPrefab = areaDeadEnd[Random.Range(0, areaDeadEnd.Length)];
                tileRotation = Quaternion.Euler(0, 180f, 0);
                return areaPrefab;
            case AreaConnectionType.Ex:
                areaPrefab = areaDeadEnd[Random.Range(0, areaDeadEnd.Length)];
                tileRotation = Quaternion.Euler(0, -90f, 0);
                return areaPrefab;
            case AreaConnectionType.Sx:
                areaPrefab = areaDeadEnd[Random.Range(0, areaDeadEnd.Length)];
                tileRotation = Quaternion.identity;
                return areaPrefab;

            default:
                return areaEmpty[0];
        }
    }
    #endregion

    private void InstantiateAreasFromGrid()
    {
        prefabAreaWidth = tileWidth;
        prefabAreaHeight = tileWidth;

        for (int i = 0; i < levelWidthInTiles + 2; ++i)
        {
            for (int j = 0; j < levelHeightInTiles + 2; ++j)
            {
                if (mapGrid[i, j] == AreaConnectionType.None) continue;

                // Create tile in current position in grid.
                float instantiateXPosition = transform.position.x + (i * prefabAreaWidth);
                float instantiateZPosition = transform.position.z + (j * prefabAreaHeight);
                Transform prefabToMake = GetPrefabFromAreaConnectionType(mapGrid[i, j]);
                Transform createArea = Instantiate(prefabToMake, new Vector3(instantiateXPosition, 0.0f, instantiateZPosition), tileRotation);
                    
                // Tell AreaTile script which tile in the grid it is.
                if (createArea.GetComponent<AreaTile>() != null)
                {
                    createArea.GetComponent<AreaTile>().PositionInGrid = new Vector2(i - 1, j - 1);
                }
                
                // Spawn player in one of the enemy spawn points or in middle of tile.
                if ((i == portalStartLocation.X && j == portalStartLocation.Z) || mapGrid[i, j] == AreaConnectionType.Empty)
                {
                    if ((i == portalStartLocation.X && j == portalStartLocation.Z))
                    {
                        float playerPositionX = instantiateXPosition;
                        float playerPositionZ = instantiateZPosition;
                        
                        var spoints = createArea.GetComponent<AreaTile>().EnemySpawnPoints;

                        if (spoints.Length > 0)
                        {
                            var randomSpoint = spoints[Random.Range(0, spoints.Length)];
                            if (randomSpoint != null)
                            {
                                playerPositionX = randomSpoint.position.x;
                                playerPositionZ = randomSpoint.position.z;
                            }                                
                        }
                        
                        player1.transform.position = new Vector3(playerPositionX, 1.0f, playerPositionZ);
                        Instantiate(portalEnterPrefab, new Vector3(playerPositionX + 1, 4.5f, playerPositionZ - 2f), Quaternion.identity);
                    }
                }
                else 
                {
                    // Spawn mist
                    if (mistPrefab != null)
                    {
                        createArea.GetComponent<AreaTile>().AreaMist = Instantiate(mistPrefab, new Vector3(instantiateXPosition, 3.0f, instantiateZPosition), Quaternion.identity);
                        createArea.GetComponent<AreaTile>().AreaMist.transform.parent = otherStuffParentInScene;
                    }                        

                    // Spawn enemies
                    if (createArea.GetComponent<AreaTile>().AreaType == AreaType.EnemyArea)
                    {
                        int enemyCount = createArea.GetComponent<AreaTile>().EnemySpawnPoints.Length;
                        for (int e = 0; e < enemyCount; e++)
                        {
                            // Exit out if no spawn points exists.
                            if (createArea.GetComponent<AreaTile>().EnemySpawnPoints.Length <= 0) continue;
                            
                            // Exit out if the spawn point is not correctly assigned.
                            if (createArea.GetComponent<AreaTile>().EnemySpawnPoints[e] == null) continue;
                            
                            // Put the current spawn point to one of the spawn points in the array.
                            Vector3 spawnpos = createArea.GetComponent<AreaTile>().EnemySpawnPoints[e].position;

                            // Spawn enemy and deactivate it for now
                            GameObject spawnedEnemy = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)], spawnpos, Quaternion.identity);
                            spawnedEnemy.transform.parent = createArea;
                            spawnedEnemy.SetActive(false);

                            // Add enemy to the area tiles own script
                            if (createArea.GetComponent<AreaTile>() != null)
                            {
                                createArea.GetComponent<AreaTile>().Enemies.Add(spawnedEnemy);
                            }
                        }
                    }

                    // Spawn chests if the AreaTile script says so
                    if (createArea.GetComponent<AreaTile>().SpawnChest)
                    {
                        foreach (Transform itemSpawnPoint in createArea.GetComponent<AreaTile>().ItemSpawnPoints)
                        {
                            if (itemSpawnPoint != null && chestPrefab != null)
                            {
                                createArea.GetComponent<AreaTile>().SpawnedChests.Add(Instantiate(chestPrefab, itemSpawnPoint));
                            }
                        }
                    }
                    
                    // Adjust portal position if on this tile
                    if (i == portalEndLocation.X && j == portalEndLocation.Z)
                    {
                        if (createArea.GetComponent<AreaTile>().EnemySpawnPoints.Length > 0)
                        {
                            createPortalEnd.position = new Vector3(createArea.GetComponent<AreaTile>().EnemySpawnPoints[0].position.x, 2.5f, createArea.GetComponent<AreaTile>().EnemySpawnPoints[0].position.z);
                        }
                    }
                }

                createArea.parent = areaTileParentInScene;
            }
        }
    }

    private static ArrayList GetListOfAllAreaConnectionTypes()
    {
        ArrayList returnList = new ArrayList();
        foreach (AreaConnectionType value in Enum.GetValues(typeof(AreaConnectionType)))
        {
            returnList.Add(value);
        }
        return returnList;
    }

    private AreaConnectionType GetQualifyingAreaType(int placementGridX, int placementGridZ, bool hasWest, bool hasNorth, bool hasEast, bool hasSouth, bool noWest, bool noNorth, bool noEast, bool noSouth, bool useDeadEnds)
    {
        if (useDeadEnds)
        {
            return AreaConnectionType.Empty;
        }

        ArrayList currentQualifyingTypes = GetListOfAllAreaConnectionTypes();

        currentQualifyingTypes.Remove(AreaConnectionType.Empty);
        currentQualifyingTypes.Remove(AreaConnectionType.None);

        currentQualifyingTypes.Remove(AreaConnectionType.Wx);
        currentQualifyingTypes.Remove(AreaConnectionType.Nx);
        currentQualifyingTypes.Remove(AreaConnectionType.Ex);
        currentQualifyingTypes.Remove(AreaConnectionType.Sx);

        if (hasWest)
        {
            currentQualifyingTypes.Remove(AreaConnectionType.NS);
            currentQualifyingTypes.Remove(AreaConnectionType.NE);
            currentQualifyingTypes.Remove(AreaConnectionType.ES);
            currentQualifyingTypes.Remove(AreaConnectionType.NES);
        }

        if (hasNorth)
        {
            currentQualifyingTypes.Remove(AreaConnectionType.WE);
            currentQualifyingTypes.Remove(AreaConnectionType.WS);
            currentQualifyingTypes.Remove(AreaConnectionType.ES);
            currentQualifyingTypes.Remove(AreaConnectionType.WES);
        }

        if (hasEast)
        {
            currentQualifyingTypes.Remove(AreaConnectionType.NS);
            currentQualifyingTypes.Remove(AreaConnectionType.WN);
            currentQualifyingTypes.Remove(AreaConnectionType.WS);
            currentQualifyingTypes.Remove(AreaConnectionType.WNS);
        }

        if (hasSouth)
        {
            currentQualifyingTypes.Remove(AreaConnectionType.WE);
            currentQualifyingTypes.Remove(AreaConnectionType.NE);
            currentQualifyingTypes.Remove(AreaConnectionType.WN);
            currentQualifyingTypes.Remove(AreaConnectionType.WNE);
        }

        if (placementGridX == 0 || noWest)
        {
            //currentQualifyingTypes.Remove(AreaConnectionType.WNES);
            currentQualifyingTypes.Remove(AreaConnectionType.WE);
            currentQualifyingTypes.Remove(AreaConnectionType.WN);
            currentQualifyingTypes.Remove(AreaConnectionType.WS);
            currentQualifyingTypes.Remove(AreaConnectionType.WNE);
            currentQualifyingTypes.Remove(AreaConnectionType.WES);
            currentQualifyingTypes.Remove(AreaConnectionType.WNS);
        }

        if (placementGridX == levelWidthInTiles - 1 || noEast)
        {
            //currentQualifyingTypes.Remove(AreaConnectionType.WNES);
            currentQualifyingTypes.Remove(AreaConnectionType.WE);
            currentQualifyingTypes.Remove(AreaConnectionType.NE);
            currentQualifyingTypes.Remove(AreaConnectionType.ES);
            currentQualifyingTypes.Remove(AreaConnectionType.WNE);
            currentQualifyingTypes.Remove(AreaConnectionType.WES);
            currentQualifyingTypes.Remove(AreaConnectionType.NES);
        }

        if (placementGridZ == 0 || noSouth)
        {
            //currentQualifyingTypes.Remove(AreaConnectionType.WNES);
            currentQualifyingTypes.Remove(AreaConnectionType.NS);
            currentQualifyingTypes.Remove(AreaConnectionType.WS);
            currentQualifyingTypes.Remove(AreaConnectionType.ES);
            currentQualifyingTypes.Remove(AreaConnectionType.WES);
            currentQualifyingTypes.Remove(AreaConnectionType.NES);
            currentQualifyingTypes.Remove(AreaConnectionType.WNS);
        }

        if (placementGridZ == levelHeightInTiles - 1 || noNorth)
        {
            //currentQualifyingTypes.Remove(AreaConnectionType.WNES);
            currentQualifyingTypes.Remove(AreaConnectionType.NS);
            currentQualifyingTypes.Remove(AreaConnectionType.NE);
            currentQualifyingTypes.Remove(AreaConnectionType.WN);
            currentQualifyingTypes.Remove(AreaConnectionType.WNE);
            currentQualifyingTypes.Remove(AreaConnectionType.NES);
            currentQualifyingTypes.Remove(AreaConnectionType.WNS);
        }

        if (currentQualifyingTypes.Count == 0)
        {
            return AreaConnectionType.Empty;
        }
        else
        {
            return (AreaConnectionType)currentQualifyingTypes[Random.Range(0, currentQualifyingTypes.Count)];
        }
    }

    private bool CellLocationHasEast(CellLocation testLocation)
    {
        if (testLocation.X == levelWidthInTiles - 1)
        {
            return false;
        }

        AreaConnectionType testType = mapGrid[testLocation.X, testLocation.Z];
        switch (testType)
        {
            //case AreaConnectionType.WNES:
            case AreaConnectionType.WE:
            case AreaConnectionType.NE:
            case AreaConnectionType.ES:
            case AreaConnectionType.WNE:
            case AreaConnectionType.WES:
            case AreaConnectionType.NES:
            case AreaConnectionType.Ex:
                return true;
            case AreaConnectionType.None:
            case AreaConnectionType.Empty:
            case AreaConnectionType.NS:
            case AreaConnectionType.WN:
            case AreaConnectionType.WS:
            case AreaConnectionType.WNS:
            case AreaConnectionType.Wx:
            case AreaConnectionType.Nx:
            case AreaConnectionType.Sx:
                return false;
            default:
                return false;
        }
    }

    private bool CellLocationHasWest(CellLocation testLocation)
    {
        if (testLocation.X == 0)
        {
            return false;
        }

        AreaConnectionType testType = mapGrid[testLocation.X, testLocation.Z];
        switch (testType)
        {
            //case AreaConnectionType.WNES:
            case AreaConnectionType.WE:
            case AreaConnectionType.WN:
            case AreaConnectionType.WS:
            case AreaConnectionType.WNE:
            case AreaConnectionType.WES:
            case AreaConnectionType.WNS:
            case AreaConnectionType.Wx:
                return true;
            default:
                return false;
        }
    }

    private bool CellLocationHasNorth(CellLocation testLocation)
    {
        if (testLocation.Z == levelHeightInTiles - 1)
        {
            return false;
        }

        AreaConnectionType testType = mapGrid[testLocation.X, testLocation.Z];
        switch (testType)
        {
            //case AreaConnectionType.WNES:
            case AreaConnectionType.NS:
            case AreaConnectionType.NE:
            case AreaConnectionType.WN:
            case AreaConnectionType.WNE:
            case AreaConnectionType.NES:
            case AreaConnectionType.WNS:
            case AreaConnectionType.Nx:
                return true;
            default:
                return false;
        }
    }

    private bool CellLocationHasSouth(CellLocation testLocation)
    {
        if (testLocation.Z == 0)
        {
            return false;
        }

        AreaConnectionType testType = mapGrid[testLocation.X, testLocation.Z];
        switch (testType)
        {
            //case AreaConnectionType.WNES:
            case AreaConnectionType.NS:
            case AreaConnectionType.WS:
            case AreaConnectionType.ES:
            case AreaConnectionType.WES:
            case AreaConnectionType.NES:
            case AreaConnectionType.WNS:
            case AreaConnectionType.Sx:
                return true;
            default:
                return false;
        }
    }

    private AreaConnectionType ChooseValidAreaType(CellLocation location)
    {
        bool hasWest = false;
        bool hasNorth = false;
        bool hasEast = false;
        bool hasSouth = false;
        bool noWest = false;
        bool noNorth = false;
        bool noEast = false;
        bool noSouth = false;

        if (location.X > 0 && mapGrid[location.X - 1, location.Z] != AreaConnectionType.Empty)
        {
            if (CellLocationHasEast(new CellLocation(location.X - 1, location.Z)))
            {
                hasWest = true;
            }
            else
            {
                noWest = true;
            }
        }

        if (location.X < levelWidthInTiles - 1 && mapGrid[location.X + 1, location.Z] != AreaConnectionType.Empty)
        {
            if (CellLocationHasWest(new CellLocation(location.X + 1, location.Z)))
            {
                hasEast = true;
            }
            else
            {
                noEast = true;
            }
        }

        if (location.Z > 0 && mapGrid[location.X, location.Z - 1] != AreaConnectionType.Empty)
        {
            if (CellLocationHasNorth(new CellLocation(location.X, location.Z - 1)))
            {
                hasSouth = true;
            }
            else
            {
                noSouth = true;
            }
        }

        if (location.Z < levelHeightInTiles - 1 && mapGrid[location.X, location.Z + 1] != AreaConnectionType.Empty)
        {
            if (CellLocationHasSouth(new CellLocation(location.X, location.Z + 1)))
            {
                hasNorth = true;
            }
            else
            {
                noNorth = true;
            }
        }

        return GetQualifyingAreaType(location.X, location.Z, hasWest, hasNorth, hasEast, hasSouth, noWest, noNorth, noEast, noSouth, false);
    }


    private bool CellConnectedToCell(int startLocX, int startLocZ, int goalLocX, int goalLocZ, ref ArrayList knownExistingConnections)
    {
        ArrayList alreadySearchedList = new ArrayList();
        ArrayList toSearchList = new ArrayList();

        bool foundPath = false;
        bool doneWithSearch = false;
        toSearchList.Add(new CellLocation(startLocX, startLocZ));

        while (!doneWithSearch)
        {
            if (toSearchList.Count == 0)
            {
                doneWithSearch = true;
                break;
            }

            CellLocation toSearch = (CellLocation)toSearchList[0];
            toSearchList.RemoveAt(0);
            if (alreadySearchedList.Contains(toSearch) == false)
            {
                alreadySearchedList.Add(toSearch);
            }

            if ((toSearch.X == goalLocX && toSearch.Z == goalLocZ) || knownExistingConnections.Contains(toSearch))
            {
                doneWithSearch = true;
                foundPath = true;

                foreach (CellLocation pos in alreadySearchedList)
                {
                    knownExistingConnections.Add(pos);
                }

                foreach (CellLocation pos in toSearchList)
                {
                    knownExistingConnections.Add(pos);
                }

                break;
            }
            else
            {
                if (CellLocationHasEast(new CellLocation(toSearch.X, toSearch.Z)))
                {
                    CellLocation newLocation = new CellLocation(toSearch.X + 1, toSearch.Z);
                    if (toSearchList.Contains(newLocation) == false && alreadySearchedList.Contains(newLocation) == false)
                    {
                        toSearchList.Add(newLocation);
                    }
                }

                if (CellLocationHasWest(new CellLocation(toSearch.X, toSearch.Z)))
                {
                    CellLocation newLocation = new CellLocation(toSearch.X - 1, toSearch.Z);
                    if (toSearchList.Contains(newLocation) == false && alreadySearchedList.Contains(newLocation) == false)
                    {
                        toSearchList.Add(newLocation);
                    }
                }

                if (CellLocationHasNorth(new CellLocation(toSearch.X, toSearch.Z)))
                {
                    CellLocation newLocation = new CellLocation(toSearch.X, toSearch.Z + 1);
                    if (toSearchList.Contains(newLocation) == false && alreadySearchedList.Contains(newLocation) == false)
                    {
                        toSearchList.Add(newLocation);
                    }
                }

                if (CellLocationHasSouth(new CellLocation(toSearch.X, toSearch.Z)))
                {
                    CellLocation newLocation = new CellLocation(toSearch.X, toSearch.Z - 1);
                    if (toSearchList.Contains(newLocation) == false && alreadySearchedList.Contains(newLocation) == false)
                    {
                        toSearchList.Add(newLocation);
                    }
                }
            }
        }

        return foundPath;
    }

    private void FixUpIslands()
    {
        ArrayList knownExistingConnections = new ArrayList();

        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            for (int j = 0; j < levelHeightInTiles; ++j)
            {
                if (mapGrid[i, j] != AreaConnectionType.Empty && !CellConnectedToCell(i, j, levelWidthInTiles / 2, levelHeightInTiles / 2, ref knownExistingConnections))
                {
                    mapGrid[i, j] = AreaConnectionType.Empty;
                }
            }
        }
    }

    private AreaConnectionType GetDeadEndCapType(int locX, int locZ)
    {
        bool hasSouth = (locZ < levelHeightInTiles - 1 && CellLocationHasSouth(new CellLocation(locX, locZ + 1)));
        bool hasWest = (locX < levelWidthInTiles - 1 && CellLocationHasWest(new CellLocation(locX + 1, locZ)));
        bool hasNorth = (locZ > 0 && CellLocationHasNorth(new CellLocation(locX, locZ - 1)));
        bool hasEast = (locX > 0 && CellLocationHasEast(new CellLocation(locX - 1, locZ)));

        // If the "dead end" is acutally a T-section
        if (hasWest && hasEast && hasSouth)
            return AreaConnectionType.WNE;
        if (hasWest && hasNorth && hasSouth)
            return AreaConnectionType.NES;
        if (hasNorth && hasEast && hasSouth)
            return AreaConnectionType.WNS;
        if (hasWest && hasNorth && hasEast)
            return AreaConnectionType.WES;
        
        // If the "dead end" is actually a turn or straight
        if (hasWest && hasEast)
            return AreaConnectionType.WE;
        if (hasSouth && hasEast)
            return AreaConnectionType.WN;
        if (hasWest && hasNorth)
            return AreaConnectionType.ES;
        if (hasNorth && hasEast)
            return AreaConnectionType.WS;
        if (hasWest && hasSouth)
            return AreaConnectionType.NE;
        if (hasNorth && hasSouth)
            return AreaConnectionType.NS;
        
        // Dead end tiles
        if (hasWest)
            return AreaConnectionType.Ex;
        if (hasNorth)
            return AreaConnectionType.Sx;
        if (hasEast)
            return AreaConnectionType.Wx;
        if (hasSouth)
            return AreaConnectionType.Nx;
        
        // If no match at all
        return AreaConnectionType.Empty;
        
        
        /*
        if (locX > 0 && CellLocationHasEast(new CellLocation(locX - 1, locZ)))
        {
            if (locZ > 0 && CellLocationHasNorth(new CellLocation(locX, locZ - 1)))
                return AreaConnectionType.WS;
            
            if (locZ < levelHeightInTiles - 1 && CellLocationHasSouth(new CellLocation(locX, locZ + 1)))
                return AreaConnectionType.WN;

            if (locX + 1 < levelWidthInTiles - 1 && CellLocationHasWest(new CellLocation(locX + 2, locZ)))
                return AreaConnectionType.WE;
            
            return AreaConnectionType.Wx;
        }
        else if (locX < levelWidthInTiles - 1 && CellLocationHasWest(new CellLocation(locX + 1, locZ)))
        {
            if (locZ > 0 && CellLocationHasNorth(new CellLocation(locX, locZ - 1)))
                return AreaConnectionType.ES;
            
            if (locZ < levelHeightInTiles - 1 && CellLocationHasSouth(new CellLocation(locX, locZ + 1)))
                return AreaConnectionType.NE;
            
            if (locX - 1 > 0 && CellLocationHasEast(new CellLocation(locX - 2, locZ)))
                return AreaConnectionType.WE;
            
            return AreaConnectionType.Ex;
        }
        else if (locZ > 0 && CellLocationHasNorth(new CellLocation(locX, locZ - 1)))
        {
            if (locX > 0 && CellLocationHasEast(new CellLocation(locX - 1, locZ)))
                return AreaConnectionType.WS;
            
            if (locX < levelWidthInTiles - 1 && CellLocationHasWest(new CellLocation(locX + 1, locZ)))
                return AreaConnectionType.ES;
            
            if (locZ + 1 < levelHeightInTiles - 1 && CellLocationHasSouth(new CellLocation(locX, locZ + 2)))
                return AreaConnectionType.NS;
            
            return AreaConnectionType.Sx;
        }
        else if (locZ < levelHeightInTiles - 1 && CellLocationHasSouth(new CellLocation(locX, locZ + 1)))
        {
            if (locX > 0 && CellLocationHasEast(new CellLocation(locX - 1, locZ)))
                return AreaConnectionType.WN;
            
            if (locX < levelWidthInTiles - 1 && CellLocationHasWest(new CellLocation(locX + 1, locZ)))
                return AreaConnectionType.NE;

            if (locZ - 1 > 0 && CellLocationHasNorth(new CellLocation(locX, locZ - 2)))
                return AreaConnectionType.NS;
            
            return AreaConnectionType.Nx;
        }

        return AreaConnectionType.Empty;
        */
    }

    private void CapOffDeadEnds()
    {
        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            for (int j = 0; j < levelHeightInTiles; ++j)
            {
                if (mapGrid[i, j] == AreaConnectionType.Empty)
                {
                    mapGrid[i, j] = GetDeadEndCapType(i, j);
                }
            }
        }
    }

    private ArrayList GetPotentialPortalPositions()
    {
        ArrayList returnList = new ArrayList();

        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            for (int j = 0; j < levelHeightInTiles; ++j)
            {
                if (mapGrid[i, j] == AreaConnectionType.Empty) continue;
                
                if (i.Equals((int)bossLocation.x) && j.Equals((int)bossLocation.y))
                {
                    // Debug.Log("SAME AS BOSS LOCATION!");
                } else
                {
                    returnList.Add(new CellLocation(i, j));
                }
            }
        }

        return returnList;
    }

    private ArrayList GetPotentialPortalPositionsInRow(int row)
    {
        ArrayList returnList = new ArrayList();

        for (int i = 0; i < levelWidthInTiles; ++i)
        {
            if (i.Equals((int)bossLocation.x) || row.Equals((int)bossLocation.y)) continue;
            
            if (mapGrid[i, row] != AreaConnectionType.Empty)
            {
                returnList.Add(new CellLocation(i, row));
            }
        }

        return returnList;
    }

    private ArrayList GetPotentialPortalPositionsInColumn(int column)
    {
        ArrayList returnList = new ArrayList();

        for (int i = 0; i < levelHeightInTiles; ++i)
        {
            if (column.Equals((int)bossLocation.x) || i.Equals((int)bossLocation.y)) continue;
            
            if (mapGrid[column, i] != AreaConnectionType.Empty)
            {
                returnList.Add(new CellLocation(column, i));
            }
        }

        return returnList;
    }

    private void InstantiatePortals(CellLocation chosenStart, CellLocation chosenEnd)
    {
        prefabAreaWidth = tileWidth;
        prefabAreaHeight = tileWidth;

        int portalStartGridX = chosenStart.X + 1;
        int portalStartGridZ = chosenStart.Z + 1;
        float portalStartLocX = transform.position.x + (portalStartGridX * prefabAreaWidth);
        float portalStartLocZ = transform.position.z + (portalStartGridZ * prefabAreaHeight);
        // Transform createPortalStart = (Transform)Instantiate(prefabPortalStart, new Vector3(portalStartLocX, 0.0f, portalStartLocZ), Quaternion.identity);

        int portalEndGridX = chosenEnd.X + 1;
        int portalEndGridZ = chosenEnd.Z + 1;
        float portalEndLocX = transform.position.x + (portalEndGridX * prefabAreaWidth);
        float portalEndLocZ = transform.position.z + (portalEndGridZ * prefabAreaHeight);
        createPortalEnd = Instantiate(portalExitPrefab, new Vector3(portalEndLocX, 2.5f, portalEndLocZ), Quaternion.identity);

        string nextScene = (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings) ? SceneManager.GetSceneByBuildIndex(SceneManager.GetActiveScene().buildIndex + 1).name : "Level1";

        createPortalEnd.GetComponent<InteractablePortal>().sceneName = nextScene;

        portalStartLocation = new CellLocation(portalStartGridX, portalStartGridZ);
        portalEndLocation = new CellLocation(portalEndGridX, portalEndGridZ);

        // createPortalStart.parent = otherStuffParent;
        createPortalEnd.parent = otherStuffParentInScene;
    }

    private void CreatePortalsRandom()
    {
        ArrayList potentialPortalPositions = GetPotentialPortalPositions();

        int chosenStartIndex = Random.Range(0, potentialPortalPositions.Count);
        CellLocation chosenStart = (CellLocation)potentialPortalPositions[chosenStartIndex];
        potentialPortalPositions.RemoveAt(chosenStartIndex);
        //int chosenEndIndex = Random.Range(0, potentialPortalPositions.Count);
        CellLocation chosenEnd = new CellLocation((int)bossLocation.x, (int)bossLocation.y); // (CellLocation)potentialPortalPositions[chosenEndIndex];

        InstantiatePortals(chosenStart, chosenEnd);
    }

    private void CreatePortalsAtEdges()
    {
        bool placingHorizontally = (Random.Range(0, 2) == 0);

        ArrayList potentialPositions0 = new ArrayList();
        ArrayList potentialPositions1 = new ArrayList();

        if (placingHorizontally)
        {
            for (int i = 1; i < levelWidthInTiles; ++i)
            {
                potentialPositions0 = GetPotentialPortalPositionsInColumn(i);
                if (potentialPositions0.Count > 0)
                {
                    break;
                }
            }

            for (int i = levelWidthInTiles - 1; i >= 1; --i)
            {
                potentialPositions1 = GetPotentialPortalPositionsInColumn(i);
                if (potentialPositions1.Count > 0)
                {
                    break;
                }
            }
        }
        else
        {
            for (int i = 1; i < levelHeightInTiles; ++i)
            {
                potentialPositions0 = GetPotentialPortalPositionsInRow(i);
                if (potentialPositions0.Count > 0)
                {
                    break;
                }
            }

            for (int i = levelHeightInTiles - 1; i >= 1; --i)
            {
                potentialPositions1 = GetPotentialPortalPositionsInRow(i);
                if (potentialPositions1.Count > 0)
                {
                    break;
                }
            }
        }

        if (potentialPositions0.Count <= 0 || potentialPositions1.Count <= 0)
        {
            Debug.LogWarning("DungeonGenerator::CreatePortalsAtEdges() couldn't find valid potential placement stairs up/down positions!");
        }

        bool portalsStartFromPositions0 = (Random.Range(0, 2) == 0);
        if (portalsStartFromPositions0)
        {
            int chosenStartIndex = Random.Range(0, potentialPositions0.Count);
            CellLocation chosenStart = (CellLocation)potentialPositions0[chosenStartIndex];
            potentialPositions1.Remove(new CellLocation(chosenStart.X, chosenStart.Z));
            int chosenEndIndex = Random.Range(0, potentialPositions1.Count);
            CellLocation chosenEnd = (CellLocation)potentialPositions1[chosenEndIndex];
            InstantiatePortals(chosenStart, chosenEnd);
        }
        else
        {
            int chosenStartIndex = Random.Range(0, potentialPositions1.Count);
            CellLocation chosenStart = (CellLocation)potentialPositions1[chosenStartIndex];
            potentialPositions0.Remove(new CellLocation(chosenStart.X, chosenStart.Z));
            int chosenEndIndex = Random.Range(0, potentialPositions0.Count);
            CellLocation chosenEnd = (CellLocation)potentialPositions0[chosenEndIndex];
            InstantiatePortals(chosenStart, chosenEnd);
        }
    }


    private void DeleteDungeonFloor()
    {
        for (int i = 0; i < areaTileParentInScene.childCount; ++i)
        {
            Destroy(areaTileParentInScene.GetChild(i).gameObject);
        }

        for (int i = 0; i < otherStuffParentInScene.childCount; ++i)
        {
            Destroy(otherStuffParentInScene.GetChild(i).gameObject);
        }
    }

    public void GoToNextLevel()
    {
        DeleteDungeonFloor();
        CreateDungeonFloor();
        DungeonFloorNumber++;
    }
}
