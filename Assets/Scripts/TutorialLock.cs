﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialLock : MonoBehaviour {

    [SerializeField] private GameObject col;
    [SerializeField] private GameObject enemy;
    [SerializeField] private GameObject[] enemySpawnPoints;
    [SerializeField] private GameObject spawnVFX;

    private void Awake()
    {
        col.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            col.gameObject.SetActive(true);
            for (int i = 0; i < enemySpawnPoints.Length; i++)
            {
                Instantiate(enemy, enemySpawnPoints[i].transform.position, Quaternion.identity);
                if(spawnVFX != null)
                    Instantiate(spawnVFX, enemySpawnPoints[i].transform.position, Quaternion.identity);
            }
        }

    }
}
