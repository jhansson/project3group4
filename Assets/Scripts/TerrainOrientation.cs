﻿using UnityEngine;

public class TerrainOrientation : MonoBehaviour {

	// Set the rotation for the material on terrains on start.
	void Start () {
        Renderer renderer = GetComponent<Renderer>();        

        // Get the combined rotation around Y to set the material rotation
        float rotation = transform.localEulerAngles[1] + transform.parent.transform.localEulerAngles[1];

        renderer.material.SetFloat("_UVRotation", rotation);
	}
}
