﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{
    [SerializeField, Tooltip("The graphics that will represent an area.")]
    private GameObject areaIcon;

    [SerializeField, Tooltip("The width of space between all areas.")]
    private float borderSize;

    private GameObject[,] spawnedImages;

    private AreaConnectionType[,] miniMap;

    private void OnEnable()
    {
        EventManager.Instance.AddListener<GameEvents.MapCreated>(OnMapCreated);
        EventManager.Instance.AddListener<GameEvents.PlayerEnteredArea>(OnPlayerEnteredArea);
    }

    // Use this for initialization
    void Start () 
    {
        if (SceneManager.GetActiveScene().buildIndex > 2)
        {
            transform.parent.transform.parent.transform.parent.gameObject.SetActive(true);
        }
        else
        {
            transform.parent.transform.parent.transform.parent.gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

    private void OnMapCreated(GameEvents.MapCreated e)
    {
        miniMap = e.MapGrid;
        spawnedImages = new GameObject[miniMap.GetLength(0), miniMap.GetLength(1)];
        
        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(miniMap.GetLength(0) * areaIcon.GetComponent<RectTransform>().sizeDelta.x, miniMap.GetLength(1) * areaIcon.GetComponent<RectTransform>().sizeDelta.x);

        for (int i = 0; i < miniMap.GetLength(0); ++i)
        {
            for (int j = 0; j < miniMap.GetLength(1); ++j)
            {
                if (miniMap[i, j] != AreaConnectionType.Empty)
                {
                    GameObject spawnedImage = Instantiate(areaIcon, new Vector3(((areaIcon.GetComponent<RectTransform>().sizeDelta.x + borderSize) * i) + areaIcon.GetComponent<RectTransform>().sizeDelta.x, ((areaIcon.GetComponent<RectTransform>().sizeDelta.x + borderSize) * j) - ((areaIcon.GetComponent<RectTransform>().sizeDelta.x + borderSize) * miniMap.GetLength(1)), 0), Quaternion.identity);
                    spawnedImage.transform.SetParent(transform, false);
                    spawnedImage.GetComponent<miniMapTile>().positionInGrid = new Vector2(i, j);
                    spawnedImage.GetComponent<miniMapTile>().AreaConnectionType = miniMap[i, j];
                    spawnedImage.GetComponent<miniMapTile>().SetImage();
                    spawnedImage.GetComponent<Image>().enabled = false;                    
                    spawnedImages[i, j] = spawnedImage;

                }
            }
        }
    }

    private void OnPlayerEnteredArea(GameEvents.PlayerEnteredArea e)
    {
        var midX = (((miniMap.GetLength(0)) / 2f) - e.AreaPositionInGrid.x) * areaIcon.GetComponent<RectTransform>().sizeDelta.x; //) - (areaIcon.GetComponent<RectTransform>().sizeDelta.x * 1.5f);
        var midY = (((miniMap.GetLength(1)) / 2f) - e.AreaPositionInGrid.y) * areaIcon.GetComponent<RectTransform>().sizeDelta.x; //) + (areaIcon.GetComponent<RectTransform>().sizeDelta.x / 2f);
        
        GetComponent<RectTransform>().anchoredPosition = new Vector2(midX, midY);
        //GetComponent<RectTransform>().localPosition = new Vector3(((miniMap.GetLength(0) / 2f) - e.AreaPositionInGrid.x) * areaIcon.GetComponent<RectTransform>().sizeDelta.x, ((miniMap.GetLength(1) / 2f) - e.AreaPositionInGrid.y) * areaIcon.GetComponent<RectTransform>().sizeDelta.x, 0);
        
        for (int i = 0; i < miniMap.GetLength(0); ++i)
        {
            for (int j = 0; j < miniMap.GetLength(1); ++j)
            {
                if (new Vector2(i, j).Equals(e.AreaPositionInGrid))
                {
                    spawnedImages[i, j].GetComponent<Image>().enabled = true;
                    spawnedImages[i, j].GetComponent<miniMapTile>().playerIsOnTile = true;
                    spawnedImages[i, j].GetComponent<miniMapTile>().hasBeenFound = true;
                    spawnedImages[i, j].GetComponent<miniMapTile>().SetImage();

                    switch (miniMap[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y])
                    {
                        case AreaConnectionType.Wx:
                            if ((int)e.AreaPositionInGrid.x > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x - 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;                                
                            }
                            break;
                        case AreaConnectionType.Ex:
                            if ((int)e.AreaPositionInGrid.x < miniMap.GetLength(0) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x + 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.Sx:
                            if ((int)e.AreaPositionInGrid.y > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y - 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.Nx:
                            if ((int)e.AreaPositionInGrid.y < miniMap.GetLength(1) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y + 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.WE:
                            if ((int)e.AreaPositionInGrid.x < miniMap.GetLength(0) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x + 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.x > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x - 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.NS:                            
                            if ((int)e.AreaPositionInGrid.y < miniMap.GetLength(1) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y + 1].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y - 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.WN:
                            if ((int)e.AreaPositionInGrid.y < miniMap.GetLength(1) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y + 1].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.x > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x - 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.NE:
                            if ((int)e.AreaPositionInGrid.y < miniMap.GetLength(1) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y + 1].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.x < miniMap.GetLength(0) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x + 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.ES:
                            if ((int)e.AreaPositionInGrid.x < miniMap.GetLength(0) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x + 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y - 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.WS:
                            if ((int)e.AreaPositionInGrid.x > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x - 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y - 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.WNE:
                            if ((int)e.AreaPositionInGrid.x > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x - 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y < miniMap.GetLength(1) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y + 1].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.x < miniMap.GetLength(0) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x + 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.WES:
                            if ((int)e.AreaPositionInGrid.x > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x - 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.x < miniMap.GetLength(0) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x + 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y - 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.NES:
                            if ((int)e.AreaPositionInGrid.y < miniMap.GetLength(1) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y + 1].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.x < miniMap.GetLength(0) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x + 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y - 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        case AreaConnectionType.WNS:
                            if ((int)e.AreaPositionInGrid.x > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x - 1, (int)e.AreaPositionInGrid.y].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y < miniMap.GetLength(1) - 1)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y + 1].GetComponent<Image>().enabled = true;
                            }
                            if ((int)e.AreaPositionInGrid.y > 0)
                            {
                                spawnedImages[(int)e.AreaPositionInGrid.x, (int)e.AreaPositionInGrid.y - 1].GetComponent<Image>().enabled = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    if (spawnedImages[i, j] != null)
                    {
                        spawnedImages[i, j].GetComponent<miniMapTile>().playerIsOnTile = false;
                        spawnedImages[i, j].GetComponent<miniMapTile>().SetImage();                  
                    }                    
                }
                
                
                
            }
        }
    }

    private void OnDisable()
    {
        if (EventManager.Instance != null)
        {
            //
            EventManager.Instance.RemoveListener<GameEvents.MapCreated>(OnMapCreated);
            EventManager.Instance.RemoveListener<GameEvents.PlayerEnteredArea>(OnPlayerEnteredArea);
        }
    }
}
