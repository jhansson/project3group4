﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class FootStepPlayer : MonoBehaviour {

    [SerializeField] private GameObject leftFoot, rightFoot;
    [SerializeField] private Vector3 rayOffset = new Vector3(0, 1.5f, 0);
    [SerializeField] private LayerMask groundLayerMask;
    [SerializeField] private float rayDistance = 2f;
    private RaycastHit surfaceHitLeft, surfaceHitRight;
    private float currentFootstepLeft, currentFootstepRight;
    private float previousFootstepLeft, previousFootstepRight;
    private Animator anim;

    [SerializeField] private AudioClip footStepSFX;
    private AudioSource audioSource;
    
    [SerializeField, Range(.7f, 1.3f)]
    private float minPitch = .9f, maxPitch = 1.05f;

    [SerializeField, Range(0f, 1f)]
    private float minVolume = .2f, maxVolume = .4f;



    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.spatialBlend = 1;
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (anim != null)
        {
            FootStepLeft();
            FootStepRight();
        }
    }

    private void FootStepLeft()
    {
        currentFootstepLeft = anim.GetFloat("FootstepLeft");
        if (currentFootstepLeft > 0 && previousFootstepLeft < 0)
        {
            Ray rayLeftFoot = new Ray(leftFoot.transform.position + rayOffset, Vector3.down * rayDistance);
            if (Physics.Raycast(rayLeftFoot, out surfaceHitLeft, rayDistance, groundLayerMask))
            {
                Step();
            }
        }
        previousFootstepLeft = anim.GetFloat("FootstepLeft");
    }

    private void FootStepRight()
    {
        currentFootstepRight = anim.GetFloat("FootstepRight");
        if (currentFootstepRight > 0 && previousFootstepRight < 0)
        {
            Ray rayRightFoot = new Ray(rightFoot.transform.position + rayOffset, Vector3.down * rayDistance);
            if (Physics.Raycast(rayRightFoot, out surfaceHitRight, rayDistance, groundLayerMask))
            {
                Step();
            }
        }
        previousFootstepRight = anim.GetFloat("FootstepRight");
    }

    public void Step()
    {
        if(footStepSFX != null)
        {
            audioSource.pitch = Random.Range(minPitch, maxPitch);
            audioSource.volume = Random.Range(minVolume, maxVolume);
            audioSource.PlayOneShot(footStepSFX);
        }

    }

}
