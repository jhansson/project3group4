﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class miniMapTile : MonoBehaviour
{
	[SerializeField, Tooltip("Image to use for T sections.")]
	private Sprite tSectionImage;

    [SerializeField, Tooltip("Image to use for T sections you stand on.")]
    private Sprite tSectionImageCurrent;

    [SerializeField, Tooltip("Image to use for turns.")]
	private Sprite turnImage;

    [SerializeField, Tooltip("Image to use for turns.")]
    private Sprite turnImageCurrent;

    [SerializeField, Tooltip("Image to use for straight paths.")]
	private Sprite straightImage;

    [SerializeField, Tooltip("Image to use for straight paths.")]
    private Sprite straightImageCurrent;

    [SerializeField, Tooltip("Image to use for dead ends.")]
	private Sprite deadendImage;

    [SerializeField, Tooltip("Image to use for dead ends.")]
    private Sprite deadendImageCurrent;

    [SerializeField, Tooltip("Icon to use for portals")]
	private Sprite portalIcon;

    [SerializeField, Tooltip("Image to use for unexplored areas")]
    private Sprite unexploredImage;

    [HideInInspector]
    public Vector2 positionInGrid = Vector2.zero;
	
	[HideInInspector]
	public AreaConnectionType AreaConnectionType;

	private Image img;
	private float imageRotation;

	private Sprite tileSprite;

	//[HideInInspector]
	public bool playerIsOnTile;
	[HideInInspector]
	public bool hasBeenFound;

    private void Awake()
    {
        img = GetComponent<Image>();
    }

    // Use this for initialization
    private void Start ()
	{

	}
	
	// Update is called once per frame
	private void Update () {
		if (img.sprite == null)
		{
            //SetImage();
        }
	}

	public void SetImage()
	{
		switch (AreaConnectionType)
		{
			// T-sections
			case AreaConnectionType.NES:
				imageRotation = 90f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = tSectionImageCurrent;
                }
                else
                {
                    tileSprite = tSectionImage;
                }
				break;
			case AreaConnectionType.WES:
				imageRotation = 0f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = tSectionImageCurrent;
                }
                else
                {
                    tileSprite = tSectionImage;
                }
                break;
			case AreaConnectionType.WNE:
				imageRotation = 180f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = tSectionImageCurrent;
                }
                else
                {
                    tileSprite = tSectionImage;
                }
                break;
			case AreaConnectionType.WNS:
				imageRotation = -90f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = tSectionImageCurrent;
                }
                else
                {
                    tileSprite = tSectionImage;
                }
                break;
			// Turns
			case AreaConnectionType.ES:
				imageRotation = 90f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = turnImageCurrent;
                }
                else
                {
                    tileSprite = turnImage;
                }
                break;
			case AreaConnectionType.WS:
				imageRotation = 0f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = turnImageCurrent;
                }
                else
                {
                    tileSprite = turnImage;
                }
                break;
			case AreaConnectionType.WN:
				imageRotation = -90f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = turnImageCurrent;
                }
                else
                {
                    tileSprite = turnImage;
                }
                break;
			case AreaConnectionType.NE:
				imageRotation = 180f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = turnImageCurrent;
                }
                else
                {
                    tileSprite = turnImage;
                }
                break;
			// Straights
			case AreaConnectionType.NS:
				imageRotation = 0f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = straightImageCurrent;
                }
                else
                {
                    tileSprite = straightImage;
                }
                break;
			case AreaConnectionType.WE:
				imageRotation = 90f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = straightImageCurrent;
                }
                else
                {
                    tileSprite = straightImage;
                }
                break;
			// Dead ends
			case AreaConnectionType.Sx:
				imageRotation = 0f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = deadendImageCurrent;
                }
                else
                {
                    tileSprite = deadendImage;
                }
                break;
			case AreaConnectionType.Wx:
				imageRotation = -90f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = deadendImageCurrent;
                }
                else
                {
                    tileSprite = deadendImage;
                }
                break;
			case AreaConnectionType.Nx:
				imageRotation = 180f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = deadendImageCurrent;
                }
                else
                {
                    tileSprite = deadendImage;
                }
                break;
			case AreaConnectionType.Ex:
				imageRotation = 90f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = deadendImageCurrent;
                }
                else
                {
                    tileSprite = deadendImage;
                }
                break;
			default:
				imageRotation = 0f;
                if (!hasBeenFound)
                {
                    tileSprite = unexploredImage;
                }
                else if (playerIsOnTile)
                {
                    tileSprite = deadendImageCurrent;
                }
                else
                {
                    tileSprite = deadendImage;
                }
                break;
		}

        img.sprite = tileSprite;
        if (tileSprite == unexploredImage)
        {
            GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            GetComponent<RectTransform>().rotation = Quaternion.Euler(0, 0, imageRotation);
        }
	}
}
