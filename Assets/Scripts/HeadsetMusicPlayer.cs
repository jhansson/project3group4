﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class HeadsetMusicPlayer : MonoBehaviour {

    public Playlist playList;
    private AudioSource source;
    [SerializeField] private AudioClip[] normalSongs, personalFavorites;

    public AudioMixerSnapshot WearingHeadphones;
    public AudioMixerSnapshot NormalMusic;

    private int songIndex;
    private Playlist previousList;
    private int previousIndex = 999;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        source.spatialBlend = 1;
        previousList = playList;
    }

    private void OnEnable()
    {       
        NewSong();
    }

    private void Update()
    {
        if(!source.isPlaying)
        {
            NewSong();
        }
        if (gameObject.activeInHierarchy)
        {
            Debug.Log("Should play");
            WearingHeadphones.TransitionTo(0.01f);

        }

    }

    private void NewSong()
    {
        switch(playList)
        {
            case Playlist.Normal:
                RandomizeNewSong(normalSongs);
                break;
            case Playlist.Favorites:
                RandomizeNewSong(personalFavorites);
                break;
            default:
                RandomizeNewSong(normalSongs);
                break;
        }
    }

    private void RandomizeNewSong(AudioClip[] clip)
    {
        if (clip.Length == 0)
        {
            Debug.Log(gameObject.name + " : Headset playlist can't jam with an empty playlist, please add some jam");
            return;
        }
        
        if(clip.Length == 1)
        {
            songIndex = 0;
        }
        else if(playList == previousList)
        {
            songIndex = Random.Range(0, clip.Length);
            if (songIndex == previousIndex)
            {
                RandomizeNewSong(clip);
                return;
            }

            previousList = playList;
            previousIndex = songIndex;
        }

        source.clip = clip[songIndex];
        WearingHeadphones.TransitionTo(0.01f);
        source.Play();
    }

    private void OnDisable()
    {
        NormalMusic.TransitionTo(0.01f);
        source.Stop();
    }
}

public enum Playlist
{
    Normal, Favorites
}
