﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerIKControl : MonoBehaviour {

    protected Animator animator;

    public bool ikActive = false;
    public Transform target = null;

    [Range(0, 1)] public float Weight = 0.0f;

	void Start () {
        animator = GetComponent<Animator>();
    }

    void OnAnimatorIK()
    {
        if (animator)
        {
            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {
                // Set the left hand target position and rotation, if one has been assigned
                if (target != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, Weight);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, Weight);

                    animator.SetIKPosition(AvatarIKGoal.LeftHand, target.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, target.rotation);
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }
}
