﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class IKControl : MonoBehaviour {

    protected Animator animator;

    public bool ikActive = false;
    public GameObject myLeftHand, myRightHand;
    public Transform rightHandObj = null;
    [SerializeField] private Vector3 targetOffset;
    public Transform lookObj = null;
    private bool tracking = true;
    [SerializeField] private GameObject firePit;
    private AvatarIKGoal ikGoal;

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}

    private void Update()
    {
        if (tracking)
            rightHandObj.position = lookObj.position + targetOffset;

    }

    void OnAnimatorIK() {
        if (animator) {
            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive) {
                if ((animator.GetBool("AttackRight")))
                    ikGoal = AvatarIKGoal.RightHand;
                else
                    ikGoal = AvatarIKGoal.LeftHand;

                // Set the look target position, if one has been assigned
                if (lookObj != null) {
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookObj.position);
                }

                // Set the right hand target position and rotation, if one has been assigned
                if (rightHandObj != null) {
                    animator.SetIKPositionWeight(ikGoal, animator.GetFloat("ikWeight"));
                    animator.SetIKRotationWeight(ikGoal, animator.GetFloat("ikWeight"));
                    animator.SetIKPosition(ikGoal, rightHandObj.position);
                    //animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else {
                animator.SetIKPositionWeight(ikGoal, 0);
                animator.SetIKRotationWeight(ikGoal, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }

    public void StartTracking()
    {
        tracking = true;
    }

    public void StopTracking()
    {
        tracking = false;
    }

    public void SpawnFire()
    {
        Vector3 handPos;
        if (animator.GetBool("AttackRight"))
            handPos = new Vector3(myRightHand.transform.position.x, rightHandObj.position.y - targetOffset.y, myRightHand.transform.position.z);
        else
            handPos = new Vector3(myLeftHand.transform.position.x, rightHandObj.position.y - targetOffset.y, myLeftHand.transform.position.z);
        GameObject go = Instantiate(firePit, handPos, Quaternion.identity);
        go.transform.Rotate(-90f, 0, 0);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawCube(rightHandObj.position, new Vector3(0.5f, 0.5f, 0.5f));
    }
}
