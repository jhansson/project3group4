﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound {

    public string name;

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume;
    [Range(.1f, 3f)] 
    public float pitch;

    [Range(.1f , 3f)]
    public float minpitch = 0.8f, maxPitch = 1;

    [HideInInspector]
    public AudioSource source;

    public bool loop;

    public bool is3D = true;
}
