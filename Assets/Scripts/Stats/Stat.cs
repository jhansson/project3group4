﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat  {


    public int baseValue;
    public int minValue;
    public int maxValue;


    private List<int> modifiers = new List<int>();

    public int GetValue()
    {
        int finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }
    //TODO clamp modifier values between the min and max
    public void AddModifier (int modifier)
    {
        if (modifier != 0)
        {
            modifiers.Add(modifier);
        }
    }

    public void RemoveModifier(int modifier)
    {
        if (modifier != 0)
        {
            modifiers.Remove(modifier);
        }

    }

    /// <summary>
    /// Modify the base value positive or negative, clamps between the min value and modifiers
    /// </summary>
    /// <param name="modifier"></param>
    public void ModifyValue(int modifier)
    {
        int finalValue = 0;
        modifiers.ForEach(x => finalValue += x);

        baseValue += modifier;
        baseValue = Mathf.Clamp(baseValue, minValue - finalValue, maxValue);
    }
}
