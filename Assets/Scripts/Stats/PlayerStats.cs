﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats {

    EquipmentManager manager;

    private void Awake()
    {
        if(GetComponent<EquipmentManager>() != null)
        {
            manager = GetComponent<EquipmentManager>();
            manager.onEquipmentChanged += OnEquipmentChanged;
        }

    }

    private void OnEquipmentChanged(EQ newItem, EQ oldItem)
    {
        if (oldItem != null)
        {
            speed.RemoveModifier(oldItem.speedModifier);
        }

        if (newItem != null)
        {
            speed.AddModifier(newItem.speedModifier);
        }
    }

}
