﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashBarScript : MonoBehaviour {

    [SerializeField]
    private Image fillImage;

    private float meterFull = 1;
    private float meterEmpty = 0;

    private CharacterMovement characterMovement;

	// Use this for initialization
	void Start () {
        fillImage.fillAmount = meterFull;
        characterMovement = FindObjectOfType<PlayerController>().GetComponent<CharacterMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        if (characterMovement.isDashing == true)
        {
            fillImage.fillAmount = meterEmpty;
            fillImage.fillAmount = Mathf.Clamp ((Time.time - characterMovement.dashStartTime) / 1, 0f, 1f);
        }
	}


}
