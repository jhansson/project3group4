﻿using UnityEngine;

public class AnimationEffecter : MonoBehaviour {

    public GameObject vfx;
    public Transform anchorPoint;
    public GameObject rock;

    public void PlayEffect()
    {
        if(vfx != null && anchorPoint != null)
            Instantiate(vfx, anchorPoint);
    }

    public void Holding()
    {
        if (rock != null)
            rock.SetActive(true);
    }

    public void NotHolding()
    {
        // Deactivate the GameObject for the stone, we should now replace it with a particle
        if (rock != null)
            rock.SetActive(false);
    }
}
