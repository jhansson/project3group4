﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSpin : StateMachineBehaviour {

    private ProjectileShooter projectile;
    private float timer;
    private AIController entity;
    private float targetRotation;
    float rotationleft = 360;
    [SerializeField] private float rotationspeed = 20;
    private TurretStates turretStates;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        entity = animator.gameObject.GetComponent<AIController>();
        turretStates = entity.gameObject.GetComponent<TurretStates>();
        projectile = entity.GetComponent<ProjectileShooter>();
        projectile.projectileToShoot = projectile.projectiles[1]; //rapid fire projectile
        timer = Time.time;
        rotationleft = 360;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float rotation = rotationspeed * Time.deltaTime;
        if (rotationleft > rotation)
        {
            rotationleft -= rotation;
        }
        else
        {
            rotation = rotationleft;
            rotationleft = 0;
            animator.SetBool(turretStates.GetCurrentState(), false);

        }
        entity.transform.Rotate(0, rotation, 0);

        //SpinAround();

        if (projectile.projectileToShoot != null)
            projectile.Shoot();

    }

}
