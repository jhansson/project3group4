﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CyclopsThrowing : StateMachineBehaviour {


    private AIController entity;
    private NavMeshAgent agent;
    private Animator anim;
    private ProjectileShooter projectileSpawner;

    [HideInInspector] public Transform target;
    private float giveUpDistance = 20;
    private float distanceToTarget;
    private float currentVertical;
    private float vertical;
    private float timer;
    private bool canAttack;
    private float targetRotation;
    private float turnSmoothVelocity;
    private float turnSmoothTime = .1f;
    [SerializeField] private float meleeAttackRange = 3.5f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        anim = animator;
        entity = animator.gameObject.GetComponent<AIController>();
        agent = animator.gameObject.GetComponent<NavMeshAgent>();
        projectileSpawner = entity.GetComponent<ProjectileShooter>();
        agent.enabled = true;
        anim.SetBool("RangedAttack", false);
        anim.SetBool("Attack", false);
        timer = Time.time;
        target = entity.target.transform;
        agent.isStopped = false;
        agent.stoppingDistance = entity.attackRange;
        agent.speed = entity.chaseSpeed;
        agent.SetDestination(target.position);

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        distanceToTarget = Vector3.Distance(agent.transform.position, target.position);

        canAttack = (Time.time >= timer + entity.attackDelay);
        RotateTowards((target.position - agent.transform.position).normalized);
    }


    public void RotateTowards(Vector3 direction)
    {
        targetRotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        agent.transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(
            agent.transform.eulerAngles.y,
            targetRotation,
            ref turnSmoothVelocity,
            turnSmoothTime);
    }
}
