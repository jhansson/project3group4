﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAimShoot : StateMachineBehaviour {


    private ProjectileShooter projectile;
    private float turnSmoothVelocity;
    [SerializeField] private float turnSmoothTime = .1f;
    [SerializeField] private float timeToSwitch = 10;
    private float spinTimer;
    private float timer;
    private AIController entity;
    private float targetRotation;
    private TurretStates turretStates;
    [SerializeField] private float giveUpDistance = 10;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        entity = animator.gameObject.GetComponent<AIController>();
        animator.SetBool("AimShoot", true);
        turretStates = entity.GetComponent<TurretStates>();
        projectile = entity.GetComponent<ProjectileShooter>();
        projectile.projectileToShoot = projectile.projectiles[0]; //normal projectile
        timer = Time.time;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(Time.time >= timer + timeToSwitch)
        {
            //Switch state
            animator.SetBool(turretStates.NextState(), true);
        }

        if (entity.target != null)
            RotateTowards((entity.transform.position - entity.target.transform.position).normalized);

        if (Vector3.Distance(entity.transform.position, entity.target.transform.position) >= giveUpDistance)
            return;

        if (projectile.projectileToShoot != null)
            projectile.Shoot();
    }

    private void RotateTowards(Vector3 direction)
    {
        targetRotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        entity.transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(
            entity.transform.eulerAngles.y,
            targetRotation,
            ref turnSmoothVelocity,
            turnSmoothTime);
    }
}
