﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HadesIdle : StateMachineBehaviour {

    private Animator anim;
    [SerializeField, Range(0, 100)]
    private float chanceToHit = 50;
    private IHealth health;
    [SerializeField, Range(0, 1), Tooltip("The health percentage to reach before Hades starts to spawn enemies")]
    private float percentageToSpawnEnemies = .75f;
    [SerializeField, Range(0, 1), Tooltip("The health percentage to reach before Hades starts to drop bombs")]
    private float percentageToSpawnFireballs = .4f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        anim = animator;
        health = anim.GetComponent<IHealth>();
        anim.SetBool("Hit", false);
        if (animator.gameObject.GetComponent<FireballSpawner>() != null)
            animator.gameObject.GetComponent<FireballSpawner>().enabled = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Spawns the fireball rain
        if(health != null)
        {
            if (health.GetHealthPercentage() <= percentageToSpawnFireballs)
            {
                if(animator.gameObject.GetComponent<FireballSpawner>() != null)
                    animator.gameObject.GetComponent<FireballSpawner>().enabled = true;
            }
        }

        if (health != null)
        {
            if (health.GetHealthPercentage() <= .7f)
            {
                if (animator.gameObject.GetComponent<HadesWall>() != null)
                    if(!animator.gameObject.GetComponent<HadesWall>().alreadySpawned)
                    {
                        animator.gameObject.GetComponent<HadesWall>().SetPortalActiveState(true);
                    }

            }
        }

        if (Random.Range(0, 100) < chanceToHit)
        {
            Vector3 myRight = anim.transform.TransformDirection(Vector3.right);
            Vector3 other = anim.transform.GetComponent<IKControl>().rightHandObj.transform.position - animator.transform.position;
            if (Vector3.Dot(myRight, other) >= 0)
                animator.SetBool("AttackRight", true);
            else
                animator.SetBool("AttackRight", false);
            animator.SetBool("Hit", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
