﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretStates : MonoBehaviour {

    public enum TurretState { AimShoot, Spin };
    [SerializeField] private TurretState[] states;
    private int stateCounter;

    public string NextState()
    {
        stateCounter++;
        if (stateCounter >= states.Length)
            stateCounter = 0;
        return states[stateCounter].ToString();
    }

    public string GetNextState()
    {
        if (stateCounter >= states.Length)
            return states[0].ToString();
        else
            return states[stateCounter + 1].ToString();
    }

    public string GetCurrentState()
    {
        return states[stateCounter].ToString();
    }
}
