﻿Shader "Custom/WS_Clip" {
	Properties {
		_Albedo ("Albedo", 2D) = "gray"
		//_Glossiness("Glossiness", 2D) = "gray"
		//_Metallic("Metallic", 2D) = "gray"
		//_Normal("Normal", 2D) = "bump"
		//_Clip("Clip", float) = 10.0
		_Mask("Foo", 2D) = "white"
	}
	SubShader {

		/*
			To Get Alpha working, add 'alpha' to the surf pragma, and set RenderType, Queue to Transparent.
		*/

		///*
		Tags {
			"RenderType"="Opaque"
			"Queue"="Overlay"
		}

		//ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		//*/

		/*
		Tags {
			"RenderType"="Opaque"
			"Queue"="Overlay"
		}

		LOD 200
		*/

		/*
		Tags{
			"RenderType"="Transparent"
			"Queue"="Transparent"
			"IgnoreProjector"="True"
		}

		

		// Think the alpha issue is related to blend ...
		Pass {
			ZWrite Off
			ColorMask RGB
			Blend Off
		}

		// UsePass "Transparent/Diffuse/FORWARD"

		*/
		LOD 200
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _Albedo;

		struct Input {
			float2 uv_Albedo;
			//float2 uv_BumpMap;
			//float3 worldPos;
			float4 screenPos;
		};

		/*
		sampler2D _Glossiness;
		sampler2D _Metallic;
		sampler2D _Normal;
		*/
		//half _Clip;

		sampler2D _Mask;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf(Input IN, inout SurfaceOutputStandard o) {

			// For now, let's clip rendering based on worldpos
			// clip(_Clip - IN.worldPos.y);

			fixed4 c = tex2D(_Albedo, IN.uv_Albedo);
			o.Albedo = c.rgb;

			// Metallic and smoothness from textures
			//o.Metallic = tex2D(_Metallic, IN.uv_Albedo);
			//o.Smoothness = tex2D(_Glossiness, IN.uv_Albedo);

			// Screenspace alpha blend, 
			float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
			clip((tex2D(_Mask, screenUV).r - 0.5) * -1);
			//o.Alpha = 1 - tex2D(_Mask, screenUV).r;
			// o.Alpha = 1;

			// Apply normal map
			//o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_BumpMap));
		}

		ENDCG
	}
	FallBack "Diffuse"
}
