// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Foilage"
{
	Properties
	{
		[Header(Translucency)]
		_Translucency("Strength", Range( 0 , 50)) = 1
		_TransNormalDistortion("Normal Distortion", Range( 0 , 1)) = 0.1
		_TransScattering("Scaterring Falloff", Range( 1 , 50)) = 2
		_TransDirect("Direct", Range( 0 , 1)) = 1
		_TransAmbient("Ambient", Range( 0 , 1)) = 0.2
		_TransShadow("Shadow", Range( 0 , 1)) = 0.9
		_Albedo("Albedo", 2D) = "white" {}
		_Cutoff( "Mask Clip Value", Float ) = 0
		_Normal("Normal", 2D) = "bump" {}
		_WindDirection("Wind Direction", Vector) = (1,0,0,0)
		_WindForce("Wind Force", Float) = 0.23
		_AlphaWindFrequency("Alpha Wind Frequency", Float) = 7.47
		_WindFrequency("Wind Frequency", Float) = 1.05
		_WindSpeed("Wind Speed", Float) = 34.12
		_AlphaWindSpeed("Alpha Wind Speed", Float) = 7.47
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_Metallic("Metallic", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Transparent+300" }
		Cull Back
		Blend One Zero , SrcAlpha OneMinusSrcAlpha
		AlphaToMask On
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		struct SurfaceOutputStandardCustom
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			fixed3 Transmission;
			fixed3 Translucency;
		};

		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _Metallic;
		uniform float _Smoothness;
		uniform half _Translucency;
		uniform half _TransNormalDistortion;
		uniform half _TransScattering;
		uniform half _TransDirect;
		uniform half _TransAmbient;
		uniform half _TransShadow;
		uniform float _WindForce;
		uniform float4 _WindDirection;
		uniform float _WindFrequency;
		uniform float _WindSpeed;
		uniform float _AlphaWindFrequency;
		uniform float _AlphaWindSpeed;
		uniform float _Cutoff = 0;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 temp_output_10_0 = ( float4( mul( unity_ObjectToWorld, float4( ase_vertex3Pos , 0.0 ) ).xyz , 0.0 ) - _WindDirection );
			float4 normalizeResult9 = normalize( temp_output_10_0 );
			float temp_output_22_0 = length( temp_output_10_0 );
			float lerpResult6 = lerp( sin( ( temp_output_22_0 + ( ( temp_output_22_0 / _WindFrequency ) + ( _WindSpeed * _Time.y ) ) ) ) , sin( ( temp_output_22_0 + ( ( temp_output_22_0 / _AlphaWindFrequency ) + ( _Time.y * _AlphaWindSpeed ) ) ) ) , (0.0 + (_SinTime.w - -1.0) * (1.0 - 0.0) / (1.0 - -1.0)));
			v.vertex.xyz += ( v.color * ( ( _WindForce * normalizeResult9 ) * lerpResult6 ) ).rgb;
		}

		inline half4 LightingStandardCustom(SurfaceOutputStandardCustom s, half3 viewDir, UnityGI gi )
		{
			#if !DIRECTIONAL
			float3 lightAtten = gi.light.color;
			#else
			float3 lightAtten = lerp( _LightColor0.rgb, gi.light.color, _TransShadow );
			#endif
			half3 lightDir = gi.light.dir + s.Normal * _TransNormalDistortion;
			half transVdotL = pow( saturate( dot( viewDir, -lightDir ) ), _TransScattering );
			half3 translucency = lightAtten * (transVdotL * _TransDirect + gi.indirect.diffuse * _TransAmbient) * s.Translucency;
			half4 c = half4( s.Albedo * translucency * _Translucency, 0 );

			half3 transmission = max(0 , -dot(s.Normal, gi.light.dir)) * gi.light.color * s.Transmission;
			half4 d = half4(s.Albedo * transmission , 0);

			SurfaceOutputStandard r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Metallic = s.Metallic;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandard (r, viewDir, gi) + c + d;
		}

		inline void LightingStandardCustom_GI(SurfaceOutputStandardCustom s, UnityGIInput data, inout UnityGI gi )
		{
			UNITY_GI(gi, s, data);
		}

		void surf( Input i , inout SurfaceOutputStandardCustom o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode1 = tex2D( _Albedo, uv_Albedo );
			o.Albedo = tex2DNode1.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			float3 temp_cast_1 = (tex2DNode1.a).xxx;
			o.Transmission = temp_cast_1;
			float3 temp_cast_2 = (tex2DNode1.a).xxx;
			o.Translucency = temp_cast_2;
			o.Alpha = tex2DNode1.a;
			#if UNITY_PASS_SHADOWCASTER
			clip( tex2DNode1.a - _Cutoff );
			#endif
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustom keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				float4 texcoords01 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.texcoords01 = float4( v.texcoord.xy, v.texcoord1.xy );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord.xy = IN.texcoords01.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandardCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
1927;29;1906;1004;2375.536;591.4694;1.3;True;True
Node;AmplifyShaderEditor.ObjectToWorldMatrixNode;39;-1819.138,-81.87001;Float;False;0;1;FLOAT4x4
Node;AmplifyShaderEditor.PosVertexDataNode;38;-1806.136,-9.069921;Float;False;0;0;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;-1617.637,-77.96911;Float;False;2;2;0;FLOAT4x4;0.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;False;1;FLOAT3;0.0;False;1;FLOAT3
Node;AmplifyShaderEditor.Vector4Node;5;-1602.43,34.04253;Float;False;Property;_WindDirection;Wind Direction;9;0;1,0,0,0;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;10;-1394.43,-27.95761;Float;False;2;0;FLOAT3;0.0,0,0,0;False;1;FLOAT4;0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.RangedFloatNode;15;-1270.445,107.6942;Float;False;Property;_WindFrequency;Wind Frequency;12;0;1.05;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;16;-1256.917,280.8327;Float;False;Property;_WindSpeed;Wind Speed;13;0;34.12;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;17;-1318.441,448.1583;Float;False;Property;_AlphaWindSpeed;Alpha Wind Speed;14;0;7.47;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.LengthOpNode;22;-1204.551,12.36091;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;14;-1314.646,198.6943;Float;False;Property;_AlphaWindFrequency;Alpha Wind Frequency;11;0;7.47;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;18;-1278.717,357.8332;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-1063.688,315.1369;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-1064.087,412.7368;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;24;-987.0847,135.5102;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;23;-988.4432,37.75363;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;28;-809.2998,65.41841;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;29;-806.5847,184.8987;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;35;-681.526,321.8479;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;34;-673.1433,138.8275;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SinTimeNode;7;-681.0211,464.3221;Float;False;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SinOpNode;27;-543.3599,230.1191;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.NormalizeNode;9;-1227.978,-82.95653;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.SinOpNode;26;-542.0026,330.5914;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.TFHCRemapNode;8;-534.0209,468.3222;Float;False;5;0;FLOAT;0.0;False;1;FLOAT;-1.0;False;2;FLOAT;1.0;False;3;FLOAT;0.0;False;4;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;13;-1225.283,-181.3058;Float;False;Property;_WindForce;Wind Force;10;0;0.23;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-986.9886,-68.57319;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT4;0;False;1;FLOAT4
Node;AmplifyShaderEditor.LerpOp;6;-339.0474,304.8516;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.VertexColorNode;12;-247.0376,34.63939;Float;False;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-157.8359,241.0752;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.WorldPosInputsNode;4;-2018.294,-193.0656;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-242.4123,-635.1412;Float;True;Property;_Albedo;Albedo;6;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;32;-254.6569,-169.4183;Float;False;Property;_Metallic;Metallic;16;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-20.24758,173.108;Float;False;2;2;0;COLOR;0.0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SamplerNode;2;-243.4123,-439.1412;Float;True;Property;_Normal;Normal;8;0;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;33;-254.6573,-84.30353;Float;False;Property;_Smoothness;Smoothness;15;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;153,-145;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Foilage;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Custom;0;True;True;300;True;TransparentCutout;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;SrcAlpha;OneMinusSrcAlpha;2;SrcAlpha;OneMinusSrcAlpha;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;7;0;-1;-1;0;0;0;True;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;40;0;39;0
WireConnection;40;1;38;0
WireConnection;10;0;40;0
WireConnection;10;1;5;0
WireConnection;22;0;10;0
WireConnection;19;0;16;0
WireConnection;19;1;18;0
WireConnection;20;0;18;0
WireConnection;20;1;17;0
WireConnection;24;0;22;0
WireConnection;24;1;14;0
WireConnection;23;0;22;0
WireConnection;23;1;15;0
WireConnection;28;0;23;0
WireConnection;28;1;19;0
WireConnection;29;0;24;0
WireConnection;29;1;20;0
WireConnection;35;0;22;0
WireConnection;35;1;29;0
WireConnection;34;0;22;0
WireConnection;34;1;28;0
WireConnection;27;0;34;0
WireConnection;9;0;10;0
WireConnection;26;0;35;0
WireConnection;8;0;7;4
WireConnection;21;0;13;0
WireConnection;21;1;9;0
WireConnection;6;0;27;0
WireConnection;6;1;26;0
WireConnection;6;2;8;0
WireConnection;30;0;21;0
WireConnection;30;1;6;0
WireConnection;31;0;12;0
WireConnection;31;1;30;0
WireConnection;0;0;1;0
WireConnection;0;1;2;0
WireConnection;0;3;32;0
WireConnection;0;4;33;0
WireConnection;0;6;1;4
WireConnection;0;7;1;4
WireConnection;0;9;1;4
WireConnection;0;10;1;4
WireConnection;0;11;31;0
ASEEND*/
//CHKSM=AFD5BCA94E5C880783AF9DCF943EEF31B31BC54E