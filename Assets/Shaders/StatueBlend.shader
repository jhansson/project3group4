﻿Shader "Custom/StatueBlend" {
	Properties {
		_Albedo01 ("Albedo Primary", 2D) = "white" {}
		_Albedo02("Albedo Secondary", 2D) = "white" {}

		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_Normal01("Normal Primary", 2D) = "bump" {}
		_Normal02("Normal Secondary", 2D) = "bump" {}

		_Emissive("Emissive", 2D) = "white" {}

		_Blend("Blend", Range(0, 1)) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque"}
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		// Two Albedos to blend between depending on state
		sampler2D _Albedo01;
		sampler2D _Albedo02;

		// And two normal maps to blend between
		sampler2D _Normal01;
		sampler2D _Normal02;

		sampler2D _Emissive;

		struct Input {
			float2 uv_MainTex;
		};

		// Values will be same for both states,
		half _Glossiness;
		half _Metallic;

		half _Blend;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			//UNITY_DEFINE_INSTANCED_PROP(fixed, _Blend)
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 color = lerp(tex2D(_Albedo01, IN.uv_MainTex), tex2D(_Albedo02, IN.uv_MainTex), _Blend);
			o.Albedo = color.rgb;

			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

			o.Normal = lerp(UnpackNormal(tex2D(_Normal01, IN.uv_MainTex)), UnpackNormal(tex2D(_Normal02, IN.uv_MainTex)), _Blend);

			o.Emission = lerp(half3(0, 0, 0), tex2D(_Emissive, IN.uv_MainTex).rgb, _Blend);

			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
