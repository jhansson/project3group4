﻿// Get a blurred image from render. 
// Based on Unity GrabPass example and the blur solution by user vinipc (https://forum.unity.com/threads/simple-optimized-blur-shader.185327/#post-3038561)

Shader "Custom/Blur" {
	Properties {
		_Size("Blur", Range(0, 30)) = 1
	}
	SubShader {
		// Place in Queue just after everything to be blurred.
		Tags{ "Queue" = "Transparent" }

		GrabPass {
			"_HBlur"
		}

		Pass {
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			struct v2f {
				float4 grabPos : TEXCOORD0;
				float4 pos : SV_POSITION;
			};

			v2f vert(appdata_base v) {
				v2f o;
				// use UnityObjectToClipPos from UnityCG.cginc to calculate 
				// the clip-space of the vertex
				o.pos = UnityObjectToClipPos(v.vertex);
				// use ComputeGrabScreenPos function from UnityCG.cginc
				// to get the correct texture coordinate
				o.grabPos = ComputeGrabScreenPos(o.pos);
				return o;
			}

			sampler2D _HBlur;
			float4 _HBlur_TexelSize;
			float _Size;

			half4 frag(v2f i) : SV_Target
			{
				half4 bgcolor = tex2Dproj(_HBlur, i.grabPos);
				half4 sum = half4(0, 0, 0, 0);

				#define GRABPIXEL(weight,kernelx) tex2Dproj( _HBlur, UNITY_PROJ_COORD(float4(i.grabPos.x + _HBlur_TexelSize.x * kernelx * _Size, i.grabPos.y, i.grabPos.z, i.grabPos.w))) * weight

				sum += GRABPIXEL(0.05, -4.0);
				sum += GRABPIXEL(0.09, -3.0);
				sum += GRABPIXEL(0.12, -2.0);
				sum += GRABPIXEL(0.15, -1.0);
				sum += GRABPIXEL(0.18, 0.0);
				sum += GRABPIXEL(0.15, +1.0);
				sum += GRABPIXEL(0.12, +2.0);
				sum += GRABPIXEL(0.09, +3.0);
				sum += GRABPIXEL(0.05, +4.0);

				return sum;
			}

			ENDCG
		}

		GrabPass{
				"_VBlur"
		}

		Pass{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			struct v2f {
				float4 grabPos : TEXCOORD0;
				float4 pos : SV_POSITION;
			};

			v2f vert(appdata_base v) {
				v2f o;
				// use UnityObjectToClipPos from UnityCG.cginc to calculate 
				// the clip-space of the vertex
				o.pos = UnityObjectToClipPos(v.vertex);
				// use ComputeGrabScreenPos function from UnityCG.cginc
				// to get the correct texture coordinate
				o.grabPos = ComputeGrabScreenPos(o.pos);
				return o;
			}

			sampler2D _VBlur;
			float4 _VBlur_TexelSize;
			float _Size;

			half4 frag(v2f i) : SV_Target
			{
				half4 sum = half4(0, 0, 0, 0);

				#define GRABPIXEL(weight,kernely) tex2Dproj(_VBlur, UNITY_PROJ_COORD(float4(i.grabPos.x, i.grabPos.y + _VBlur_TexelSize.y * kernely * _Size, i.grabPos.z, i.grabPos.w))) * weight

				sum += GRABPIXEL(0.05, -4.0);
				sum += GRABPIXEL(0.09, -3.0);
				sum += GRABPIXEL(0.12, -2.0);
				sum += GRABPIXEL(0.15, -1.0);
				sum += GRABPIXEL(0.18, 0.0);
				sum += GRABPIXEL(0.15, +1.0);
				sum += GRABPIXEL(0.12, +2.0);
				sum += GRABPIXEL(0.09, +3.0);
				sum += GRABPIXEL(0.05, +4.0);

				return sum;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
