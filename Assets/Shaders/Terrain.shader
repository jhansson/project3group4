// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Terrain"
{
	Properties
	{
		_SplatMap("SplatMap", 2D) = "white" {}
		_UVRotation("UV Rotation", Float) = 90
		_AlbedoR("Albedo (R)", 2D) = "white" {}
		_NormalR("Normal (R)", 2D) = "bump" {}
		_MetallicR("Metallic (R)", Range( 0 , 1)) = 0
		_SmoothnessR("Smoothness (R)", Range( 0 , 1)) = 0
		_AlbedoG("Albedo (G)", 2D) = "white" {}
		_NormalG("Normal (G)", 2D) = "bump" {}
		_MetallicG("Metallic (G)", Range( 0 , 1)) = 0
		_SmoothnessG("Smoothness (G)", Range( 0 , 1)) = 0
		_AlbedoB("Albedo (B)", 2D) = "white" {}
		_NormalB("Normal (B)", 2D) = "bump" {}
		_MetallicB("Metallic (B)", Range( 0 , 1)) = 0
		_SmoothnessB("Smoothness (B)", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _SplatMap;
		uniform float4 _SplatMap_ST;
		uniform sampler2D _NormalR;
		uniform sampler2D _AlbedoR;
		uniform float4 _AlbedoR_ST;
		uniform float _UVRotation;
		uniform sampler2D _NormalG;
		uniform sampler2D _NormalB;
		uniform sampler2D _AlbedoG;
		uniform sampler2D _AlbedoB;
		uniform float _MetallicR;
		uniform float _MetallicG;
		uniform float _MetallicB;
		uniform float _SmoothnessR;
		uniform float _SmoothnessG;
		uniform float _SmoothnessB;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_SplatMap = i.uv_texcoord * _SplatMap_ST.xy + _SplatMap_ST.zw;
			float4 tex2DNode1 = tex2D( _SplatMap, uv_SplatMap );
			float2 uv_AlbedoR = i.uv_texcoord * _AlbedoR_ST.xy + _AlbedoR_ST.zw;
			float cos6 = cos( radians( _UVRotation ) );
			float sin6 = sin( radians( _UVRotation ) );
			float2 rotator6 = mul( uv_AlbedoR - float2( 0.5,0.5 ) , float2x2( cos6 , -sin6 , sin6 , cos6 )) + float2( 0.5,0.5 );
			float4 weightedBlendVar23 = tex2DNode1;
			float3 weightedAvg23 = ( ( weightedBlendVar23.x*UnpackNormal( tex2D( _NormalR, rotator6 ) ) + weightedBlendVar23.y*UnpackNormal( tex2D( _NormalG, rotator6 ) ) + weightedBlendVar23.z*UnpackNormal( tex2D( _NormalB, rotator6 ) ) + weightedBlendVar23.w*float3( 0,0,0 ) )/( weightedBlendVar23.x + weightedBlendVar23.y + weightedBlendVar23.z + weightedBlendVar23.w ) );
			o.Normal = weightedAvg23;
			float4 weightedBlendVar19 = tex2DNode1;
			float4 weightedAvg19 = ( ( weightedBlendVar19.x*tex2D( _AlbedoR, rotator6 ) + weightedBlendVar19.y*tex2D( _AlbedoG, rotator6 ) + weightedBlendVar19.z*tex2D( _AlbedoB, rotator6 ) + weightedBlendVar19.w*float4( 0.0,0,0,0 ) )/( weightedBlendVar19.x + weightedBlendVar19.y + weightedBlendVar19.z + weightedBlendVar19.w ) );
			o.Albedo = weightedAvg19.rgb;
			float4 weightedBlendVar40 = tex2DNode1;
			float weightedAvg40 = ( ( weightedBlendVar40.x*_MetallicR + weightedBlendVar40.y*_MetallicG + weightedBlendVar40.z*_MetallicB + weightedBlendVar40.w*0.0 )/( weightedBlendVar40.x + weightedBlendVar40.y + weightedBlendVar40.z + weightedBlendVar40.w ) );
			o.Metallic = weightedAvg40;
			float4 weightedBlendVar41 = tex2DNode1;
			float weightedAvg41 = ( ( weightedBlendVar41.x*_SmoothnessR + weightedBlendVar41.y*_SmoothnessG + weightedBlendVar41.z*_SmoothnessB + weightedBlendVar41.w*0.0 )/( weightedBlendVar41.x + weightedBlendVar41.y + weightedBlendVar41.z + weightedBlendVar41.w ) );
			o.Smoothness = weightedAvg41;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
1927;29;1906;1004;2374.506;1339.415;3.078107;True;True
Node;AmplifyShaderEditor.CommentaryNode;37;-1141.787,-172.6035;Float;False;648.5154;449.3411;Allowing user to rotate the UV;5;7;8;6;10;9;UV;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-1091.787,161.7376;Float;False;Property;_UVRotation;UV Rotation;1;0;90;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.Vector2Node;8;-932.4786,26.00376;Float;False;Constant;_UVPivot;UV Pivot;3;0;0.5,0.5;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.RadiansOpNode;10;-891.1522,160.97;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;7;-986.3294,-122.6036;Float;False;0;2;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.CommentaryNode;38;-357.1895,122.4469;Float;False;372.3347;643.7726;Comment;4;15;16;33;30;Green Channel;0.03834212,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;39;-354.4758,828.1041;Float;False;372.3347;643.2528;Comment;4;17;18;32;31;Blue Channel;0.02027321,0,1,1;0;0
Node;AmplifyShaderEditor.RotatorNode;6;-706.2719,-49.15729;Float;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;5.0;False;1;FLOAT2
Node;AmplifyShaderEditor.CommentaryNode;36;-359.1184,-597.1198;Float;False;372.3347;643.6354;Comment;4;3;2;24;28;Red Channel;1,0,0,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-290.4572,565.9937;Float;False;Property;_MetallicG;Metallic (G);8;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;17;-304.4758,878.1041;Float;True;Property;_AlbedoB;Albedo (B);10;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;31;-290.1221,1274.259;Float;False;Property;_MetallicB;Metallic (B);12;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-242.0848,-953.2051;Float;True;Property;_SplatMap;SplatMap;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;18;-302.1411,1076.578;Float;True;Property;_NormalB;Normal (B);11;0;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;32;-289.8165,1356.357;Float;False;Property;_SmoothnessB;Smoothness (B);13;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;33;-293.2656,651.2197;Float;False;Property;_SmoothnessG;Smoothness (G);9;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;24;-294.7906,-152.1872;Float;False;Property;_MetallicR;Metallic (R);4;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;2;-309.1184,-547.1198;Float;True;Property;_AlbedoR;Albedo (R);2;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;3;-306.7836,-348.6447;Float;True;Property;_NormalR;Normal (R);3;0;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;16;-304.8547,370.9217;Float;True;Property;_NormalG;Normal (G);7;0;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;15;-307.1895,172.4469;Float;True;Property;_AlbedoG;Albedo (G);6;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;28;-297.0556,-68.48437;Float;False;Property;_SmoothnessR;Smoothness (R);5;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.WeightedBlendNode;40;383.6402,53.20161;Float;False;5;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.WeightedBlendNode;41;381.8331,221.1681;Float;False;5;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.WeightedBlendNode;19;382.6533,-285.1587;Float;False;5;0;COLOR;0.0;False;1;COLOR;1.0;False;2;COLOR;0.0,0,0,0;False;3;COLOR;0.0,0,0,0;False;4;COLOR;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.WeightedBlendNode;23;384.2772,-117.7975;Float;False;5;0;COLOR;0,0,0,0;False;1;FLOAT3;1.0;False;2;FLOAT3;0.0,0,0,0;False;3;FLOAT3;0.0,0,0,0;False;4;FLOAT3;0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;614.8328,-28.9762;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Terrain;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;10;0;9;0
WireConnection;6;0;7;0
WireConnection;6;1;8;0
WireConnection;6;2;10;0
WireConnection;17;1;6;0
WireConnection;18;1;6;0
WireConnection;2;1;6;0
WireConnection;3;1;6;0
WireConnection;16;1;6;0
WireConnection;15;1;6;0
WireConnection;40;0;1;0
WireConnection;40;1;24;0
WireConnection;40;2;30;0
WireConnection;40;3;31;0
WireConnection;41;0;1;0
WireConnection;41;1;28;0
WireConnection;41;2;33;0
WireConnection;41;3;32;0
WireConnection;19;0;1;0
WireConnection;19;1;2;0
WireConnection;19;2;15;0
WireConnection;19;3;17;0
WireConnection;23;0;1;0
WireConnection;23;1;3;0
WireConnection;23;2;16;0
WireConnection;23;3;18;0
WireConnection;0;0;19;0
WireConnection;0;1;23;0
WireConnection;0;3;40;0
WireConnection;0;4;41;0
ASEEND*/
//CHKSM=92B14AE41A920B0B35C72FDD912E11AAC804F218