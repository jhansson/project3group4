﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/DrawSimple" {
	SubShader{
		ZWrite Off
		ZTest Always
		Lighting Off
		Pass {

			CGPROGRAM
			#pragma vertex VShader
			#pragma fragment FShader

			struct v2f {
				//float4 pos: SV_POSITION;
				float4 pos: POSITION;
			};

			// Return vertex position,
			v2f VShader(v2f i) {
				v2f o;
				o.pos = UnityObjectToClipPos(i.pos);
				return o;
			}

			// Return white
			half4 FShader():COLOR0{
				return half4(1,1,1,1);
			}
			ENDCG
		}
	}
}
