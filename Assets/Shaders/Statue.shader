// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "StatueBlend"
{
	Properties
	{
		_Albedo02("Albedo 02", 2D) = "white" {}
		_Albedo01("Albedo 01", 2D) = "white" {}
		_Emissive("Emissive", 2D) = "white" {}
		_Normal02("Normal 02", 2D) = "bump" {}
		_Normal01("Normal 01", 2D) = "bump" {}
		_Blend("Blend", Range( 0 , 1)) = 0
		_AO("AO", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Normal02;
		uniform float4 _Normal02_ST;
		uniform sampler2D _Normal01;
		uniform float4 _Normal01_ST;
		uniform half _Blend;
		uniform sampler2D _Albedo02;
		uniform float4 _Albedo02_ST;
		uniform sampler2D _Albedo01;
		uniform float4 _Albedo01_ST;
		uniform sampler2D _Emissive;
		uniform float4 _Emissive_ST;
		uniform sampler2D _AO;
		uniform float4 _AO_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal02 = i.uv_texcoord * _Normal02_ST.xy + _Normal02_ST.zw;
			float2 uv_Normal01 = i.uv_texcoord * _Normal01_ST.xy + _Normal01_ST.zw;
			float3 lerpResult7 = lerp( UnpackNormal( tex2D( _Normal02, uv_Normal02 ) ) , UnpackNormal( tex2D( _Normal01, uv_Normal01 ) ) , _Blend);
			o.Normal = lerpResult7;
			float2 uv_Albedo02 = i.uv_texcoord * _Albedo02_ST.xy + _Albedo02_ST.zw;
			float2 uv_Albedo01 = i.uv_texcoord * _Albedo01_ST.xy + _Albedo01_ST.zw;
			float4 lerpResult8 = lerp( tex2D( _Albedo02, uv_Albedo02 ) , tex2D( _Albedo01, uv_Albedo01 ) , _Blend);
			o.Albedo = lerpResult8.rgb;
			float2 uv_Emissive = i.uv_texcoord * _Emissive_ST.xy + _Emissive_ST.zw;
			float4 lerpResult6 = lerp( float4( float3(0,0,0) , 0.0 ) , tex2D( _Emissive, uv_Emissive ) , _Blend);
			o.Emission = lerpResult6.rgb;
			float2 uv_AO = i.uv_texcoord * _AO_ST.xy + _AO_ST.zw;
			o.Occlusion = tex2D( _AO, uv_AO ).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
1921;23;1918;1016;1275.235;1084.521;1.745892;True;True
Node;AmplifyShaderEditor.SamplerNode;4;18.91784,-467.2826;Float;True;Property;_Normal02;Normal 02;0;0;Assets/Environments/HeroAssets/Textures/T_Statue02_Emissive_N.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;3;22.82387,-277.7104;Float;True;Property;_Normal01;Normal 01;0;0;Assets/Environments/HeroAssets/Textures/T_Statue02_N.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;5;28.63891,1.899144;Float;True;Property;_Emissive;Emissive;0;0;Assets/Environments/HeroAssets/Textures/T_Statue02_Emissive.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.Vector3Node;10;152.495,194.8621;Float;False;Constant;_NoEmissive;No Emissive;5;0;0,0,0;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;2;20.05652,-663.8851;Float;True;Property;_Albedo01;Albedo 01;0;0;Assets/Environments/HeroAssets/Textures/T_Statue02_BC.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;9;45.03206,-77.12562;Half;False;Property;_Blend;Blend;5;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;19.00336,-855.5295;Float;True;Property;_Albedo02;Albedo 02;0;0;Assets/Environments/HeroAssets/Textures/T_Statue02_Emissive_BC.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LerpOp;7;389.3845,-186.3833;Float;False;3;0;FLOAT3;0.0;False;1;FLOAT3;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.LerpOp;6;391.2718,-67.63404;Float;False;3;0;COLOR;0.0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0;False;1;COLOR
Node;AmplifyShaderEditor.SamplerNode;11;394.8499,-532.0576;Float;True;Property;_AO;AO;6;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LerpOp;8;388.6699,-306.4666;Float;False;3;0;COLOR;0.0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;739.9584,-213.2;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;StatueBlend;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;7;0;4;0
WireConnection;7;1;3;0
WireConnection;7;2;9;0
WireConnection;6;0;10;0
WireConnection;6;1;5;0
WireConnection;6;2;9;0
WireConnection;8;0;1;0
WireConnection;8;1;2;0
WireConnection;8;2;9;0
WireConnection;0;0;8;0
WireConnection;0;1;7;0
WireConnection;0;2;6;0
WireConnection;0;5;11;0
ASEEND*/
//CHKSM=F6433C82FD3656043616E41621312DA637C5B425