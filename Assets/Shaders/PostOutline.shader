﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PostOutline" {
	Properties
	{
		_MainTex("Main Texture", 2D) = "black"{}
		_SceneTex("Scene Texture", 2D) = "black"{}
		_OutlineColor("Outline Color", Color) = (0, 1, 1, 1)
		_Strength("Strength", Range(0, 10)) = 1
	}
	SubShader
	{
		Pass {
			CGPROGRAM

			sampler2D _MainTex;
			float2 _MainTex_TexelSize;

			half _Strength;

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : POSITION;
				float2 uvs : TEXCOORD0;
			};

			v2f vert(appdata_base v) {
				v2f o;

				// Despite the fact that we are only drawing a quad to the screen, 
				// Unity requires us to multiply vertices by our MVP matrix, 
				// presumably to keep things working when inexperienced people try copying code from other shaders.
				o.pos = UnityObjectToClipPos(v.vertex);

				// Also, we need to fix the UVs to match our screen space coordinates.
				// There is a Unity define for this that should normally be used.
				o.uvs = o.pos.xy / 2 + 0.5;

				return o;
			}

			half4 frag(v2f IN) : COLOR
			{
				#if UNITY_UV_STARTS_AT_TOP
				if (_MainTex_TexelSize.y > 0)
					IN.uvs.y = 1 - IN.uvs.y;
				#endif

				int NumberOfIterations = 20;
				float TX_x = _MainTex_TexelSize.x;
				float ColorIntensityInRadius;

				for (int i = 0; i < NumberOfIterations; i++) {
					ColorIntensityInRadius += tex2D(
						_MainTex,
						IN.uvs.xy + float2((i - NumberOfIterations / 2) * TX_x,	0)
					).r / NumberOfIterations;
				}

				//return ColorIntensityInRadius * half4(0, 1, 1, 1);
				return ColorIntensityInRadius;
			}

			ENDCG
		}

		GrabPass{}

		Pass{

			CGPROGRAM

			sampler2D _MainTex;
			sampler2D _SceneTex;

			sampler2D _GrabTexture;

			float2 _GrabTexture_TexelSize;

			half _Strength;

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos: POSITION;
				float2 uvs: TEXCOORD0;
			};

			half4 _OutlineColor;

			v2f vert(appdata_base v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uvs = o.pos.xy / 2 + 0.5;

				return o;
			}

			half4 frag(v2f IN) : COLOR {

				#if UNITY_UV_STARTS_AT_TOP
				if (_GrabTexture_TexelSize.y > 0)
					IN.uvs.y = 1 - IN.uvs.y;
				#endif

				int NumberOfIterations = 20;

				float TX_y = _GrabTexture_TexelSize.y;
				half ColorIntensityInRadius = 0;

				if (tex2D(_MainTex, IN.uvs.xy).r > 0) {
					return tex2D(_SceneTex, float2(IN.uvs.x, IN.uvs.y));
				}

				for (int i = 0; i < NumberOfIterations; i++) {
					ColorIntensityInRadius += tex2D(
						_GrabTexture,
						IN.uvs.xy + float2(0, (i - NumberOfIterations / 2) * TX_y)
					).r / NumberOfIterations;
				}

				// Blending of the colors, the half4 is the color for the outline which we should be able to change,
				// half4 outcolor = ColorIntensityInRadius * _OutlineColor * 2 + (1 - ColorIntensityInRadius) * tex2D(_SceneTex, float2(IN.uvs.x, IN.uvs.y));
				// half4 outcolor = ColorIntensityInRadius * _OutlineColor * _Strength + (1 - ColorIntensityInRadius) * tex2D(_SceneTex, float2(IN.uvs.x, IN.uvs.y));
				half4 foo = ColorIntensityInRadius * _OutlineColor * _Strength + tex2D(_SceneTex, float2(IN.uvs.x, IN.uvs.y));
				return foo;
			}
			ENDCG
		}
	}
}