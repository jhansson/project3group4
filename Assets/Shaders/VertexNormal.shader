﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/VertexNormal" {
	Properties {
		_MainTex ("MainTexture", 2D) = "white" {}
		_SecondTex("SecondaryTexture", 2D) = "black" {}
		_MR01("Metallic & Roughness (Main)", 2D) = "gray" {}
		_MR02("Metallic & Roughness (Secondary)", 2D) = "gray" {}
		[Normal]_MainNormal("Main Normalmap", 2D) = "bump" {}
		[Normal]_SecondNormal("Secondary Normalmap", 2D) = "bump" {}
		_Power("Power", Range(1, 10)) = 1
		_Offset("Offset", Range(0, 1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		// Albedo textures
		sampler2D _MainTex;
		sampler2D _SecondTex;

		// Packed metall (R) and roughness (A) textures,
		sampler2D _MR01;
		sampler2D _MR02;

		// Normalmaps
		sampler2D _MainNormal;
		sampler2D _SecondNormal;

		// Will only get the UVs for the maintex assuming this should be same for all textures?
		struct Input {
			float2 uv_MainTex;
			float3 worldNormal;
			INTERNAL_DATA
		};

		half _Power;
		half _Offset;
		half _Weight;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {

			// Get the world normal vector
			float3 worldNormal = WorldNormalVector(IN, float3(0, 0, 1));

			// Dot product times a power factor exposed to user,
			half _Dot = (dot(worldNormal, float3(0, 1, 0)) - _Offset) * _Power;

			// Remap the dot product so we get a value 0 -> 1 instead of -1 -> 1
			_Weight = (((clamp(_Dot, -1, 1) - -1) * 1) / 2);

			// Get the colors from the two textures, and lerp them with our grade,
			fixed4 c1 = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 c2 = tex2D(_SecondTex, IN.uv_MainTex);
			o.Albedo = lerp( c1 , c2 , _Weight) ;

			// Metallic and smoothness come from packed textures, R and A channels
			fixed4 mr1 = tex2D(_MR01, IN.uv_MainTex);
			fixed4 mr2 = tex2D(_MR02, IN.uv_MainTex);
			o.Metallic = lerp(mr1.r, mr2.r, _Weight);
			o.Smoothness = lerp(mr1.a, mr2.a, _Weight);

			// Unpack and lerp normal maps
			fixed3 n1 = UnpackNormal(tex2D(_MainNormal, IN.uv_MainTex));
			fixed3 n2 = UnpackNormal(tex2D(_SecondNormal, IN.uv_MainTex));
			o.Normal = lerp( n1 , n2 , _Weight) ;

			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
